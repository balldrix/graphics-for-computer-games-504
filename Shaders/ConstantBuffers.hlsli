// Constant Buffers.hlsli
// Christopher Ball 2017
// constant buffers for all shaders I use

// PS globals
Texture2D shaderTexture : register(t0); // texture 
Texture2D normalMapTexture : register(t1); // normal map
Texture2D shadowMap : register(t2); // shadow map texture

// clamp sampler states for sampling shadow map
SamplerState linearSampler  : register(s0);
SamplerState clampSampler : register(s1);

// constant buffers
cbuffer ConstantBufferLight2 : register(b0)
{
	float4 ambLightColour;
	float4 diffuseColour;
	float4 specularColour;
	float3 lightDirection;
	float specularPower;
}

cbuffer ConstantBufferProjection : register(b1)
{
	matrix projectionMatrix; // projection matrix
}

cbuffer ConstantBufferView : register(b2)
{
	matrix viewMatrix; // view matrix
}

cbuffer ConstantBufferModel : register(b3)
{
	matrix worldMatrix; // world matrix
}

cbuffer ConstantBufferCamera : register(b4)
{
	float3 cameraPosition; // camera position
	float cameraPadding;
}

cbuffer ConstantBufferParticle : register(b5)
{
	float alpha;			// alpha channel for transparency
	float3 particlePadding;
}

cbuffer ConstantBufferLight : register(b6)
{
	matrix lightViewMatrix;
	matrix lightProjectionMatrix;
}

// vertex shader input struct
struct VShaderInput
{
	float4 position : POSITION;	// semantics set in the input layout element desc
	float3 normal : NORMAL;
	float2 texcoord : TEXCOORD0;
};

// pixel shader input struct
struct PShaderInput
{
	float4 position : SV_POSITION;
	float2 texcoord : TEXCOORD0;
	float3 normal : NORMAL0;
	float3 viewDirection: TEXCOORD1;
	float4 lightPosition : TEXCOORD3;
};

// vertex shader input for normal mapping
struct NormalMapVShaderInput
{
	float4 position: POSITION; // semantics set in the input layout element desc
	float3 normal : NORMAL;
	float2 texcoord : TEXCOORD0;
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
};

// pixel shader input for normal mapping
struct NormalMapPShaderInput
{
	float4 position : SV_POSITION;
	float2 texcoord : TEXCOORD0;
	float3 normal : NORMAL0;
	float3 viewDirection: TEXCOORD1;
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
	float4 lightPosition : TEXCOORD3;
};

// vertex shader input for particle 
struct ParticleVShaderInput
{
	float4 position : POSITION;
	float2 texcoord : TEXCOORD0;
};

struct ParticlePShaderInput
{
	float4 position : SV_POSITION;
	float2 texcoord : TEXCOORD0;
};

struct OcculderVShaderInput
{
	float4 position : POSITION;
};

struct OcculderPShaderInput
{
	float4 position : SV_POSITION;
	float4 depthPosition : TEXTURE0;
};