// DAPixelShader.hlsl
// Christopher Ball 2017
// Pixel shader with ambient and diffuse light

#include "ConstantBuffers.hlsli"

float4 main(PShaderInput input) : SV_TARGET
{
	// store the colour of the pixel from the texture using linear sampler
	float4 textureColour = shaderTexture.Sample(linearSampler, input.texcoord);

	// Set the bias value for fixing the floating point precision issues.
	float bias = 0.0002f;

	float4 pixelColour = ambLightColour;
	pixelColour.a = 1.0;

	// Calculate the projected texture coordinates.
	float2 projectTexCoord;
	projectTexCoord.x = input.lightPosition.x / input.lightPosition.w / 2.0f + 0.5f;
	projectTexCoord.y = -input.lightPosition.y / input.lightPosition.w / 2.0f + 0.5f;

	// calculate light intensity using dot product of the 
	// normal value and inverse of light direction
	float lightIntensity = saturate(dot(input.normal, -lightDirection));

	// Determine if the projected coordinates are in the 0 to 1 range.  If so then this pixel is in the view of the light.
	if((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y))
	{
		// Sample the shadow map depth value from the depth texture using the sampler at the projected texture coordinate location.
		float depthValue = shadowMap.Sample(clampSampler, projectTexCoord).r;

		// Calculate the depth of the light.
		float lightDepthValue = input.lightPosition.z / input.lightPosition.w;

		// Subtract the bias from the lightDepthValue.
		lightDepthValue = lightDepthValue - bias;

		// compare the depth value from the shadow map with the depth using the light positions
		// to see if the pixel is in shadow
		if(lightDepthValue < depthValue)
		{
			if(lightIntensity > 0.0f)
			{
				// use light intensity to calculate diffuse 
				pixelColour += (diffuseColour * lightIntensity);

				// Saturate the final light color.
				pixelColour = saturate(pixelColour);
			}
		}
	}
	else
	{
		// pixel is outside light area
		if(lightIntensity > 0.0f)
		{
			pixelColour += (diffuseColour * lightIntensity);
			pixelColour = saturate(pixelColour);
		}
	}

	// return new pixel colour
	pixelColour = pixelColour * textureColour;
	return pixelColour;
}