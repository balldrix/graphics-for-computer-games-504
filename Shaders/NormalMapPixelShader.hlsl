// NormalMapPixelShader.hlsl
// Christopher Ball 2017
// pixel shader for normal mapping

#include "ConstantBuffers.hlsli"

float4 main(NormalMapPShaderInput input) : SV_TARGET
{
	// store the colour of the pixel from the texture using linear sampler
	float4 textureColour = shaderTexture.Sample(linearSampler, input.texcoord);
	float4 normalMap = normalMapTexture.Sample(linearSampler, input.texcoord);

	// convert normal map values to range -1 to +1
	normalMap = (normalMap * 2.0f) - 1.0f;

	// calculate new normal map normal with bump map equation
	float3 bumpMap;
	bumpMap = normalize((normalMap.x * input.tangent) + (normalMap.y * input.binormal) + (normalMap.z * input.normal));

	// Invert the light direction.
	float3 lightDir = -lightDirection;

	// calculate light intensity using dot product of the 
	// new normal value and inverse of light direction
	float lightIntensity = saturate(dot(bumpMap, lightDir));

	// Set the bias value for fixing the floating point precision issues.
	float bias = 0.0002f;

	float4 pixelColour = ambLightColour;
	pixelColour.a = 1.0;

	// Calculate the projected texture coordinates.
	float2 projectTexCoord;
	projectTexCoord.x = input.lightPosition.x / input.lightPosition.w / 2.0f + 0.5f;
	projectTexCoord.y = -input.lightPosition.y / input.lightPosition.w / 2.0f + 0.5f;

	// Determine if the projected coordinates are in the 0 to 1 range.  If so then this pixel is in the view of the light.
	if((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y))
	{
		// Sample the shadow map depth value from the depth texture using the sampler at the projected texture coordinate location.
		float depthValue = shadowMap.Sample(clampSampler, projectTexCoord).r;

		// Calculate the depth of the light.
		float lightDepthValue = input.lightPosition.z / input.lightPosition.w;

		// Subtract the bias from the lightDepthValue.
		lightDepthValue = lightDepthValue - bias;

		// compare the depth value from the shadow map with the depth using the light positions
		// to see if the pixel is in shadow
		if(lightDepthValue < depthValue)
		{
			// use light intensity to calculate diffuse 
			pixelColour += (diffuseColour * lightIntensity);

			// Saturate the final light color.
			pixelColour = saturate(pixelColour);
		}
	}
	else
	{
		// pixel is outside light area
		pixelColour += (diffuseColour * lightIntensity);
		pixelColour = saturate(pixelColour);
	}

	// saturate incase value is higher than 1
	pixelColour = pixelColour * textureColour;

	return pixelColour;
}