// OcculderPixelShader.hlsl
// Christopher Ball 2017
// void shader for occulders as they 
// only record depth for the shadow map

#include "ConstantBuffers.hlsli"

float4 main(OcculderPShaderInput input) : SV_TARGET
{
	// get the depth value by dividing the homogenous (depth)z by (normal)w
	float depthValue = input.depthPosition.z / input.depthPosition.w;
	
	// return depth as greyscale colour
	float4 colour = float4(depthValue, depthValue, depthValue, 1.0f);
	return colour;
}