// OccluderVertexShader.hlsl
// Christopher Ball 2017
// Used to calculate occulder position
// in shadow map

#include "ConstantBuffers.hlsli"

OcculderPShaderInput main(OcculderVShaderInput input)
{
	OcculderPShaderInput output = (OcculderPShaderInput)0;
	float4 pos = input.position;

	pos = mul(pos, worldMatrix);
	pos = mul(pos, lightViewMatrix);
	pos = mul(pos, lightProjectionMatrix);
	output.position = pos;

	// pass the transformed position to pixel shader to calculate depth
	output.depthPosition = output.position;

	return output;
}