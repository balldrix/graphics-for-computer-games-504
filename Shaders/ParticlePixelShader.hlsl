// ParticlePixelShader.hlsl
// Christopher Ball 2017
// pixel shader for particles

#include "ConstantBuffers.hlsli"

float4 main(ParticlePShaderInput input) : SV_TARGET
{
	float4 colour = shaderTexture.Sample(linearSampler, input.texcoord);

	// use alpha channel
	if(colour.a > 0)
	{
		colour.a = alpha;
	}

	return colour;
}