// ParticleVertexShader.hlsl
// Christopher Ball 2017
// vertex shader for particles

#include "ConstantBuffers.hlsli"

ParticlePShaderInput main(ParticleVShaderInput input)
{
	// initialise output 
	ParticlePShaderInput output = (ParticlePShaderInput)0;
	
	// multiply vertex position by view and project matrices
	output.position = mul(mul(mul(input.position, worldMatrix), viewMatrix), projectionMatrix);

	output.texcoord = input.texcoord; // set texture cordinate

	return output;
}