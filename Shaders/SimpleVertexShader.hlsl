// SimpleVertexShader.hlsl
// Christopher Ball 2017
// Simple vertex shader

#include "ConstantBuffers.hlsli"

// Vertex Shader
PShaderInput main(VShaderInput input)
{
	PShaderInput output = (PShaderInput)0; // pixel shader output struct
	
	output.position.w = 1.0f;
	output.position = mul(mul(mul(input.position, worldMatrix), viewMatrix), projectionMatrix); // multiply vertex position by view and project matrices

	// projected view point from light source
	output.lightPosition.w = 1.0f;
	output.lightPosition = mul(mul(mul(input.position, worldMatrix), lightViewMatrix), lightProjectionMatrix);

	// Transform the vertex position into projected space.
	float4 modelWorldPosition = mul(input.position, worldMatrix);

	// calculate view direction
	output.viewDirection = normalize(cameraPosition.xyz - modelWorldPosition.xyz);

	// calculate new normal value with world matrix multiplication
	output.normal = normalize(mul(input.normal.xyz, (float3x3)worldMatrix));

	output.texcoord = input.texcoord; // set texture cordinate
	
	// calculate view direction
	output.viewDirection = normalize(cameraPosition.xyz - modelWorldPosition.xyz);

	return output;
}