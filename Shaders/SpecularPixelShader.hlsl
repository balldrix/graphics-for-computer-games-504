// SpecularPixelShader.hlsl
// Christopher Ball 2017
// using blinn-phong shading model
// to create specular lighting

#include "ConstantBuffers.hlsli"

float4 main(PShaderInput input) : SV_TARGET
{
	// store the colour of the pixel from the texture using linear sampler
	float4 textureColour = shaderTexture.Sample(linearSampler, input.texcoord);

	// calculate light intensity using dot product of the 
	// normal value and inverse of light direction
	float lightIntensity = saturate(dot(input.normal, -lightDirection));

	// Set the bias value for fixing the floating point precision issues.
	float bias = 0.0002f;

	float4 pixelColour = ambLightColour;
	pixelColour.a = 1.0;

	float4 specular = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float3 halfwayVector = float3(0.0f, 0.0f, 0.0f);
	float specularIntensity = 0.0f;

	// Calculate the projected texture coordinates.
	float2 projectTexCoord;
	projectTexCoord.x = input.lightPosition.x / input.lightPosition.w / 2.0f + 0.5f;
	projectTexCoord.y = -input.lightPosition.y / input.lightPosition.w / 2.0f + 0.5f;

	// Determine if the projected coordinates are in the 0 to 1 range.  If so then this pixel is in the view of the light.
	if((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y))
	{
		// Sample the shadow map depth value from the depth texture using the sampler at the projected texture coordinate location.
		float depthValue = shadowMap.Sample(clampSampler, projectTexCoord).r;

		// Calculate the depth of the light.
		float lightDepthValue = input.lightPosition.z / input.lightPosition.w;

		// Subtract the bias from the lightDepthValue.
		lightDepthValue = lightDepthValue - bias;

		// compare the depth value from the shadow map with the depth using the light positions
		// to see if the pixel is in shadow
		if(lightDepthValue < depthValue)
		{
			// check if intensity is higher than 0
			if(lightIntensity > 0.0f)
			{
				// set pixel colour with intensity and colour value
				pixelColour += diffuseColour * lightIntensity;

				// saturate incase value is higher than 1
				pixelColour = saturate(pixelColour);

				// calculate halfway vector between light direction and 
				halfwayVector = normalize(-lightDirection + input.viewDirection);

				// calculate specular amount
				specularIntensity = pow(saturate(dot(input.normal, halfwayVector)), specularPower);

				specular = mul(specularIntensity, specularColour);
			}
		}
	}
	else
	{
		// pixel is outside light area
		if(lightIntensity > 0.0f)
		{
			// set pixel colour with intensity and colour value
			pixelColour += diffuseColour * lightIntensity;

			// saturate incase value is higher than 1
			pixelColour = saturate(pixelColour);

			// calculate halfway vector between light direction and 
			halfwayVector = normalize(-lightDirection + input.viewDirection);

			// calculate specular amount
			specularIntensity = pow(saturate(dot(input.normal, halfwayVector)), specularPower);

			specular = mul(specularIntensity, specularColour);
		}
	}


	// pixel colour is diffuse for texture
	pixelColour *= textureColour;

	// return pixel colour
	return pixelColour + specular;
}