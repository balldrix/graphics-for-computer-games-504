// HBlurPixelShader.hlsl
// Christopher Ball 2017
// horizontal blur

Texture2D<float4> Texture : register(t0);
sampler TextureSampler : register(s0);

float4 main(float4 color : COLOR0, float2 texCoord : TEXCOORD0) : SV_Target0
{
	// clear pixel colour
	float4 pixelColour = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// create weight array
	float weight[5];

	weight[0] = 1.0f;
	weight[1] = 0.9f;
	weight[2] = 0.6f;
	weight[3] = 0.2f;
	weight[4] = 0.1f;

	// Create a normalized value to average the weights out a bit.
	float normalization = (weight[0] + 2.0f * (weight[1] + weight[2] + weight[3] + weight[4]));

	// Normalize the weights.
	weight[0] = weight[0] / normalization;
	weight[1] = weight[1] / normalization;
	weight[2] = weight[2] / normalization;
	weight[3] = weight[3] / normalization;
	weight[4] = weight[4] / normalization;

	// create texture offsets
	float2 offset[9];

	offset[0].x = texCoord.x;
	offset[0].y = texCoord.y - 0.00277777777777777777777777777778 * 4;

	offset[1].x = texCoord.x;
	offset[1].y = texCoord.y - 0.00277777777777777777777777777778 * 3;

	offset[2].x = texCoord.x;
	offset[2].y = texCoord.y - 0.00277777777777777777777777777778 * 2;

	offset[3].x = texCoord.x;
	offset[3].y = texCoord.y - 0.00277777777777777777777777777778 * 1;

	offset[4].x = texCoord.x;
	offset[4].y = texCoord.y;

	offset[5].x = texCoord.x;
	offset[5].y = texCoord.y + 0.00277777777777777777777777777778 * 1;

	offset[6].x = texCoord.x;
	offset[6].y = texCoord.y + 0.00277777777777777777777777777778 * 2;

	offset[7].x = texCoord.x;
	offset[7].y = texCoord.y + 0.00277777777777777777777777777778 * 3;

	offset[8].x = texCoord.x;
	offset[8].y = texCoord.y + 0.00277777777777777777777777777778 * 4;

	pixelColour += Texture.Sample(TextureSampler, offset[0]) * weight[4];
	pixelColour += Texture.Sample(TextureSampler, offset[1]) * weight[3];
	pixelColour += Texture.Sample(TextureSampler, offset[2]) * weight[2];
	pixelColour += Texture.Sample(TextureSampler, offset[3]) * weight[1];
	pixelColour += Texture.Sample(TextureSampler, offset[4]) * weight[0];
	pixelColour += Texture.Sample(TextureSampler, offset[5]) * weight[1];
	pixelColour += Texture.Sample(TextureSampler, offset[6]) * weight[2];
	pixelColour += Texture.Sample(TextureSampler, offset[7]) * weight[3];
	pixelColour += Texture.Sample(TextureSampler, offset[8]) * weight[4];

	pixelColour.a = 1.0f;

	return pixelColour;
}