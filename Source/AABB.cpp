#include "AABB.h"
#include "GameObject.h"

AABB::AABB() :
	m_min(0.0f,0.0f, 0.0f),
	m_max(0.0f,0.0f, 0.0f)
{
}

AABB::AABB(const Vector3 & min, const Vector3 & max)
{
	// set min and max vectors
	m_min = min;
	m_max = max;
}

AABB::~AABB()
{
}

void
AABB::Reset()
{
	// reset vectors to zero
	m_min.Zero;
	m_max.Zero;
}

void
AABB::SetAABB(const AABB &hitbox)
{
	// set min and max vectors
	m_min = hitbox.m_min;
	m_max = hitbox.m_max;
}

void
AABB::SetAABB(const Vector3 &min, const Vector3 &max)
{
	// set min and max vectors
	m_min = min;
	m_max = max;
}

void
AABB::OffSetAABB(float x, float y, float z)
{
	// offset min and max x values
	m_min.x += x;
	m_max.x += x;

	// offset min and max y values
	m_min.y += y;
	m_max.y += y;

	// offset min and max z values
	m_min.z += z;
	m_max.z += z;
}

void AABB::OffSetAABB(Vector3 offSet)
{
	// off set min and max values
	m_min += offSet;
	m_max += offSet;
}

void
AABB::SetMin(const Vector3 &min)
{
	m_min = min; // set min
}

void 
AABB::SetMax(const Vector3 &max)
{
	m_max = max; // set max
}

bool
AABB::BoxCollision(const AABB &other) const
{
	// check if min and max points fall outside other bounding box
	// if they always fall outside they will be no collision 
	if (m_max.x < other.m_min.x || m_min.x > other.m_max.x)
	{
		return false;
	}

	if(m_max.y < other.m_min.y || m_min.y > other.m_max.y)
	{
		return false;
	}

	if(m_max.z < other.m_min.z || m_min.z > other.m_max.z)
	{
		return false;
	}
	
	return true;
}

bool 
AABB::BoxSphereCollision(const Vector3 &spherePosition, const float &sphereRadius)
{
	// calculate the square of the radius
	float radiusSquared = sphereRadius * sphereRadius;

	// minimum distance
	float minDistance = 0;

	// check if sphere position x is below aabb min x
	if(spherePosition.x < m_min.x)
	{
		minDistance += (spherePosition.x - m_min.x) * (spherePosition.x - m_min.x);
	}
	else if(spherePosition.x > m_max.x)
	{
		minDistance += (spherePosition.x - m_max.x) * (spherePosition.x - m_max.x);
	}

	// check if sphere position y is below aabb min y
	if(spherePosition.y < m_min.y)
	{
		minDistance += (spherePosition.y - m_min.y) * (spherePosition.y - m_min.y);
	}
	else if(spherePosition.y > m_max.y)
	{
		minDistance += (spherePosition.y - m_max.y) * (spherePosition.y - m_max.y);
	}

	// check if sphere position y is below aabb min y
	if(spherePosition.z < m_min.z)
	{
		minDistance += (spherePosition.z - m_min.z) * (spherePosition.z - m_min.z);
	}
	else if(spherePosition.z > m_max.z)
	{
		minDistance += (spherePosition.z - m_max.z) * (spherePosition.z - m_max.z);
	}

	// distance is within sphere return true
	if(minDistance <= radiusSquared)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool 
AABB::SphereSphereCollision(SphereCollider sphere1, SphereCollider sphere2)
{
	// distance vector between spheres
	Vector3 distance = sphere1.position - sphere2.position;
	
	// distance length
	float length = distance.Length();
	
	// if length is less than sum of radii
	if(length < sphere1.radius + sphere2.radius)
	{
		return true;
	}
	else
	{
		return false;
	}
}

AABB&
AABB::operator = (const AABB &other)
{
	// set min and max values
	m_min = other.m_min;
	m_max = other.m_max;
	return *this;
}