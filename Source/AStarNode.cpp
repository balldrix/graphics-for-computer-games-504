#include "pch.h"
#include "AStarNode.h"
#include "Level.h"

AStarNode::AStarNode() :
	m_ID(0),
	m_x(0),
	m_z(0),
	m_g(0),
	m_h(0),
	m_parent(nullptr),
	m_goalNode(nullptr)
{
}

AStarNode::~AStarNode()
{
}

void
AStarNode::Init(int x, int z, AStarNode* parent, AStarNode* goalNode)
{
	m_ID = z * MAZE_WIDTH + x;
	m_x = x;
	m_z = z;
	m_goalNode = goalNode;
	m_parent = parent;
	m_neighbours.clear();
}

void
AStarNode::CalculateH(AStarNode* endNode)
{
	int x = abs(m_x - endNode->m_x);
	int z = abs(m_z - endNode->m_z);

	m_h = x + z;
}

void 
AStarNode::AddNeighbour(AStarNode* neighbour)
{
	m_neighbours.push_back(neighbour);
}
