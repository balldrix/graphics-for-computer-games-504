// AStarNode.h
// Christopher Ball 2017
// Node class for A* pathfinding

#ifndef _ASTARNODE_H_
#define _ASTARNODE_H_

#include "pch.h"

class AStarNode
{
public:
	AStarNode();
	~AStarNode();

	void		Init(int x, int z, AStarNode* parent, AStarNode* goalNode);
	
	void		CalculateH(AStarNode* endNode); // calculate manhattan cost
	float		GetF() { return m_g + m_h; }	// total cost of node	
	void		AddNeighbour(AStarNode* neighbour); // add neighbour node to list

	int			m_ID;							// unique ID
	int			m_x;							// x position	
	int			m_z;							// z position
	float		m_g;							// distance from start
	int			m_h;							// heuristic
	AStarNode*	m_parent;						// parent node
	AStarNode*	m_goalNode;						// goal node
	std::vector<AStarNode*> m_neighbours;		// node's neighbours
};

#endif _ASTARNODE_H_
