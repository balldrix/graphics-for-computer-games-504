#include "pch.h"
#include "AStarPathfinding.h"
#include "AStarNode.h"

AStarPathfinding::AStarPathfinding()
{
}

AStarPathfinding::~AStarPathfinding()
{
}

void AStarPathfinding::Init(bool map[])
{
	// copy map of walkable cells 
	memcpy(m_map, map, sizeof(m_map));
}

void 
AStarPathfinding::FindPath(AStarNode* startNode,
								AStarNode*  endNode,
								std::vector<AStarNode*>& pathOut)
{
	// clear lists
	for(int i = 0; i < m_openList.size(); i++)
	{
		delete m_openList[i];
		m_openList[i] = nullptr;
	}
	m_openList.clear();

	for(int i = 0; i < m_closedList.size(); i++)
	{
		delete m_closedList[i];
		m_closedList[i] = nullptr;
	}
	m_closedList.clear();

	for(int i = 0; i < pathOut.size(); i++)
	{
		delete pathOut[i];
		pathOut[i] = nullptr;
	}
	pathOut.clear();

	if(startNode)
	{
		// start node distance is 0 from itself
		startNode->m_g = 0;
		startNode->CalculateH(endNode);

		// add start node to open list
		m_openList.push_back(startNode);

		while(!m_openList.empty())
		{
			// make a new node with lowest fscore
			AStarNode* currentNode;

			float lowestFScore = 99999.9f;
			for(int i = 0; i < m_openList.size(); i++)
			{
				float currentFScore = m_openList[i]->GetF();
				if(currentFScore < lowestFScore)
				{
					lowestFScore = currentFScore;
					currentNode = m_openList[i];
				}
			}

			// if current node is the end
			if(currentNode->m_ID == endNode->m_ID)
			{
				// path finding complete
				pathOut = BuildPath(currentNode);
				return; 
			}

			// remove currentNode from open list
			for(int i = 0; i < m_openList.size(); i++)
			{
				if(m_openList[i]->m_ID == currentNode->m_ID)
				{
					m_openList.erase(m_openList.begin() + i);
				}
			}

			// add current node to closed set
			m_closedList.push_back(currentNode);

			// calculate neighbours

			int x = currentNode->m_x;
			int z = currentNode->m_z;

			// check north is walkable
			if(m_map[(z - 1) * MAZE_WIDTH + x])
			{
				AStarNode* tempNode = new AStarNode();
				tempNode->Init(x, z - 1, currentNode, endNode);
				currentNode->AddNeighbour(tempNode);
			}

			// check south is walkable
			if(m_map[(z + 1) * MAZE_WIDTH + x])
			{
				AStarNode* tempNode = new AStarNode();
				tempNode->Init(x, z + 1, currentNode, endNode);
				currentNode->AddNeighbour(tempNode);
			}

			// check east is walkable
			if(m_map[z * MAZE_WIDTH + (x + 1)])
			{
				AStarNode* tempNode = new AStarNode();
				tempNode->Init(x + 1, z, currentNode, endNode);
				currentNode->AddNeighbour(tempNode);
			}

			// check west is walkable
			if(m_map[z * MAZE_WIDTH + (x - 1)])
			{
				AStarNode* tempNode = new AStarNode();
				tempNode->Init(x - 1, z, currentNode, endNode);
				currentNode->AddNeighbour(tempNode);
			}

			// loop through neighbours
			for(int i = 0; i < currentNode->m_neighbours.size(); i++)
			{
				AStarNode* neighbour = currentNode->m_neighbours[i];

				// if neighbour is not on closed list
				if(!IsOnClosedList(neighbour))
				{
					// get tentative g value
					float tempG = currentNode->m_g + 10;

					// if neighbour is not already on open list
					if(!IsOnOpenList(neighbour))
					{
						// add to open list
						m_openList.push_back(neighbour);
					}
					else
					{
						neighbour = GetNodeFromOpen(neighbour->m_ID);
						if(tempG >= neighbour->m_g)
						{
							continue;
						}
					}

					neighbour->m_g = tempG;
					neighbour->m_parent = currentNode;
					neighbour->CalculateH(endNode);
				}
			}
		}
	}
}

bool 
AStarPathfinding::IsOnClosedList(AStarNode* node)
{
	for(int i = 0; i < m_closedList.size(); i++)
	{
		if(node->m_ID == m_closedList[i]->m_ID)
		{
			return true;
		}
	}
	return false;
}

bool 
AStarPathfinding::IsOnOpenList(AStarNode* node)
{
	for(int i = 0; i < m_openList.size(); i++)
	{
		if(node->m_ID == m_openList[i]->m_ID)
		{
			return true;
		}
	}
	return false;
}

std::vector<AStarNode*> 
AStarPathfinding::BuildPath(AStarNode* endNode)
{
	// temporary vars
	std::vector<AStarNode*> newPath;
	AStarNode* temp = endNode;

	// add end node to list
	newPath.push_back(temp);

	// loop until temp node is the start of path
	while(temp->m_parent != nullptr)
	{
		// add node to path
		newPath.push_back(temp->m_parent);
		temp = temp->m_parent;
	}

	// return complete path
	return newPath;
}

AStarNode*
AStarPathfinding::GetNodeFromOpen(int ID)
{
	for(int i = 0; i < m_openList.size(); i++)
	{
		if(m_openList[i]->m_ID == ID)
		{
			return m_openList[i];
		}
	}
	return nullptr;
}
