// AStarPathfinding.h
// Christopher Ball 2017
// Class to calculate best path with A*

#ifndef _ASTARPATHFINDING_H_
#define _ASTARPATHFINDING_H_

#include "pch.h"
#include "Level.h"

class AStarNode;

class AStarPathfinding
{
public:
	AStarPathfinding();
	~AStarPathfinding();
	void Init(bool map[]); // initialise pathfinding
	void FindPath(AStarNode* startNode, AStarNode* endNode, std::vector<AStarNode*> &pathOut); // find path from start node to end node
	bool IsOnClosedList(AStarNode* node); // check if node is on closed list
	bool IsOnOpenList(AStarNode* node); // check if node is on open list

	AStarNode*	GetNodeFromOpen(int ID); // return node with ID from open list

private:
	std::vector<AStarNode*> BuildPath(AStarNode* endNode);	// build path from end node backwards
	std::vector<AStarNode*> m_openList;						// open list
	std::vector<AStarNode*> m_closedList;					// close list
	bool m_map[MAZE_DEPTH * MAZE_WIDTH];					// array of map to check for paths
};


#endif _ASTARPATHFINDING_H_
