#include "pch.h"
#include "Bullet.h"
#include "Constants.h"

Bullet::Bullet() :
	m_lifeTimer(0.0f)
{
	m_sphereCollider.radius = 0.2f;
}

Bullet::~Bullet()
{
}

void 
Bullet::Update(float deltaTime)
{
	// check life timer
	if(m_lifeTimer > NPCConstants::BULLET_LIFETIME)
	{
		// if time is up then kill bullet
		Kill();
	}

	// update position
	m_position += m_velocity * deltaTime;

	// transpose model matrix
	m_modelMatrix = XMMatrixTranslationFromVector(m_position);

	// update collider
	m_sphereCollider.position = m_position;

	// create final model world matrix
	m_modelMatrix = m_rotationalMatrix * m_scaleMatrix * m_modelMatrix;

	// update timer
	m_lifeTimer += deltaTime;
}

void 
Bullet::Kill()
{	
	m_active = false;
	m_velocity = Vector3(0.0f, 0.0f, 0.0f);
	m_lifeTimer = 0.0f;
	m_position = Vector3(0.0f, 0.0f, 0.0f);
}
