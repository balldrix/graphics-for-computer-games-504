// Bullet.h
// Christopher Ball 2017
// bullet class for projectiles

#ifndef _BULLET_H_
#define _BULLET_H_

#include "GameObject.h"

class Bullet : public GameObject
{
public:
	Bullet();
	~Bullet();
	void Update(float deltaTime);	// update bullet
	void Kill();				// reset life timer

private:
	float m_lifeTimer;				// life timer
};

#endif _BULLET_H_