#include "Camera.h"

Camera::Camera() :
m_eyePosition(0.0f, 0.0f, 0.0f),
m_focusPosition(0.0f, 0.0f, 0.0f),
m_upDirection(0.0f, 0.0f, 0.0f),
m_viewMatrix(),
m_projectionMatrix(),
m_screenWidth(0.0f),
m_screenHeight(0.0f),
m_fov(0.0f),
m_aspect(0.0f),
m_nearZ(0.0f),
m_farZ(0.0f),
m_offset(Vector3(0.0f, 0.0f, 0.0f)),
m_zoom(1.0f),
m_angle(0.0f)
{}

Camera::~Camera()
{}

void 
Camera::Init(Vector3 position, float angle, float width, float height, float nearZ, float farZ)
{
	// set start offset
	m_offset = OFFSET;

	// set camera values
	SetView(position, angle, Vector3(0.0f, 1.0f, 0.0f));

	// copy params into member variables
	m_screenWidth = width;
	m_screenHeight = height;
	m_nearZ = nearZ;
	m_farZ = farZ;
	m_fov = XM_PIDIV4; // Pi / 4 = 45 degrees
	m_aspect =  width / height; // ratio of width / height

	// set projection
	SetPerspective(m_fov, m_aspect, m_nearZ, m_farZ);

	SetViewMatrix();
	SetProjectionMatrix();
}

void
Camera::Update()
{
	SetViewMatrix(); // set view matrix
	SetProjectionMatrix(); // set projection matrix
}

void 
Camera::SetViewMatrix()
{
	// set view matrix with XMMatrixLookAtLH function 
	m_viewMatrix = XMMatrixLookAtLH(m_eyePosition,
									m_focusPosition,
									m_upDirection);
}

void 
Camera::SetProjectionMatrix()
{
	// set frustrum projection with XMMatrixPerspectiveFovLH function
	m_projectionMatrix = XMMatrixPerspectiveFovLH(m_fov,
												  m_aspect,
												  m_nearZ,
												  m_farZ);
}

void
Camera::SetView(Vector3 position, float angle, Vector3 up)
{
	// set position, facing and up values

	// get rotational matrix of view angle
	Matrix rotationMatrix = Matrix::CreateRotationY(angle + m_angle);

	// create new offset vector using rotation
	Vector3 transformedOffset = Vector3::Transform(OFFSET, rotationMatrix);

	// set camera position
	m_eyePosition = transformedOffset * m_zoom + position;

	// set facing position
	m_focusPosition = position;

	// set up direction
	m_upDirection = up;
}

void 
Camera::SetPerspective(float fov, float aspect, float nearZ, float farZ)
{
	// set field of view, aspect ratio, near plane z and far plane z values
	m_fov = fov;
	m_aspect = aspect;
	m_nearZ = nearZ;
	m_farZ = farZ;
}

void 
Camera::SetAngle(float angle)
{
	m_angle += angle;
}

void 
Camera::SetZoom(float zoom)
{
	m_zoom += zoom;

	if(m_zoom >= 1.8f)
	{
		m_zoom = 1.8f;
	}

	if(m_zoom <= 0.4f)
	{
		m_zoom = 0.4f;
	}
}

void 
Camera::Reset()
{
	m_offset = OFFSET;
	m_zoom = 1.0f;
	m_angle = 0.0f;
}


