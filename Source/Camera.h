// Camera.h
// Christopher Ball 2017
// manages camera position, view
// direction and frustrum

#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "pch.h"

// camera constants
const Vector3 OFFSET = Vector3(0.0f, 10.0f, -8.0f);	// camera offset starting position

class Camera
{
public:
	Camera();
	~Camera();

	void	Init(Vector3 position,
				 float angle,
				 float width,
				 float height,
				 float nearZ,
				 float farZ);	// initialise camera

	void	Update();		// update camera

	void	SetViewMatrix(); // set view matrix
	void	SetProjectionMatrix(); // set projection matrix
	void	SetView(Vector3 position, float angle, Vector3 up);
	void	SetPerspective(float fov, float aspect, float nearZ, float farZ);
	void	SetAngle(float angle);	// set view angle
	void	SetZoom(float zoom);	// set view zoom
	void	Reset();			// reset camera

	Matrix	GetViewMatrix() const { return m_viewMatrix; } // get view matrix
	Matrix	GetProjectionMatrix() const { return m_projectionMatrix; } // get projection matrix
	float	GetAngle() const { return m_angle; } // get camera angle
	float	GetZoom() const { return m_zoom; } // get zoom
	Vector3	GetEyePosition() const { return m_eyePosition; } // eye position

private:
	Vector3 m_eyePosition;		// camera position in space
	Vector3 m_focusPosition;	// unit vector of camera's facing direction
	Vector3 m_upDirection;		// unit vector of camera's upwards direction
	Matrix	m_viewMatrix;		// camera view matrix
	Matrix	m_projectionMatrix; // camera projection frustrum matrix
	float	m_screenWidth;		// screen width
	float	m_screenHeight;		// screen height
	float	m_fov;				// field of view angle
	float	m_aspect;			// aspect ration
	float	m_nearZ;			// near z position
	float	m_farZ;				// far z position

	Vector3 m_offset;		// camera offset position
	float	m_angle;		// offset rotation angle
	float	m_zoom;			// camera zoom amount
};

#endif _CAMERA_H_
