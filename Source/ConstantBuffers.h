// ConstantBuffers.h
// Christopher Ball 2017
// header file for keeping Constant Buffer and 
// vertex structs, for the shaders, in one place

#ifndef _CONSTANTBUFFERS_H_
#define	_CONSTANTBUFFERS_H_

#include "pch.h"

// number of samples used for blur
#define MAX_SAMPLES 5

// Struct for all standard vertices
struct Vertex
{
	Vector3 position;	// vertex position
	Vector3 normal;		// normal co-ordinates
	Vector2 uv;			// texture co-ordinates
};

// normal map vertex struct
struct NormalMapVertex
{
	Vector3 position;
	Vector3 normal;
	Vector2 uv;
	Vector3 tangent;
	Vector3 binormal;
};

// constant buffer for shadows
struct ConstantBufferLight
{
	Matrix lightViewMatrix;
	Matrix lightProjectionMatrix;
};

// constant buffer used for lighting
struct ConstantBufferLight2
{
	Vector4 ambLightColour;
	Vector4 diffuseColour;
	Vector4 specularColour;
	Vector3 lightDirection;
	float specularPower;
};

// constant buffer used for the view projection
struct ConstantBufferProjection
{
	Matrix projectionMatrix; // projection matrix
};

// constant buffer for camera view
struct ConstantBufferView
{
	Matrix viewMatrix; // view matrix
};

// constant buffer for world matrix
struct ConstantBufferModel
{
	Matrix worldMatrix; // world matrix
};

// constant buffer camera position
struct ConstantBufferCamera
{
	Vector3 cameraPosition;	// camera position
	float padding;
};

// constant buffer for particle alpha channel
struct ConstantBufferParticle
{
	float alpha;		// alpha transparency
	Vector3  padding;
};

// basic vertex input layout definition
static D3D11_INPUT_ELEMENT_DESC PNTInputElementDesc[] =
{
	{
		"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0
	},
	{
		"NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
	},
	{
		"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
	},
};

// vertex input for normal mapping
static D3D11_INPUT_ELEMENT_DESC PNTTBInputElementDesc[] =
{
	{
		"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0
	},
	{
		"NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
	},
	{
		"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
	},
	{
		"TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
	},
	{
		"BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
	},
};

// vertex input for particls
static D3D11_INPUT_ELEMENT_DESC PTInputElementDesc[] =
{
	{
		"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0
	},
	{
		"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
	},
};

#endif _CONSTANTBUFFERS_H_