// Constants.h
// Christopher Ball 2017
// This is where I keep my constant variables for use throughout my project

#ifndef _CONSTANTS_H_
#define	_CONSTANTS_H_

#include "pch.h"

// Global Constants
namespace GlobalConstants
{
	const unsigned int	GAME_WIDTH		= 1280;							// width of game window in pixels
	const unsigned int	GAME_HEIGHT		= 720;							// height of game window in pixels
	const wchar_t		WND_CLASS_NAME[] = L"MyWndClass";				// name of class for creating a window
	const wchar_t		WINDOW_NAME[] = L"Assignment 504";				// Title of window that shows in top bar
	const float			BACK_COLOUR[4] = {0.0f, 0.0f, 0.0f, 0.0f };		// colour of window backPath

	const unsigned int	SHADOWMAP_WIDTH = 1024;				// width of shadow map texture
	const unsigned int	SHADOWMAP_HEIGHT = 1024;				// height of shadow map texture

	// cell dimensions
	const unsigned int CELL_WIDTH = 2;	// width of wall

	// blur effect life time
	const float			BLUR_TIMER = 0.5f;
}

// Player Constants
namespace PlayerConstants
{
	const float MOVEMENT_SPEED = 10.0f;
	const float TURNING_SPEED = 2.0f;
	const float ZOOM_SPEED = 2.0f;
	const int	MAX_HEALTH = 100;
	const int	MAX_LIVES = 3;
}

// NPC Constants
namespace NPCConstants
{
	const float NPC_SPEED = 4.0f;
	const float NPC_VIEWRANGE = 12.0f;
	const float NPC_FIRINGRATE = 1.0f;
	const float BULLET_LIFETIME = 1.0f;
	const float BULLET_SPEED = 20.0f;
}

#endif _CONSTANTS_H_
