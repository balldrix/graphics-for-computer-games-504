// Controls.h
// Christopher Ball 2017
// list of controls

#ifndef _CONTROLS_H_
#define _CONTROLS_H_

#include "pch.h"

// namespace to turn virtual key controls
// into readable name
namespace Controls
{
	UCHAR escKey = VK_ESCAPE;
	UCHAR playerFowards = 'W';
	UCHAR playerBackwards = 'S';
	UCHAR playerLeft = 'A';
	UCHAR playerRight = 'D';
	UCHAR rotateViewLeft = VK_LEFT;
	UCHAR rotateViewRight = VK_RIGHT;
	UCHAR zoomIn = VK_UP;
	UCHAR zoomOut = VK_DOWN;
	UCHAR resetCamera = VK_NUMPAD0;
}

#endif _CONTROLS_H_
