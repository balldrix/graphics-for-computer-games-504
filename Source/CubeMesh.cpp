#include "pch.h"
#include "CubeMesh.h"

#include "Graphics.h"
#include "ConstantBuffers.h"

CubeMesh::CubeMesh()
{
}

CubeMesh::~CubeMesh()
{
}

void 
CubeMesh::Init(Graphics* graphics)
{
	// setup cube mesh vertices, indices
	// and buffers for the cube mesh

	std::vector<VertexPositionNormalTexture> vertices;
	std::vector<uint16_t> indices;

	GeometricPrimitive::CreateCube(vertices, indices, 2.0f, false);

	D3D11_BUFFER_DESC bd = { 0 };
	D3D11_SUBRESOURCE_DATA subData = { 0 };

	m_vertexCount = vertices.size();
	m_indexCount = indices.size();

	// create vertex buffer for a cube
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(VertexPositionNormalTexture) * m_vertexCount;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	subData.pSysMem = vertices.data();
	graphics->GetDevice()->CreateBuffer(&bd, &subData, &m_vertexBuffer);

	// create index buffer
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(short) * m_indexCount;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	subData.pSysMem = indices.data();
	graphics->GetDevice()->CreateBuffer(&bd, &subData, &m_indexBuffer);

	// set stride
	m_stride = sizeof(VertexPositionNormalTexture);
}
