#include "Flag.h"
#include "Camera.h"

Flag::Flag() :
	m_target(nullptr)
{
}

Flag::~Flag()
{
}

void 
Flag::Update(float deltaTime)
{
	if(m_target) // check flag has a target turn to face target
	{
		// get vector point of target
		Vector3 lookAt = m_target->GetEyePosition() - m_position;
		lookAt.Normalize();

		// angle is inverse cos of look at z axis
		float angle = acos(lookAt.z);

		// if camera is left of flag
		if(lookAt.x < 0)
		{
			// inverse angle
			angle *= -1.0f;
		}

		// set rotation matrix by rotating around y
		m_rotationalMatrix = XMMatrixRotationY(angle);
	}

	// translate model position
	m_modelMatrix = XMMatrixTranslationFromVector(m_position);

	// create final model world matrix
	m_modelMatrix = m_rotationalMatrix * m_scaleMatrix * m_modelMatrix;
}

void 
Flag::SetTarget(Camera* target)
{
	m_target = target;
}
