// Flag.h
// Christopher Ball 2017
// flag post using quad mesh and billboarding

#ifndef _FLAG_H_
#define _FLAG_H_

#include "GameObject.h"

class Camera;

class Flag : public GameObject
{
public:
	Flag();
	~Flag();
	void Update(float deltaTime);			// update flag object
	void SetTarget(Camera* target);		// set target for billboarding

private:
	Camera*	m_target;					// target for billboarding
};

#endif _FLAG_H_
