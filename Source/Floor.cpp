#include "Floor.h"

Floor::Floor()
{}

Floor::~Floor()
{}

void
Floor::Update()
{
	m_modelMatrix = XMMatrixTranslationFromVector(m_position);

	// create final model world matrix
	m_modelMatrix = m_rotationalMatrix * m_scaleMatrix * m_modelMatrix;
}
