// Floor.h
// Christopher Ball 2017
// Object class for the floor

#ifndef _FLOOR_H_
#define _FLOOR_H_

#include "GameObject.h"

// inherit from game object
class Floor : public GameObject
{
public:
	Floor();
	~Floor();
	void Update(); // update world position matrix
};

#endif _FLOOR_H_
