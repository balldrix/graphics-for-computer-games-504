#include "FloorMesh.h"
#include "Graphics.h"
#include "ConstantBuffers.h"

FloorMesh::FloorMesh()
{}

FloorMesh::~FloorMesh()
{}

void 
FloorMesh::Init(Graphics* graphics)
{
	D3D11_BUFFER_DESC bd = { 0 };
	D3D11_SUBRESOURCE_DATA subData = { 0 };

	// vertex buffer
	m_vertexCount = 4;

	NormalMapVertex verts[4];

	verts[0].position = Vector3(-1.0f, -1.0f, -1.0f);
	verts[0].normal = Vector3(0.0f, 1.0f, 0.0f);
	verts[0].uv = Vector2(0.0f, 1.0f);
	verts[0].tangent = Vector3(1.0f, 0.0f, 0.0f);
	verts[0].binormal = verts[0].tangent.Cross(verts[0].normal);

	verts[1].position = Vector3(1.0f, -1.0f, -1.0f);
	verts[1].normal = Vector3(0.0f, 1.0f, 0.0f);
	verts[1].uv = Vector2(1.0f, 1.0f);
	verts[1].tangent = Vector3(1.0f, 0.0f, 0.0f);
	verts[1].binormal = verts[1].tangent.Cross(verts[1].normal);

	verts[2].position = Vector3(1.0f, -1.0f, 1.0f);
	verts[2].normal = Vector3(0.0f, 1.0f, 0.0f);
	verts[2].uv = Vector2(1.0f, 0.0f);
	verts[2].tangent = Vector3(1.0f, 0.0f, 0.0f);
	verts[2].binormal = verts[2].tangent.Cross(verts[2].normal);

	verts[3].position = Vector3(-1.0f, -1.0f, 1.0f);
	verts[3].normal = Vector3(0.0f, 1.0f, 0.0f);
	verts[3].uv = Vector2(0.0f, 0.0f);
	verts[3].tangent = Vector3(1.0f, 0.0f, 0.0f);
	verts[3].binormal = verts[0].tangent.Cross(verts[0].normal);

	// index buffer
	WORD indices[] =
	{
		0, 2, 1,
		3, 2, 0
	};

	m_indexCount = ARRAYSIZE(indices);

	// create vertex buffer for a cube
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(NormalMapVertex) * m_vertexCount;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	subData = { verts, 0, 0 };
	HRESULT result = graphics->GetDevice()->CreateBuffer(&bd, &subData, &m_vertexBuffer);

	// create index buffer
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(short) * m_indexCount;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	subData = { indices, 0, 0 };
	graphics->GetDevice()->CreateBuffer(&bd, &subData, &m_indexBuffer);

	// set stride
	m_stride = sizeof(NormalMapVertex);
}
