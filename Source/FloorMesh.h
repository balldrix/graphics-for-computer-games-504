// FloorMesh.h
// Christopher Ball 2017
// Mesh information for the Ground

#ifndef _FLOORMESH_H_
#define _FLOORMESH_H_

#include "MeshObject.h"

// forward declarations
class Graphics;

// inherit from game object
class FloorMesh : public MeshObject
{
public:
	FloorMesh();
	~FloorMesh();
	void Init(Graphics* graphics); // initilalise floor mesh
};

#endif _FLOORMESH_H_