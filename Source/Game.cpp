#include "Game.h"

#include "pch.h"

#include "Graphics.h"
#include "Input.h"
#include "Player.h"

#include "LevelManager.h"
#include "WaypointManager.h"
#include "NPCManager.h"

#include "Camera.h"

#include "Level.h"

#include <xaudio2.h>

#include "Shader.h"
#include "Texture.h"
#include "Material.h"

#include "SphereMesh.h"
#include "CubeMesh.h"
#include "FloorMesh.h"
#include "RampMesh.h"
#include "QuadMesh.h"
#include "ParticleMesh.h"

#include "MazeBlock.h"
#include "Floor.h"
#include "Ramp.h"

#include "NPC.h"
#include "Bullet.h"

#include "Flag.h"

#include "Hud.h"

#include "PostProcessing.h"

#include "Particle.h"
#include "ParticleEmitter.h"
#include "ParticleSystem.h"

#include "ConstantBuffers.h"
#include "Constants.h"
#include "Controls.h"
#include "Randomiser.h"

#include "Error.h"

Game::Game() :
m_graphics(nullptr),
m_input(nullptr),
m_player(nullptr),
m_levelManager(nullptr),
m_waypointManager(nullptr),
m_NPCManager(nullptr),
m_thirdPersonCamera(nullptr),
m_spriteBatch(nullptr),
m_simpleShader(nullptr),
m_specularShader(nullptr),
m_normalMapShader(nullptr),
m_particleShader(nullptr),
m_occluderShader(nullptr),
m_wallTexture(nullptr),
m_pathTexture(nullptr),
m_pathNormalMap(nullptr),
m_groundTexture(nullptr),
m_rampTexture(nullptr),
m_rampNormalMap(nullptr),
m_blueTexture(nullptr),
m_redTexture(nullptr),
m_yellowTexture(nullptr),
m_flagTexture(nullptr),
m_explosionCentreTex(nullptr),
m_explosionParticleTex(nullptr),
m_smokeTexture(nullptr),
m_fireTexture(nullptr),
m_sparkTexture(nullptr),
m_wallMaterial(nullptr),
m_pathMaterial(nullptr),
m_groundMaterial(nullptr),
m_rampMaterial(nullptr),
m_blueMetalicMaterial(nullptr),
m_redMetalicMaterial(nullptr),
m_yellowMaterial(nullptr),
m_flagMaterial(nullptr),
m_explosionCentreMat(nullptr),
m_explosionParticleMat(nullptr),
m_smokeMaterial(nullptr),
m_occluderMaterial(nullptr),
m_cubeMesh(nullptr),
m_floorMesh(nullptr),
m_rampMesh(nullptr),
m_characterMesh(nullptr),
m_bulletMesh(nullptr),
m_flagMesh(nullptr),
m_wallBlock(nullptr),
m_path(nullptr),
m_ground(nullptr),
m_ramp(nullptr),
m_bullet(nullptr),
m_flag(nullptr),
m_hud(nullptr),
m_postProcessing(nullptr),
m_particleSystem(nullptr),
m_explosionCentre(nullptr),
m_explosionParticle(nullptr),
m_smokeParticle(nullptr),
m_fireParticle(nullptr),
m_sparkParticle(nullptr),
m_timerFreq(0.0f),
m_currentTime(0.0f),
m_previousTime(0.0f),
m_keyDelay(0.0f),
m_keyPressed(false),
m_currentLevel(0),
m_blurEffect(false),
m_blurTimer(0.0f),
m_sparksTimer(0.0f),
m_sparksFlag(true)
{
}

Game::~Game()
{
	DeleteAll(); // delete all pointers
}

void
Game::Init(Graphics* graphics)
{
	m_graphics = graphics; // copy pointer address to 

	// create new input class
	m_input = new Input();

	// create new first person camera
	m_thirdPersonCamera = new Camera();

	// create new levelmanager
	m_levelManager = new LevelManager();

	// create new waypoint manager
	m_waypointManager = new WaypointManager();

	// create new NPC manager
	m_NPCManager = new NPCManager(m_waypointManager);

	// allow multi threading for audio engine
	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	// initialse sprite batch engine
	m_spriteBatch = new SpriteBatch(m_graphics->GetDeviceContext());

	// create particle system
	m_particleSystem = new ParticleSystem();

	// load all assets
	LoadAssets();

	// start new game
	NewGame();
}

void
Game::Run()
{
	m_currentTime = m_timer.GetTicks(); // get cpu tick count
	float deltaTime = (m_currentTime - m_previousTime) * m_timerFreq; // calculate time taken since last update
	m_previousTime = m_currentTime; // keep current time for next update

	// if delta time becomes too large
	// lock at 60fps
	if(deltaTime > 0.016f)
	{
		deltaTime = 0.016f;
	}

	ProcessInput(deltaTime); // read key and mouse input into game
	Update(deltaTime); // update game
	Render(); // render objects	
}

void 
Game::ProcessInput(float deltaTime)
{
	// if esc key is pressed
	if(m_input->IsKeyDown(Controls::escKey))
	{
		PostQuitMessage(0); // quit game
	}

	/////////////////////////////////////////////////////////////
	// press keys

	// if player presses key to zoom in or out
	if(m_input->IsKeyDown(Controls::zoomIn))
	{
		m_thirdPersonCamera->SetZoom(-PlayerConstants::ZOOM_SPEED *
									deltaTime);
	}
	else if(m_input->IsKeyDown(Controls::zoomOut))
	{
		m_thirdPersonCamera->SetZoom(PlayerConstants::ZOOM_SPEED *
									 deltaTime);
	}

	// reset zoom
	if(m_input->IsKeyDown(Controls::resetCamera))
	{
		m_thirdPersonCamera->Reset();
	}

	// if player presses key to rotate left or right
	if(m_input->IsKeyDown(Controls::rotateViewLeft))
	{
		m_thirdPersonCamera->SetAngle(PlayerConstants::TURNING_SPEED * deltaTime);
	}
	else if(m_input->IsKeyDown(Controls::rotateViewRight))
	{
		m_thirdPersonCamera->SetAngle(-PlayerConstants::TURNING_SPEED * deltaTime);
	}

	/////////////////////////////////////////////////////////////
	// if player presses key to move forwards or backwards
	if(m_input->IsKeyDown(Controls::playerFowards))
	{
		m_player->SetVelocity(m_player->GetFacing() * PlayerConstants::MOVEMENT_SPEED); // set z velocity
	}
	else if(m_input->IsKeyDown(Controls::playerBackwards))
	{
		m_player->SetVelocity((m_player->GetFacing() * -1.0f) * PlayerConstants::MOVEMENT_SPEED); // set z velocity 
	}

	// if player presses key to turn left or right
	if(m_input->IsKeyDown(Controls::playerRight))
	{
		m_player->SetAngle(m_player->GetAngle() + (PlayerConstants::TURNING_SPEED * deltaTime)); // turn player right
	}
	else if(m_input->IsKeyDown(Controls::playerLeft))
	{
		m_player->SetAngle(m_player->GetAngle() - (PlayerConstants::TURNING_SPEED * deltaTime)); // turn player left
	}

	/////////////////////////////////////////////////////////////
	// release movement keys
	if(!m_input->IsKeyDown(Controls::playerFowards) &&
	   !m_input->IsKeyDown(Controls::playerBackwards))
	{
		m_player->SetVelocity(Vector3(0.0f, 0.0f, 0.0f));
	}
}

void
Game::Update(float deltaTime)
{
	////////////////////////////////////////////////////////////////
	if(m_player->IsActive())
	{
	// player collision detection
		Vector3 playerPosition = m_player->GetPosition(); // store current player position
		m_player->Update(deltaTime); // update player

		// calculate player zone for collision detection
		float xMin = m_player->GetPosition().x - GlobalConstants::CELL_WIDTH * 2;
		float zMin = m_player->GetPosition().z - GlobalConstants::CELL_WIDTH * 2;
		float zMax = m_player->GetPosition().z + GlobalConstants::CELL_WIDTH * 2;
		float xMax = m_player->GetPosition().x + GlobalConstants::CELL_WIDTH * 2;

		// create list of possible collision blocks
		std::vector<GameObject*> wallsNearPlayer;

		for(int i = 0; i < m_mazeObjects.size(); i++)
		{
			if(m_mazeObjects[i]->GetPosition().x > xMin &&
			   m_mazeObjects[i]->GetPosition().x < xMax &&
			   m_mazeObjects[i]->GetPosition().z > zMin &&
			   m_mazeObjects[i]->GetPosition().z < zMax)
			{
				if(m_mazeObjects[i]->GetID() == "WALL")
				{
					wallsNearPlayer.push_back(m_mazeObjects[i]);
				}
			}
		}

		//////////////////////////////////////////////////////////////////
		// player and npc collision
		for(int i = 0; i < m_NPCManager->GetNumNPCs(); i++)
		{
			NPC* tempNPC = m_NPCManager->GetNPC(i);
			Vector3 npcPosition = tempNPC->GetPosition();
			tempNPC->Update(deltaTime);

			// calculate npc zone for collision detection
			float xMin = tempNPC->GetPosition().x - GlobalConstants::CELL_WIDTH * 2;
			float xMax = tempNPC->GetPosition().x + GlobalConstants::CELL_WIDTH * 2;
			float zMin = tempNPC->GetPosition().z - GlobalConstants::CELL_WIDTH * 2;
			float zMax = tempNPC->GetPosition().z + GlobalConstants::CELL_WIDTH * 2;

			std::vector<GameObject*> wallsNearNPC;

			// create list of possible collision blocks
			for(int i = 0; i < m_mazeObjects.size(); i++)
			{
				if(m_mazeObjects[i]->GetPosition().x > xMin &&
				   m_mazeObjects[i]->GetPosition().x < xMax &&
				   m_mazeObjects[i]->GetPosition().z > zMin &&
				   m_mazeObjects[i]->GetPosition().z < zMax)
				{
					if(m_mazeObjects[i]->GetID() == "WALL")
					{
						wallsNearNPC.push_back(m_mazeObjects[i]);
					}
				}
			}

			// if player collides with npc
			if(tempNPC->GetHitBox().SphereSphereCollision(tempNPC->GetSphereCollider(),
			   m_player->GetSphereCollider()))
			{
				// damage player health and move
				// both npc and player away from each other 
				m_player->SetHealth(m_player->GetHealth() - PlayerConstants::MAX_HEALTH * 0.167777f);

				Vector3 direction = m_player->GetPosition() - tempNPC->GetPosition();
				direction.Normalize();

				Vector3 newplayerPosition = m_player->GetPosition() + direction;
				bool bounce = true;

				for(int i = 0; i < wallsNearPlayer.size(); i++)
				{
					if(wallsNearPlayer[i]->GetHitBox().BoxSphereCollision(newplayerPosition, m_player->GetSphereCollider().radius))
					{
						bounce = false;
						break;
					}
				}
				if(bounce)
				{
					m_player->SetPosition(newplayerPosition);
				}
				else
				{
					m_player->SetPosition(playerPosition);
				}

				m_player->SetVelocity(Vector3(0.0f, 0.0f, 0.0f));

				Vector3 newNPCPosition = tempNPC->GetPosition() - direction;
				bounce = true;

				for(int i = 0; i < wallsNearNPC.size(); i++)
				{
					if(wallsNearNPC[i]->GetHitBox().BoxSphereCollision(newNPCPosition, tempNPC->GetSphereCollider().radius))
					{
						bounce = false;
						break;
					}
				}

				if(bounce)
				{
					tempNPC->SetPosition(newNPCPosition);
				}
				else
				{
					tempNPC->SetPosition(npcPosition);
				}

				// turn blur on
				m_blurEffect = true;


				if(m_sparksFlag)
				{			
					// add sparks
					m_particleSystem->GetEmitter("SPARKS")->Begin(m_player->GetPosition() - (direction * m_player->GetSphereCollider().radius));// - tempNPC->GetPosition());
					m_sparksFlag = false;
				}
			}

			// do ray cast to player if in range
			// to check line of sight
			Vector3 distance = tempNPC->GetPosition() - m_player->GetPosition();

			if(distance.Length() < NPCConstants::NPC_VIEWRANGE)
			{
				// loop though maze objects
				for(unsigned int j = 0; j < m_mazeObjects.size(); j++)
				{
					// check if object is a wall
					if(m_mazeObjects[j]->GetID() == "WALL")
					{
						// check raycast from NPC to player for maze objects in the way
						if(RaycastToAABB(tempNPC->GetPosition(),
						   m_player->GetPosition(),
						   *m_mazeObjects[j]))
						{
							// if ray hits maze object player is hidden
							// from NPC line of sight
							tempNPC->SetLineOfSight(false);
							break;
						}
						tempNPC->SetLineOfSight(true); // if no object is in the way player isn't hidden
					}
				}
			}
			else
			{
				tempNPC->SetLineOfSight(false);
			}

			//////////////////////////////////
			// check bullet collision
			Bullet* bullet = tempNPC->GetBullet();

			if(bullet->IsActive())
			{
				m_particleSystem->GetEmitter("FIRE_SMOKE")->SetPosition(bullet->GetPosition() + Vector3(0.0f, 0.5f, 0.0f));
				m_particleSystem->GetEmitter("FIRE")->SetPosition(bullet->GetPosition() + Vector3(0.0f, 0.1f, 0.0f));

				if(bullet->GetHitBox().SphereSphereCollision(
					bullet->GetSphereCollider(),
					m_player->GetSphereCollider()))
				{
					// damage and move player, kill bullet
					m_player->SetHealth(m_player->GetHealth() - PlayerConstants::MAX_HEALTH * 0.167777f);

					// calculate bounce direction
					Vector3 direction = m_player->GetPosition() - bullet->GetPosition();
					direction.Normalize();

					// store new position
					Vector3 newPosition = m_player->GetPosition() + direction;
					bool bounce = true; // won't always bounce

					// check for wall collision before bouncing
					for(int i = 0; i < wallsNearPlayer.size(); i++)
					{
						if(wallsNearPlayer[i]->GetHitBox().BoxSphereCollision(newPosition, m_player->GetSphereCollider().radius))
						{
							bounce = false;
							break;
						}
					}

					if(bounce)
					{
						m_player->SetPosition(newPosition);
					}
					else
					{
						m_player->SetPosition(playerPosition);
					}

					// remove bullet
					bullet->Kill();
					bullet->Update(deltaTime);
					m_particleSystem->GetEmitter("FIRE_SMOKE")->End();
					m_particleSystem->GetEmitter("FIRE")->End();

					// turn blur on
					m_blurEffect = true;
				}

				// calculate bullet zone for collision detection
				float xMin = bullet->GetPosition().x - GlobalConstants::CELL_WIDTH * 2;
				float zMin = bullet->GetPosition().z - GlobalConstants::CELL_WIDTH * 2;
				float zMax = bullet->GetPosition().z + GlobalConstants::CELL_WIDTH * 2;
				float xMax = bullet->GetPosition().x + GlobalConstants::CELL_WIDTH * 2;

				// create list of possible collision blocks
				std::vector<GameObject*> wallsNearBullet;

				for(int i = 0; i < m_mazeObjects.size(); i++)
				{
					if(m_mazeObjects[i]->GetPosition().x > xMin &&
					   m_mazeObjects[i]->GetPosition().x < xMax &&
					   m_mazeObjects[i]->GetPosition().z > zMin &&
					   m_mazeObjects[i]->GetPosition().z < zMax)
					{
						if(m_mazeObjects[i]->GetID() == "WALL")
						{
							wallsNearBullet.push_back(m_mazeObjects[i]);
						}
					}
				}

				// check for collision with bullet and walls
				for(int i = 0; i < wallsNearBullet.size(); i++)
				{
					if(wallsNearBullet[i]->GetHitBox().BoxSphereCollision(bullet->GetSphereCollider().position,
					   bullet->GetSphereCollider().radius))
					{
						bullet->Kill(); // remove bullet
						bullet->Update(deltaTime);
						m_particleSystem->GetEmitter("FIRE_SMOKE")->End();
						m_particleSystem->GetEmitter("FIRE")->End();
					}
				}
			}

			////////////////////////////////////////////////////////////////
			// NPC collision detection with wall
			for(int i = 0; i < wallsNearNPC.size(); i++)
			{
				if(wallsNearNPC[i]->GetHitBox().BoxSphereCollision(tempNPC->GetPosition(), tempNPC->GetSphereCollider().radius))
				{
					tempNPC->SetPosition(npcPosition); // set player position to previous position
					tempNPC->SetVelocity(Vector3(0.0f, 0.0f, 0.0f)); // reset velocity
				}
			}

			// loop through list to check player collision with walls
			for(int i = 0; i < wallsNearPlayer.size(); i++)
			{
				//if collision occurs set player position to Vector3 playerPosition
				if(wallsNearPlayer[i]->GetHitBox().BoxSphereCollision(m_player->GetPosition(), m_player->GetSphereCollider().radius))
				{
					m_player->SetPosition(playerPosition); // set player position to previous position
					m_player->SetVelocity(Vector3(0.0f, 0.0f, 0.0f)); // reset velocity
					
					if(m_sparksFlag)
					{
						Vector3 direction = m_player->GetPosition() - wallsNearPlayer[i]->GetPosition();
						direction.Normalize();

						// add sparks
						m_particleSystem->GetEmitter("SPARKS")->Begin(m_player->GetPosition() - (direction * m_player->GetSphereCollider().radius));
						m_sparksFlag = false;
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////
		// camera 
		m_thirdPersonCamera->SetView(m_player->GetPosition(),
									 m_player->GetAngle(),
									 Vector3(0.0f, 1.0f, 0.0f));

		m_thirdPersonCamera->SetPerspective(XM_PIDIV4,
											m_graphics->GetWidth() / m_graphics->GetHeight(),
											0.1f,
											100.0f); // set frustum

		m_thirdPersonCamera->Update(); // update camera
		m_graphics->SetCBufferCamera(m_thirdPersonCamera->GetEyePosition());

		////////////////////////////////////////////////////////////////
		// update flag rotation
		for(int i = 0; i < m_flagsList.size(); i++)
		{
			m_flagsList[i]->Update(deltaTime);
		}

		////////////////////////////////////////////////////////////////
		// flag post detection
		Vector3 distance = m_player->GetPosition() - m_waypointManager->GetWaypoint("FLAG_POST_000")->position;

		// if player is near ramp flag post, move to second level
		if(distance.Length() < 0.5)
		{
			m_currentLevel++;
			m_player->SetPosition(m_waypointManager->GetWaypoint("FLAG_POST_001")->position);
		}

		distance = m_player->GetPosition() - m_waypointManager->GetWaypoint("MAZE_END")->position;
		// if player is near to final flag post
		if(distance.Length() < 0.5)
		{
			// show player wins screen
			MessageBox(m_graphics->GetHwnd(), L"Congratulations! You made it to the end of the maze", L"You Win!", MB_OK);
			PostQuitMessage(0); // quit game
		}

		////////////////////////////////////////////////////////////////
		// health and life check
		if(m_player->GetHealth() <= 0)
		{
			// kill player
			KillPlayer();
		}

		if(m_player->GetLives() == 0)
		{
			// player has run out of lives
			MessageBox(m_graphics->GetHwnd(), L"Unlucky, you ran out of lives", L"Game Over!", MB_OK);
			PostQuitMessage(0); // quit game
		}
	}

	////////////////////////////////////////////////////////////////
	// Hud
	m_hud->SetHealth(m_player->GetHealth());
	m_hud->SetLives(m_player->GetLives());

	////////////////////////////////////////////////////////////////
	// particle system
	m_particleSystem->Update(deltaTime);

	////////////////////////////////////////////////////////////////
	// timers
	if(m_keyDelay > KEY_PRESS_DELAY)
	{	
		// key delay is greater than timer amount
		// reset delay timer and set 
		// key pressed boolean to false
		// to allow the keys to be pressed next frame
		m_keyDelay = 0;
		m_keyPressed = false;
	}
	m_keyDelay += deltaTime; // increase key delay timer

	// blur timer
	if(m_blurEffect)
	{
		m_blurTimer += deltaTime; // increase blur timer
	}

	if(m_blurTimer > GlobalConstants::BLUR_TIMER)
	{
		m_blurEffect = false;
		m_blurTimer = 0.0f;
	}

	// sparks timer
	if(!m_sparksFlag)
	{
		m_sparksTimer += deltaTime;
	}

	if(m_sparksTimer > 0.3f)
	{
		m_sparksTimer = 0.0f;
		m_sparksFlag = true;
	}

	if(!m_player->IsActive())
	{
		if(!m_particleSystem->GetEmitter("EXPLOSION_SMOKE2")->IsActive())
		{
			// reset player to start of level
			m_player->Reset();
			m_player->SetPosition(m_waypointManager->GetWaypoint(m_currentLevel)->position);
			m_player->SetLives(m_player->GetLives() - 1);
			m_player->SetActive(true);
		}
	}
}

void
Game::Render()
{
	m_graphics->SetViewMatrix(m_thirdPersonCamera->GetViewMatrix()); // set view matrix
	m_graphics->SetProjectionMatrix(m_thirdPersonCamera->GetProjectionMatrix()); // set projection matrix

	// clear backbuffer and depth buffer
	m_graphics->ClearBackbuffer();
	m_graphics->ClearDepthBuffer();

	// set alpha blending
	m_graphics->SetAlphaBlending();

	RenderShadowMap();

	// set scene
	m_graphics->BeginScene();

	// reset target view
	m_graphics->ResetRenderTarget();

	// set standard viewport
	m_graphics->SetStandardViewPort();

	if(m_blurEffect)
	{
		// set texture for render target
		// to allow post processing
		m_postProcessing->Begin();

		// render scene
		RenderScene();

		// process effects
		m_postProcessing->Process();
			
		// reset target view
		m_graphics->ResetRenderTarget();

		// render post processing
		m_postProcessing->Render();
	}
	else
	{
		// render scene
		RenderScene();
	}

	// render hud sprites
	RenderHud();

	// display backbuffer on screen
	m_graphics->PresentBackBuffer();
}

void 
Game::RenderShadowMap()
{
	// set stencil view as render target
	m_graphics->SetShadowRenderTarget();

	// Set rendering state.
	m_graphics->SetShadowRenderState();

	// set light view projection
	m_graphics->SetCBufferLight(m_player->GetPosition());

	// Draw the objects.

	// render player
	m_player->RenderToShadowMap(m_graphics);

	// render NPCs
	m_NPCManager->RenderToShadowMap(m_graphics);

	// render walls
	for(int i = m_mazeObjects.size() - 1; i >= 0; i--)
	{
		m_mazeObjects[i]->RenderToShadowMap(m_graphics); // render object
	}
}

void 
Game::RenderScene()
{
	// render player
	if(m_player->IsActive())
	{
		m_player->Render(m_graphics);
	}

	// render maze
	// loop through object lists and call render function
	for(unsigned int i = 0; i < m_mazeObjects.size(); i++)
	{
		m_mazeObjects[i]->Render(m_graphics); // render object
	}

	for(unsigned int i = 0; i < m_floorList.size(); i++)
	{
		m_floorList[i]->Render(m_graphics); // render object
	}

	for(unsigned int i = 0; i < m_flagsList.size(); i++)
	{
		m_flagsList[i]->Render(m_graphics); // render object
	}

	// render NPCs
	m_NPCManager->Render(m_graphics);

	// render particle effects
	m_particleSystem->Render(m_graphics);
}

void Game::RenderHud()
{
	// begin sprite batch rendering
	m_spriteBatch->Begin();

	// render hud
	m_hud->Render(m_spriteBatch);

	// end sprite batch rendering
	m_spriteBatch->End();
}

void
Game::ReleaseAll()
{
	// release all pointer related release functions
	if(m_postProcessing) { m_postProcessing->Release();	}
	if(m_particleMesh) { m_particleMesh->Release(); }
	if(m_flagMesh) { m_flagMesh->Release(); }
	if(m_bulletMesh) { m_bulletMesh->Release(); }
	if(m_characterMesh) { m_characterMesh->Release(); }
	if(m_rampMesh) { m_rampMesh->Release(); }
	if(m_floorMesh) { m_floorMesh->Release(); }
	if(m_cubeMesh) { m_cubeMesh->Release(); }
	if(m_sparkTexture) { m_sparkTexture->Release(); }
	if(m_fireTexture) { m_fireTexture->Release(); }
	if(m_smokeTexture) { m_smokeTexture->Release(); }
	if(m_explosionParticleTex) { m_explosionParticleTex->Release(); }
	if(m_explosionCentreTex) { m_explosionCentreTex->Release(); }
	if(m_flagTexture) { m_flagTexture->Release(); }
	if(m_yellowTexture) { m_yellowTexture->Release(); }
	if(m_redTexture) { m_redTexture->Release(); }
	if(m_blueTexture) { m_blueTexture->Release();}
	if(m_rampNormalMap) { m_rampNormalMap->Release(); }
	if(m_rampTexture) { m_rampTexture->Release(); }
	if(m_groundNormalMap) { m_groundNormalMap->Release(); }
	if(m_groundTexture) { m_groundTexture->Release(); }
	if(m_pathNormalMap) { m_pathNormalMap->Release(); }
	if(m_pathTexture) { m_pathTexture->Release(); }
	if(m_wallTexture) { m_wallTexture->Release(); }
	if(m_occluderShader) { m_occluderShader->Release(); }
	if(m_particleShader) { m_particleShader->Release(); }
	if(m_normalMapShader) { m_normalMapShader->Release(); }
	if(m_specularShader) { m_specularShader->Release(); }
	if(m_simpleShader) { m_simpleShader->Release(); }
	if(m_graphics) { m_graphics->ReleaseAll(); }
}

void
Game::DeleteAll()
{
	// delete particles
	if(m_sparkParticle)
	{
		delete m_sparkParticle;
		m_sparkParticle = nullptr;
	}

	if(m_fireParticle)
	{
		delete m_fireParticle;
		m_fireParticle = nullptr;
	}
	
	if(m_smokeParticle)
	{
		delete m_smokeParticle;
		m_smokeParticle = nullptr;
	}

	if(m_explosionParticle)
	{
		delete m_explosionParticle;
		m_explosionParticle = nullptr;
	}

	if(m_explosionCentre)
	{
		delete m_explosionCentre;
		m_explosionCentre = nullptr;
	}
	
	// delete particle system
	if(m_particleSystem)
	{
		delete m_particleSystem;
		m_particleSystem = nullptr;
	}

	// delete post processing
	if(m_postProcessing)
	{
		delete m_postProcessing;
		m_postProcessing = nullptr;
	}

	// delete hud
	if(m_hud)
	{
		delete m_hud;
		m_hud = nullptr;
	}

	// delete bullet
	if(m_bullet)
	{
		delete m_bullet;
		m_bullet = nullptr;
	}

	// delete maze
	DeleteMaze();

	// delete meshes	
	if(m_particleMesh)
	{
		delete m_particleMesh;
		m_particleMesh = nullptr;
	}

	if(m_flagMesh)
	{
		delete m_flagMesh;
		m_flagMesh = nullptr;
	}

	if(m_bulletMesh)
	{
		delete m_bulletMesh;
		m_bulletMesh = nullptr;
	}

	if(m_characterMesh)
	{
		delete m_characterMesh;
		m_characterMesh = nullptr;
	}

	if(m_rampMesh)
	{
		delete m_rampMesh;
		m_rampMesh = nullptr;
	}

	if(m_floorMesh)
	{
		delete m_floorMesh;
		m_floorMesh = nullptr;
	}

	if(m_cubeMesh)
	{
		delete m_cubeMesh;
		m_cubeMesh = nullptr;
	}
	
	// delete materials	
	if(m_occluderMaterial)
	{
		delete m_occluderMaterial;
		m_occluderMaterial = nullptr;
	}	
	
	if(m_sparkMaterial)
	{
		delete m_sparkMaterial;
		m_sparkMaterial = nullptr;
	}

	if(m_fireMaterial)
	{
		delete m_fireMaterial;
		m_fireMaterial = nullptr;
	}

	if(m_smokeMaterial)
	{
		delete m_smokeMaterial;
		m_smokeMaterial = nullptr;
	}

	if(m_explosionParticleMat)
	{
		delete m_explosionParticleMat;
		m_explosionParticleMat = nullptr;
	}

	if(m_explosionCentreMat)
	{
		delete m_explosionCentreMat;
		m_explosionCentreMat = nullptr;
	}
	
	if(m_flagMaterial)
	{
		delete m_flagMaterial;
		m_flagMaterial = nullptr;
	}
	
	if(m_yellowMaterial)
	{
		delete m_yellowMaterial;
		m_yellowMaterial = nullptr;
	}	
	
	if(m_redMetalicMaterial)
	{
		delete m_redMetalicMaterial;
		m_redMetalicMaterial = nullptr;
	}

	if(m_blueMetalicMaterial)
	{
		delete m_blueMetalicMaterial;
		m_blueMetalicMaterial = nullptr;
	}

	if(m_rampMaterial)
	{
		delete m_rampMaterial;
		m_rampMaterial = nullptr;
	}

	if(m_groundMaterial)
	{
		delete m_groundMaterial;
		m_groundMaterial = nullptr;
	}

	if(m_pathMaterial)
	{
		delete m_pathMaterial;
		m_pathMaterial = nullptr;
	}

	if(m_wallMaterial)
	{
		delete m_wallMaterial;
		m_wallMaterial = nullptr;
	}

	// delete textures
	if(m_sparkTexture)
	{
		delete m_sparkTexture;
		m_sparkTexture = nullptr;
	}

	if(m_fireTexture)
	{
		delete m_fireTexture;
		m_fireTexture = nullptr;
	}

	if(m_smokeTexture)
	{
		delete m_smokeTexture;
		m_smokeTexture = nullptr;
	}

	if(m_explosionParticleTex)
	{
		delete m_explosionParticleTex;
		m_explosionParticleTex = nullptr;
	}

	if(m_explosionCentreTex)
	{
		delete m_explosionCentreTex;
		m_explosionCentreTex = nullptr;
	}

	if(m_flagTexture)
	{
		delete m_flagTexture;
		m_flagTexture = nullptr;
	}

	if(m_yellowTexture)
	{
		delete m_yellowTexture;
		m_yellowTexture = nullptr;
	}

	if(m_redTexture)
	{
		delete m_redTexture;
		m_redTexture = nullptr;
	}

	if(m_blueTexture)
	{
		delete m_blueTexture;
		m_blueTexture = nullptr;
	}

	if(m_rampNormalMap)
	{
		delete m_rampNormalMap;
		m_rampNormalMap = nullptr;
	}

	if(m_rampTexture)
	{
		delete m_rampTexture;
		m_rampTexture = nullptr;
	}

	if(m_groundNormalMap)
	{
		delete m_groundNormalMap;
		m_groundNormalMap = nullptr;
	}

	if(m_groundTexture)
	{
		delete m_groundTexture;
		m_groundTexture = nullptr;
	}

	if(m_pathNormalMap)
	{
		delete m_pathNormalMap;
		m_pathNormalMap = nullptr;
	}

	if(m_pathTexture)
	{
		delete m_pathTexture;
		m_pathTexture = nullptr;
	}

	if(m_wallTexture)
	{
		delete m_wallTexture;
		m_wallTexture = nullptr;
	}

	// delete shaders
	if(m_occluderShader)
	{
		delete m_occluderShader;
		m_occluderShader = nullptr;
	}

	if(m_particleShader)
	{
		delete m_particleShader;
		m_particleShader = nullptr;
	}

	if(m_normalMapShader)
	{
		delete m_normalMapShader;
		m_normalMapShader = nullptr;
	}
	
	if(m_specularShader)
	{
		delete m_specularShader;
		m_specularShader = nullptr;
	}

	if(m_simpleShader)
	{
		delete m_simpleShader;
		m_simpleShader = nullptr;
	}

	// delete 2d sprite batch engine
	if(m_spriteBatch)
	{
		delete m_spriteBatch;
		m_spriteBatch = nullptr;
	}

	// delete camera
	if(m_thirdPersonCamera)
	{
		delete m_thirdPersonCamera;
		m_thirdPersonCamera = nullptr;
	}

	// delete NPC manager
	if(m_NPCManager)
	{
		delete m_NPCManager;
		m_NPCManager = nullptr;
	}

	// delete waypoint manager
	if(m_waypointManager)
	{
		delete m_waypointManager;
		m_waypointManager = nullptr;
	}

	// delete level manager
	if(m_levelManager)
	{
		delete m_levelManager;
		m_levelManager = nullptr;
	}

	// delete player object
	if(m_player)
	{
		delete m_player;
		m_player = nullptr;
	}

	// delete input object
	if(m_input)
	{
		delete m_input;
		m_input = nullptr;
	}

	// clear graphics object pointer
	if(m_graphics)
	{
		m_graphics = nullptr;
	}

	CoUninitialize();
}

void 
Game::NewGame()
{
	// current level is 0
	m_currentLevel = 0;

	// clear waypoints and npcs
	m_waypointManager->Delete();
	m_NPCManager->DeleteAll();

	// initialise level manager
	if(!m_levelManager->Init(m_waypointManager, m_NPCManager))
	{
		char buffer[100]; // string buffer
		int d = m_levelManager->GetCurrentLevel(); // get current level number
		sprintf_s(buffer, " Error Loading Level %d in NewGame line 365 \n", d); // concatenate new string
		Error::FileLog(buffer); // log error to file

		MessageBox(m_graphics->GetHwnd(), L"Error loading level, see Logs/Error.txt", L"Error!", MB_OK); // display loading level error message
		PostQuitMessage(0); // quit game
	}

	DeleteMaze(); // delete maze wall and path object
	SpawnMaze(); // set maze wall and path objects

	InitNPCs(); // initialise npcs

	// reset player and spawn at start position
	m_player->Reset();
	m_player->SetPosition(m_waypointManager->GetWaypoint("PLAYER_WAYPOINT")->position);
	m_player->SetHealth(PlayerConstants::MAX_HEALTH);
	m_player->SetLives(PlayerConstants::MAX_LIVES);

	// initialise camera
	m_thirdPersonCamera->Init(m_player->GetPosition(),
							  m_player->GetAngle(),
							  m_graphics->GetWidth(),
							  m_graphics->GetHeight(),
							  0.1f,
							  100.0f);

	// add flag posts
	m_flag = new Flag();
	m_flag->Init(m_flagMesh, m_flagMaterial);
	m_flag->SetPosition(m_waypointManager->GetWaypoint("FLAG_POST_000")->position);
	m_flag->SetID("FLAG_POST_000");
	m_flag->SetTarget(m_thirdPersonCamera);

	m_flagsList.push_back(m_flag);

	m_flag = new Flag();
	m_flag->Init(m_flagMesh, m_flagMaterial);
	m_flag->SetPosition(m_waypointManager->GetWaypoint("MAZE_END")->position);
	m_flag->SetID("MAZE_END");
	m_flag->SetTarget(m_thirdPersonCamera);

	m_flagsList.push_back(m_flag);

	// set timer frequency
	m_timerFreq = (float)m_timer.GetFrequency();

	// seed the randoms with the current time
	Randomiser::Randomiser();

	// set key delay
	m_keyDelay = KEY_PRESS_DELAY;

	ResetMouse(); // reset mouse pos to middle of game window
}

void
Game::LoadAssets()
{
	// load shaders
	m_simpleShader = new Shader();
	m_specularShader = new Shader();
	m_normalMapShader = new Shader();
	m_particleShader = new Shader();
	m_occluderShader = new Shader();

	// load simple shader
	m_simpleShader->LoadVertexShader(m_graphics, L"Shaders\\SimpleVertexShader.cso", PNTInputElementDesc, ARRAYSIZE(PNTInputElementDesc));
	m_simpleShader->LoadPixelShader(m_graphics, L"Shaders\\DAPixelShader.cso");

	// load specular shader
	m_specularShader->LoadVertexShader(m_graphics, L"Shaders\\SimpleVertexShader.cso", PNTInputElementDesc, ARRAYSIZE(PNTInputElementDesc));
	m_specularShader->LoadPixelShader(m_graphics, L"Shaders\\SpecularPixelShader.cso");

	// load normal mapping shader
	m_normalMapShader->LoadVertexShader(m_graphics, L"Shaders\\NormalMapVertexShader.cso", PNTTBInputElementDesc, ARRAYSIZE(PNTTBInputElementDesc));
	m_normalMapShader->LoadPixelShader(m_graphics, L"Shaders\\NormalMapPixelShader.cso");

	// load particle shader
	m_particleShader->LoadVertexShader(m_graphics, L"Shaders\\ParticleVertexShader.cso", PTInputElementDesc, ARRAYSIZE(PTInputElementDesc));
	m_particleShader->LoadPixelShader(m_graphics, L"Shaders\\ParticlePixelShader.cso");

	m_occluderShader->LoadVertexShader(m_graphics, L"Shaders\\OccluderVertexShader.cso", PNTInputElementDesc, ARRAYSIZE(PNTInputElementDesc));
	m_occluderShader->LoadPixelShader(m_graphics, L"Shaders\\OccluderPixelShader.cso");

	// create new texture memory
	m_wallTexture = new Texture();
	m_pathTexture = new Texture();
	m_pathNormalMap = new Texture();
	m_groundTexture = new Texture();
	m_groundNormalMap = new Texture();
	m_rampTexture = new Texture();
	m_rampNormalMap = new Texture();
	m_blueTexture = new Texture();
	m_redTexture = new Texture();
	m_yellowTexture = new Texture();
	m_flagTexture = new Texture();
	m_explosionCentreTex = new Texture();
	m_explosionParticleTex = new Texture();
	m_smokeTexture = new Texture();
	m_fireTexture = new Texture();
	m_sparkTexture = new Texture();

	// load textures
	m_wallTexture->LoadTexture(m_graphics, "Assets\\Textures\\wood.png");
	m_pathTexture->LoadTexture(m_graphics, "Assets\\Textures\\path.png");
	m_pathNormalMap->LoadTexture(m_graphics, "Assets\\NormalMaps\\pathNormal.png");
	m_groundTexture->LoadTexture(m_graphics, "Assets\\Textures\\grass.png");
	m_groundNormalMap->LoadTexture(m_graphics, "Assets\\NormalMaps\\grassNormal.png");
	m_rampTexture->LoadTexture(m_graphics, "Assets\\Textures\\ramp.png");
	m_rampNormalMap->LoadTexture(m_graphics, "Assets\\NormalMaps\\rampNormal.png");
	m_blueTexture->LoadTexture(m_graphics, "Assets\\Textures\\player.png");
	m_redTexture->LoadTexture(m_graphics, "Assets\\Textures\\NPC.png");
	m_yellowTexture->LoadTexture(m_graphics, "Assets\\Textures\\bullet.png");
	m_flagTexture->LoadTexture(m_graphics, "Assets\\Textures\\finish.png");
	m_explosionCentreTex->LoadTexture(m_graphics, "Assets\\Particles\\explosionCentre.png");
	m_explosionParticleTex->LoadTexture(m_graphics, "Assets\\Particles\\explosionParticle.png");
	m_smokeTexture->LoadTexture(m_graphics, "Assets\\Particles\\smoke.png");
	m_fireTexture->LoadTexture(m_graphics, "Assets\\Particles\\fire.png");
	m_sparkTexture->LoadTexture(m_graphics, "Assets\\Particles\\spark.png");

	// create new material memory
	m_wallMaterial = new Material();
	m_pathMaterial = new Material();
	m_groundMaterial = new Material();
	m_rampMaterial = new Material();
	m_blueMetalicMaterial = new Material();
	m_redMetalicMaterial = new Material();
	m_yellowMaterial = new Material();
	m_flagMaterial = new Material();
	m_explosionCentreMat = new Material();
	m_explosionParticleMat = new Material();
	m_smokeMaterial = new Material();
	m_fireMaterial = new Material();
	m_sparkMaterial = new Material();
	m_occluderMaterial = new Material();

	// initialise materials
	m_wallMaterial->Init(m_wallTexture, m_simpleShader);
	m_pathMaterial->Init(m_pathTexture, m_pathNormalMap, m_normalMapShader);
	m_groundMaterial->Init(m_groundTexture, m_groundNormalMap, m_normalMapShader);
	m_rampMaterial->Init(m_rampTexture, m_rampNormalMap, m_normalMapShader);
	m_redMetalicMaterial->Init(m_redTexture, m_specularShader);
	m_blueMetalicMaterial->Init(m_blueTexture, m_specularShader);
	m_yellowMaterial->Init(m_yellowTexture, m_simpleShader);
	m_flagMaterial->Init(m_flagTexture, m_simpleShader);
	m_explosionCentreMat->Init(m_explosionCentreTex, m_particleShader);
	m_explosionParticleMat->Init(m_explosionParticleTex, m_particleShader);
	m_smokeMaterial->Init(m_smokeTexture, m_particleShader);
	m_fireMaterial->Init(m_fireTexture, m_particleShader);
	m_sparkMaterial->Init(m_sparkTexture, m_particleShader);
	m_occluderMaterial->Init(m_occluderShader);

	// create memory for mesh data
	m_cubeMesh = new CubeMesh();
	m_floorMesh = new FloorMesh();
	m_rampMesh = new RampMesh();
	m_characterMesh = new SphereMesh();
	m_bulletMesh = new SphereMesh(0.2f); // bullet is smaller than npc and player
	m_flagMesh = new QuadMesh();
	m_particleMesh = new ParticleMesh();
	
	// initialise mesh data
	m_cubeMesh->Init(m_graphics);
	m_floorMesh->Init(m_graphics);
	m_rampMesh->Init(m_graphics);
	m_characterMesh->Init(m_graphics);
	m_bulletMesh->Init(m_graphics);
	m_flagMesh->Init(m_graphics);
	m_particleMesh->Init(m_graphics);

	// create particles
	m_explosionCentre = new Particle();
	m_explosionParticle = new Particle();
	m_smokeParticle = new Particle();
	m_fireParticle = new Particle();
	m_sparkParticle = new Particle();

	// initialise particles
	m_explosionCentre->Init(m_particleMesh, m_explosionCentreMat);
	m_explosionParticle->Init(m_particleMesh, m_explosionParticleMat);
	m_smokeParticle->Init(m_particleMesh, m_smokeMaterial);
	m_fireParticle->Init(m_particleMesh, m_fireMaterial);
	m_sparkParticle->Init(m_particleMesh, m_sparkMaterial);

	// setup particle emmiters
	ParticleEmitter* smokeEmitter = new ParticleEmitter();
	smokeEmitter->Init(m_thirdPersonCamera,
					   "EXPLOSION_SMOKE", m_smokeParticle,
					   SPHERE,
					   0.5f, 1.8f,
					   true, 0.0,
					   30, 30,
					   0.2f, 1.8f,
					   0.2f, false);
	m_particleSystem->AddParticleEffect(smokeEmitter);

	ParticleEmitter* smokeEmitter2 = new ParticleEmitter();
	smokeEmitter2->Init(m_thirdPersonCamera,
						"EXPLOSION_SMOKE2", m_smokeParticle,
						RING,
						0.5f, 0.5f,
						true, 0.0,
						5, 5,
						0.2f, 2.8f,
						0.4f, false);

	m_particleSystem->AddParticleEffect(smokeEmitter2);

	ParticleEmitter* explosionParts = new ParticleEmitter();
	explosionParts->Init(m_thirdPersonCamera,
						 "EXPLOSION_PARTS", m_explosionParticle,
						 SPHERE,
						 0.1f, 5.0f,
						 false, 0.1f,
						 30, 30,
						 0.2f, 1.2f,
						 0.1f, false);
	m_particleSystem->AddParticleEffect(explosionParts);

	ParticleEmitter* explosionCentre = new ParticleEmitter();
	explosionCentre->Init(m_thirdPersonCamera,
						  "EXPLOSION_CENTRE", m_explosionCentre,
						  NONE,
						  1.8f, 0.0f,
						  true, 0.0f,
						  1, 1,
						  0.1f, 0.5f,
						  0.0f, false);
	m_particleSystem->AddParticleEffect(explosionCentre);

	ParticleEmitter* smokeEmitter3 = new ParticleEmitter();
	smokeEmitter3->Init(m_thirdPersonCamera,
						"FIRE_SMOKE", m_smokeParticle,
						RING,
						0.6f, 0.8f,
						true, 0.0,
						0, 30,
						0.05f, 2.0f,
						0.1f, true);

	m_particleSystem->AddParticleEffect(smokeEmitter3);

	ParticleEmitter* fire = new ParticleEmitter();
	fire->Init(m_thirdPersonCamera,
			   "FIRE", m_fireParticle,
			   LINE,
			   0.4f, 1.5f,
			   false, 0.0f,
			   1, 50,
			   0.005f, 0.1f,
			   0.0f, true);
	m_particleSystem->AddParticleEffect(fire);

	ParticleEmitter* sparks = new ParticleEmitter();
	sparks->Init(m_thirdPersonCamera,
				 "SPARKS", m_sparkParticle,
				 SPHERE,
				 0.2f, 25.0f,
				 true, 0.1f,
				 150, 150,
				 0.1f, 0.8f,
				 0.1f, false);
	m_particleSystem->AddParticleEffect(sparks);

	// create new player object
	m_player = new Player();
	m_player->Init(m_characterMesh, m_blueMetalicMaterial);
	m_player->SetOccluderMaterial(m_occluderMaterial);
	m_player->SetID("PLAYER");

	// initialise hud
	m_hud = new Hud();
	m_hud->Init(m_graphics, m_spriteBatch);

	// initialise post processing
	m_postProcessing = new PostProcessing();
	m_postProcessing->Init(m_graphics, m_spriteBatch);
}

void
Game::ResetGame()
{
	// reset all game variables to play a new game
	m_input->ClearKeysPressed();

	// set timer frequency
	m_timerFreq = (float)m_timer.GetFrequency();

	// reseed randomness
	Randomiser::Randomiser();

	// reset key delay
	m_keyDelay = KEY_PRESS_DELAY;
	m_keyPressed = false;
}

void 
Game::EndGame()
{
}

void
Game::SpawnMaze()
{	
	// add ground tiles to level
	// must be bigger than maze so no 
	// blank space can be seen by player
	for(int z = 0; z < (MAZE_DEPTH + 16); z++)
	{
		for(int x = 0; x < (MAZE_WIDTH + 16); x++)
		{
			if(x < 8 || x > MAZE_WIDTH + 7 ||
				z < 8 || z > MAZE_DEPTH + 7)
			{
				m_ground = new Floor();
				m_ground->Init(m_floorMesh,
								m_groundMaterial,
								GridToWorldSpace(x - 8, z - 8));
				//m_ground->SetOccluderMaterial(m_occluderMaterial);

					// update path position
				m_ground->Update();

				// add path to list of 
				m_floorList.push_back(m_ground);
			}
		}
	}

	// add an object to the renderable list
	// for each object needed in the maze
	for(unsigned int z = 0; z < MAZE_DEPTH; z++)
	{
		for(unsigned int x = 0; x < MAZE_WIDTH; x++)
		{
			// for every wall value
			if(m_levelManager->GetLevel()->GetGroundFloorMap(x, z) == CellType::WALL)
			{
				// initialise cube object
				m_wallBlock = new MazeBlock();
				m_wallBlock->Init(m_cubeMesh,
								  m_wallMaterial,
								  GridToWorldSpace(x, z));

				m_wallBlock->SetID("WALL");
				m_wallBlock->SetOccluderMaterial(m_occluderMaterial);

				// update wall block matrix with new position
				m_wallBlock->Update();

				// add cube to list of objects to render
				m_mazeObjects.push_back(m_wallBlock);

				// set pathfinding array
				m_map[(z * MAZE_WIDTH) + x] = false;
			}
			else if(m_levelManager->GetLevel()->GetGroundFloorMap(x, z) == CellType::PATH)
			{
				// setup Path object
				m_path = new Floor();
				m_path->Init(m_floorMesh,
							 m_pathMaterial,
							 GridToWorldSpace(x, z));
				//m_path->SetOccluderMaterial(m_occluderMaterial);

				// update path position
				m_path->Update();

				// add path to list of 
				m_mazeObjects.push_back(m_path);

				// set pathfinding array
				m_map[(z * MAZE_WIDTH) + x] = true;
			}
			else if(m_levelManager->GetLevel()->GetGroundFloorMap(x, z) == CellType::RAMP)
			{
				// initialise ramp object
				m_ramp = new Ramp();
				m_ramp->Init(m_rampMesh,
							 m_rampMaterial,
							 GridToWorldSpace(x, z));
				m_ramp->SetID("RAMP");
				m_ramp->SetAngle(XM_PIDIV2 * 3); // rotate to face right
				//m_ramp->SetOccluderMaterial(m_occluderMaterial);
				m_ramp->Update();

				// push ramp to list
				m_mazeObjects.push_back(m_ramp);

				// add empty block above ramp to stop player moving away from 2nd floor
				m_wallBlock = new MazeBlock();

				m_wallBlock->Init(nullptr,
								  nullptr,
								  GridToWorldSpace(x, 1.0f, z));
				m_wallBlock->SetID("WALL");
				m_wallBlock->Update();

				// push ramp to list
				m_mazeObjects.push_back(m_wallBlock);

				// set pathfinding array
				m_map[(z * MAZE_WIDTH) + x] = false;
			}
			
			if(m_levelManager->GetLevel()->GetSecondFloorMap(x, z) == CellType::WALL)
			{
				// initialise cube object
				m_wallBlock = new MazeBlock();
				m_wallBlock->Init(m_cubeMesh,
								  m_wallMaterial,
								  GridToWorldSpace(x, 1.0f, z));

				m_wallBlock->SetID("WALL");
				m_wallBlock->SetOccluderMaterial(m_occluderMaterial);

				// update wall block matrix with new position
				m_wallBlock->Update();

				// add cube to list of objects to render
				m_mazeObjects.push_back(m_wallBlock);

				// set pathfinding array
				m_map[(z * MAZE_WIDTH) + x] = false;
			}
			else if(m_levelManager->GetLevel()->GetSecondFloorMap(x, z) == CellType::PATH)
			{
				// setup Path object
				m_path = new Floor();
				m_path->Init(m_floorMesh,
							 m_pathMaterial,
							 GridToWorldSpace(x, 1.0f, z));
				//m_path->SetOccluderMaterial(m_occluderMaterial);

				// update path position
				m_path->Update();

				// add path to list of 
				m_mazeObjects.push_back(m_path);

				// set pathfinding array
				m_map[(z * MAZE_WIDTH) + x] = true;
			}
		}
	}	
}

void 
Game::InitNPCs()
{
	for(int i = 0; i < m_NPCManager->GetNumNPCs(); i++)
	{
		// initialise npc
		m_NPCManager->GetNPC(i)->Init(m_characterMesh,
									  m_redMetalicMaterial);

		// add pathfinder
		m_NPCManager->GetNPC(i)->AddPathfinding(m_map);
		m_NPCManager->GetNPC(i)->SetOccluderMaterial(m_occluderMaterial);

		// create npc bullet
		m_bullet = new Bullet();
		m_bullet->Init(m_bulletMesh, m_yellowMaterial);
		m_bullet->SetActive(false);

		// add bullet to npc and set target
		m_NPCManager->GetNPC(i)->AddBullet(m_bullet);
		m_NPCManager->GetNPC(i)->SetTarget(m_player);
		m_NPCManager->GetNPC(i)->AddParticleSystem(m_particleSystem);
		
		m_NPCManager->GetNPC(i)->SetID("NPC");
	}
}

void
Game::DeleteMaze()
{
	// clear current maze object list
	if(!m_mazeObjects.empty())
	{
		// delete all renderables from the list
		for(unsigned int i = 0; i < m_mazeObjects.size(); i++)
		{
			if(m_mazeObjects.max_size() > 0)
			{
				delete m_mazeObjects[i];
			}
		}

		// null pointers
		m_wallBlock = nullptr;
		m_path = nullptr;
		m_ramp = nullptr;

		// delete renderables list
		m_mazeObjects.clear();
	}

	if(!m_floorList.empty())
	{
		// delete all renderables from the list
		for(unsigned int i = 0; i < m_floorList.size(); i++)
		{
			if(m_floorList.max_size() > 0)
			{
				delete m_floorList[i];
			}
		}

		// null pointers
		m_ground = nullptr;

		// delete renderables list
		m_floorList.clear();
	}

	if(!m_flagsList.empty())
	{
		// delete all renderables from the list
		for(unsigned int i = 0; i < m_flagsList.size(); i++)
		{
			if(m_flagsList.max_size() > 0)
			{
				delete m_flagsList[i];
			}
		}

		// null pointers
		m_flag = nullptr;

		// delete renderables list
		m_flagsList.clear();
	}
}

void
Game::ResetMouse()
{
	// reset mouse cursor position to 
	// middle of game window
	POINT p;
	p.x = (long)m_graphics->GetWidth() / 2;
	p.y = (long)m_graphics->GetHeight() / 2;
	ClientToScreen(m_graphics->GetHwnd(), &p);
	SetCursorPos(p.x, p.y);
}

void Game::KillPlayer()
{
	// fire explosion
	m_particleSystem->GetEmitter("EXPLOSION_SMOKE")->Begin(m_player->GetPosition());
	m_particleSystem->GetEmitter("EXPLOSION_SMOKE2")->Begin(m_player->GetPosition());
	m_particleSystem->GetEmitter("EXPLOSION_PARTS")->Begin(m_player->GetPosition());
	m_particleSystem->GetEmitter("EXPLOSION_CENTRE")->Begin(m_player->GetPosition());
	m_particleSystem->GetEmitter("FIRE_SMOKE")->End();
	m_particleSystem->GetEmitter("FIRE")->End();

	// turn player off
	m_player->SetActive(false);
	
	// turn off blur to show effects
	m_blurEffect = false;
}

Vector3
Game::GridToWorldSpace(int x, int z)
{
	// return world position using 
	// map grid location
	Vector3 position;
	position.x = x * (float)GlobalConstants::CELL_WIDTH;
	position.y = 0;
	position.z = z * (float)GlobalConstants::CELL_WIDTH;
	position.z = -position.z; // invert z as the maze is rendered in -z direction

	return position;
}

Vector3 Game::GridToWorldSpace(int x, int y, int z)
{
	// return world position using 
	// map grid location
	Vector3 position;
	position.x = x * (float)GlobalConstants::CELL_WIDTH;
	position.y = y * (float)GlobalConstants::CELL_WIDTH;
	position.z = z * (float)GlobalConstants::CELL_WIDTH;
	position.z = -position.z; // invert z as the maze is rendered in -z direction

	return position;
}

Vector2
Game::GetMidScreenPos()
{
	// get middle point of game window
	POINT p;
	p.x = (long)m_graphics->GetWidth() / 2;
	p.y = (long)m_graphics->GetHeight() / 2;
	ClientToScreen(m_graphics->GetHwnd(), &p);

	// set point to vector2 and return value
	Vector2 middle;
	middle.x = (float)p.x;
	middle.y = (float)p.y;
	return middle;
}

bool 
Game::RaycastToAABB(const Vector3 & start, const Vector3 & end, GameObject object)
{
	float maxLow = 0.0f; // max low point in ray
	float maxHigh = 1.0f; // max high point in ray

	float dimensionLow; // low point calculated from vectors
	float dimensionHigh; // high point calculated from vectors

	AABB aabbBox = object.GetHitBox(); // object aabb hitbox
									   // check if rays are parallel
	Vector3 direction = end - start;
	if(direction.x == 0)
	{
		if(start.x < aabbBox.GetMin().x ||
		   start.x > aabbBox.GetMax().x)
		{
			return false;
		}
	}

	if(direction.z == 0)
	{
		if(start.z < aabbBox.GetMin().z ||
		   start.z > aabbBox.GetMax().z)
		{
			return false;
		}
	}

	// intersection point along x dimension
	dimensionLow = (aabbBox.GetMin().x - start.x) / direction.x;
	dimensionHigh = (aabbBox.GetMax().x - start.x) / direction.x;

	// check if low is less than high
	if(dimensionHigh < dimensionLow)
		std::swap(dimensionHigh, dimensionLow);

	// if intersections are out of bounds they must be false
	if(dimensionHigh < maxLow) return false;
	if(dimensionLow > maxHigh) return false;

	// Add the clip from this dimension to the previous results 
	maxLow = max(dimensionLow, maxLow);
	maxHigh = min(dimensionHigh, maxHigh);

	if(maxLow > maxHigh) return false; // max low point must be outside max high

									   // intersection point along z dimension
	dimensionLow = (aabbBox.GetMin().z - start.z) / direction.z;
	dimensionHigh = (aabbBox.GetMax().z - start.z) / direction.z;

	// check if low is less than high
	if(dimensionHigh < dimensionLow)
		std::swap(dimensionHigh, dimensionLow);

	// if intersections are out of bounds they must be false
	if(dimensionHigh < maxLow) return false;
	if(dimensionLow > maxHigh)	return false;

	// Add the clip from this dimension to the previous results 
	maxLow = max(dimensionLow, maxLow);
	maxHigh = min(dimensionHigh, maxHigh);

	if(maxLow > maxHigh) return false; // max low point must be outside max high

	return true;
}

LRESULT 
Game::MessageHandler(HWND hWindow, UINT msg, WPARAM wParam, LPARAM lParam)
{
	// handle msg values in switch statement
	switch(msg)
	{
		case WM_DESTROY:
		PostQuitMessage(0); // post quit window
		return 0;
		case WM_KEYDOWN: case WM_SYSKEYDOWN:
		m_input->SetKeyDown(wParam); // set keyboard key down
		return 0;
		case WM_KEYUP: case WM_SYSKEYUP:
		m_input->SetKeyUp(wParam); // set keyboard key up
		return 0;
		case WM_MOUSEMOVE:
		m_input->SetMouseIn(lParam); // set mouse position
		return 0;
	}
	// else return default
	return DefWindowProc(hWindow, msg, wParam, lParam);
}