// Game.h
// Christopher Ball 2017
// The game class manages the main game loop
// and loading, updating and rendering of
// all game assets and scenes

#ifndef _GAME_H_
#define _GAME_H_

#include "pch.h"
#include "Timer.h"
#include "Level.h"

// forward declarations
class Graphics;
class Input;
class Player;
class LevelManager;
class NPCManager;
class WaypointManager;
class GameObject;
class Camera;
class Shader;
class Texture;
class Material;
class CubeMesh;
class FloorMesh;
class RampMesh;
class SphereMesh;
class QuadMesh;
class ParticleMesh;
class MazeBlock;
class Floor;
class Ramp;
class Bullet;
class Flag;
class Hud;
class PostProcessing;
class Particle;
class ParticleEmitter;
class ParticleSystem;

class Game
{
public:
	Game();
	~Game();
	void			Init(Graphics* graphics);		// initialises game class and passes pointer to the graphics class

	void			Run();							// main game function

	void			ProcessInput(float deltaTime);	// read inputs
	void			Update(float deltaTime);		// update all objects in the scene
	void			Render();						// Render all
	void			RenderShadowMap();				// render shadows
	void			RenderScene();					// render scene
	void			RenderHud();					// render hud

	void			ReleaseAll();					// release all pointers
	void			DeleteAll();					// delete all pointers

	void			NewGame();						// set up new game at level 1
	void			LoadAssets();					// load all required game assets
	void			ResetGame();					// reset game so player tries again
	void			EndGame();						// calls end game when player is caught

	void			SpawnMaze();					// set maze wall and path objects
	void			InitNPCs();						// initialise npc objects
	void			DeleteMaze();					// delete maze wall and path objects

	void			ResetMouse();					// reset mouse pos to middle of game window
	void			KillPlayer();					// call player death functions

	Vector3			GridToWorldSpace(int x, int z); // convert map grid to world space	
	Vector3			GridToWorldSpace(int x, int y, int z); // convert map grid to world space

	Vector2			GetMidScreenPos();				// return the pixel position of the middle of the game window

	bool			RaycastToAABB(const Vector3& start,
								  const Vector3& end,
								  GameObject object);	// trace a line and return true if it intersects and object


	LRESULT			MessageHandler(HWND hWindow,
								   UINT msg,
								   WPARAM wParam,
								   LPARAM lParam);		// deals with windows messages

private:
	Graphics*			m_graphics;				// pointer to graphics class for access to the gpu device
	Input*				m_input;				// manages key and mouse inputs
	Player*				m_player;				// player class
	LevelManager*		m_levelManager;			// keeps track of level num and load levels
	WaypointManager*	m_waypointManager;		// keeps track of waypoint list
	NPCManager*			m_NPCManager;			// manages all npcs
	Camera*				m_thirdPersonCamera;	// 3rd person camera
	
	SpriteBatch*		m_spriteBatch;			// 2d sprites engine

	Shader*				m_simpleShader;			// basic normal shader with ambient and diffuse lighting
	Shader*				m_specularShader;		// shader using specular lighting
	Shader*				m_normalMapShader;		// shader for normal map lighting
	Shader*				m_particleShader;		// shader for particle effects
	Shader*				m_occluderShader;		// shader for generating shadow map

	Texture*			m_wallTexture;			// texture for wall material
	Texture*			m_pathTexture;			// texture for Path tiles
	Texture*			m_pathNormalMap;		// normal map for paths
	Texture*			m_groundTexture;		// texture for the floor plane
	Texture*			m_groundNormalMap;		// normal map for floor plane
	Texture*			m_rampTexture;			// ramp texture
	Texture*			m_rampNormalMap;		// normal map for ramp texture
	Texture*			m_blueTexture;			// blue texture
	Texture*			m_redTexture;			// red texture
	Texture*			m_yellowTexture;		// yellow texture
	Texture*			m_flagTexture;			// flag post texture
	Texture*			m_explosionCentreTex;	// explosion centre
	Texture*			m_explosionParticleTex; // explosion particle texture
	Texture*			m_smokeTexture;			// smoke texture
	Texture*			m_fireTexture;			// fire texture
	Texture*			m_sparkTexture;			// spark Texture

	Material*			m_wallMaterial;			// material for wall block
	Material*			m_pathMaterial;			// material for Path
	Material*			m_groundMaterial;		// ground material for plane
	Material*			m_rampMaterial;			// material for ramp
	Material*			m_blueMetalicMaterial;	// blue material
	Material*			m_redMetalicMaterial;	// red material
	Material*			m_yellowMaterial;		// yellow material
	Material*			m_flagMaterial;			// flag post material
	Material*			m_explosionCentreMat;	// smoke material
	Material*			m_explosionParticleMat; // explosion particle material
	Material*			m_smokeMaterial;		// smoke material
	Material*			m_fireMaterial;			// fire material
	Material*			m_sparkMaterial;		// spark material
	Material*			m_occluderMaterial;		// material for occluders
	
	CubeMesh*			m_cubeMesh;				// mesh for maze wall blocks
	FloorMesh*			m_floorMesh;			// mesh for floor
	RampMesh*			m_rampMesh;				// mesh for ramps
	SphereMesh*			m_characterMesh;		// sphere mesh for characters
	SphereMesh*			m_bulletMesh;			// bullet mesh sphere
	QuadMesh*			m_flagMesh;				// mesh for flag post
	ParticleMesh*		m_particleMesh;			// mesh for particles

	MazeBlock*			m_wallBlock;			// the block for maze wall
	Floor*				m_path;					// Path object
	Floor*				m_ground;				// ground plane 
	Ramp*				m_ramp;					// ramp object
	Bullet*				m_bullet;				// bullet object
	Flag*				m_flag;					// flag post

	Hud*				m_hud;					// hud containing all hud sprites

	PostProcessing*		m_postProcessing;		// post processing blur effect

	ParticleSystem*		m_particleSystem;		// particle system
	
	Particle*			m_explosionCentre;		// explosion centre particle
	Particle*			m_explosionParticle;	// explosion particle
	Particle*			m_smokeParticle;		// smoke particle
	Particle*			m_fireParticle;			// fire particle
	Particle*			m_sparkParticle;		// spark particle
	
	std::vector<GameObject*> m_mazeObjects;		// vector list of wall objects
	std::vector<GameObject*> m_floorList;		// vector list of floor objects
	std::vector<GameObject*> m_flagsList;		// vector list of flags

	Timer				m_timer;				// helper object for time step
	float				m_timerFreq;			// timer frequency 
	float				m_currentTime;			// tick time
	float				m_previousTime;			// previous tick time

	float				m_keyDelay;				// delay for keypress
	bool				m_keyPressed;			// key was pressed last frame	
							
	bool				m_map[MAZE_DEPTH * MAZE_WIDTH]; // create array for pathfinding
	int					m_currentLevel;					// current maze level

	bool				m_blurEffect;			// toggles blur effect
	float				m_blurTimer;			// times the blur effect

	float				m_sparksTimer;			// timer for sparks
	bool				m_sparksFlag;			// toggles if sparks emit
};

#endif _GAME_H_
