#include "GameObject.h"
#include "Graphics.h"
#include "MeshObject.h"
#include "Material.h"
#include "Constants.h"

GameObject::GameObject() :
	m_ID("GameObject"),
	m_mesh(nullptr),
	m_material(nullptr),
	m_OccluderMaterial(nullptr),
	m_modelMatrix(XMMatrixIdentity()),
	m_scaleMatrix(XMMatrixIdentity()),
	m_translationMatrix(XMMatrixIdentity()),
	m_rotationalMatrix(XMMatrixIdentity()),
	m_position(0.0f, 0.0f, 0.0f),
	m_velocity(0.0f, 0.0f, 0.0f),
	m_defaultXAxis(0.0f, 0.0f, 0.0f),
	m_defaultYAxis(0.0f, 0.0f, 0.0f),
	m_defaultZAxis(0.0f, 0.0f, 0.0f),
	m_hitbox(AABB(Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f))),
	m_scale(1.0f, 1.0f, 1.0f),
	m_alpha(1.0f),
	m_rotation(0.0f),
	m_active(true)
{
	m_sphereCollider.position = m_position;
	m_sphereCollider.radius = 0.0f;
}

GameObject::~GameObject()
{
}

void
GameObject::Init(MeshObject* mesh,
				 Material* material)
{
	// set member variables with passed params
	m_mesh = mesh;
	m_material = material;

	// set default axis
	m_defaultXAxis = Vector3(1.0f, 0.0f, 0.0f);
	m_defaultYAxis = Vector3(0.0f, 1.0f, 0.0f);
	m_defaultZAxis = Vector3(0.0f, 0.0f, 1.0f);

	// set aabb
	m_hitbox.SetAABB(Vector3(-1.0f, -1.0f, -1.0f), Vector3(1.0f, 1.0f, 1.0f));
}

void
GameObject::Init(MeshObject* mesh, 
				 Material* material, 
				 Vector3 position)
{
	// set member variables with passed params
	m_mesh = mesh;
	m_material = material;
	m_position = position;

	// set default axis
	m_defaultXAxis = Vector3(1.0f, 0.0f,  0.0f);
	m_defaultYAxis = Vector3(0.0f, 1.0f,  0.0f);
	m_defaultZAxis = Vector3(0.0f, 0.0f,  1.0f);

	// set aabb
	m_hitbox.SetAABB(Vector3(-1.0f, -1.0f, -1.0f), Vector3(1.0f, 1.0f, 1.0f));
}

void
GameObject::Render(Graphics* graphics)
{
	if(graphics)
	{
		// if graphics exists
		// set world matrix
		graphics->SetWorldMatrix(m_modelMatrix);

		// set alpha value
		graphics->SetCbufferAlpha(m_alpha);

		// if the object has a material
		if(m_material)
		{
			m_material->RenderSetup(graphics);
		}

		// if object has a mesh
		if(m_mesh)
		{
			m_mesh->Render(graphics);
		}
	}
}

void
GameObject::RenderToShadowMap(Graphics* graphics)
{

	// if the object has a material
	if(m_OccluderMaterial)
	{
		graphics->SetWorldMatrix(m_modelMatrix);

		m_OccluderMaterial->ShadowRenderSetup(graphics);
		
		// if object has a mesh
		if(m_mesh)
		{
			m_mesh->Render(graphics);
		}
	}

}

void 
GameObject::SetOccluderMaterial(Material* material)
{
	m_OccluderMaterial = material;
}

void
GameObject::SetID(std::string string)
{
	m_ID = string;
}

void
GameObject::SetPosition(Vector3 position)
{
	m_position = position;
}

void 
GameObject::SetVelocity(Vector3 velocity)
{
	m_velocity = velocity;
}

void 
GameObject::SetAngle(float angle)
{
	m_rotation = angle;
}

void 
GameObject::SetScale(Vector3 scale)
{
	m_scale = scale;
}

void 
GameObject::SetActive(bool active)
{
	m_active = active;
}
