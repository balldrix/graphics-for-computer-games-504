// GameObject.h
// Christopher Ball 2017
// A base class for renderable objects
// Modified code from SumoDX by Mickey Macdonald @scruffyfurn

#ifndef _GAMEOBJECT_H_
#define _GAMEOBJECT_H_

#include "pch.h"
#include "AABB.h"

// forward definitions
class MeshObject;
class Material;
class Graphics;

struct SphereCollider
{
	Vector3 position;
	float	radius;
};

class GameObject
{
public:
	GameObject();
	~GameObject();

	void			Init(MeshObject* mesh,
						 Material* material); // initialise object

	void			Init(MeshObject* mesh, 
						 Material* material, 
						 Vector3 position); // initialise object

	virtual void	Update() {};					// update object
	virtual void	Update(float deltaTime) {};		// update object with time steps
	void			Render(Graphics* graphics);		// render object
	void			RenderToShadowMap(Graphics* graphics); // render object to shadow map
	void			SetOccluderMaterial(Material* material);

	// helper methods
	// setters
	void			SetID(std::string string);
	void			SetPosition(Vector3 position);
	void			SetVelocity(Vector3 velocity);
	void			SetAngle(float angle);
	void			SetScale(Vector3 scale);
	void			SetActive(bool active);

	// getters
	MeshObject*		GetMesh() const { return m_mesh; }
	Material*		GetMaterial() const { return m_material; }
	std::string		GetID() const { return m_ID; }
	Vector3			GetPosition() const { return m_position; }
	Vector3			GetVelocity() const { return m_velocity; }
	AABB			GetHitBox()	const	{ return m_hitbox; } // return hitbox
	SphereCollider	GetSphereCollider() const { return m_sphereCollider; }		// get npc sphere radius

	virtual	Vector3	GetFacing() const { return m_defaultZAxis; }
	bool			IsActive() const { return m_active; }
	
protected:
	std::string		m_ID;				// object ID
	MeshObject*		m_mesh;				// model mesh
	Material*		m_material;			// model material
	Material*		m_OccluderMaterial;	// shadow material

	Matrix			m_modelMatrix;		// model world matrix
	Matrix			m_scaleMatrix;		// scale matrix
	Matrix			m_translationMatrix;// translation matrix
	Matrix			m_rotationalMatrix;	// rotation matrix

	Vector3			m_position;			// object position
	Vector3			m_velocity;			// object movement velocity
	Vector3			m_defaultXAxis;		// default x axis
	Vector3			m_defaultYAxis;		// default y axis
	Vector3			m_defaultZAxis;		// default z axis

	AABB			m_hitbox;			// axis aligned bounding box
	SphereCollider	m_sphereCollider;	// sphere collider

	Vector3			m_scale;			// object scale
	float			m_alpha;			// transparency value
	float			m_rotation;			// rotational angle in degrees

	bool			m_active;			// is object active
};

#endif _GAMEOBJECT_H_