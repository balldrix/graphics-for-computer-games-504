#include "Graphics.h"
#include "Constants.h"
#include "ConstantBuffers.h"
#include "Error.h"

Graphics::Graphics() :
m_hWnd(nullptr),
m_hInstance(nullptr),			
m_swapchain(nullptr),
m_D3DDevice(nullptr),
m_D3DDeviceContext(nullptr),
m_renderTargetView(nullptr),
m_backbuffer(nullptr),
m_depthBuffer(nullptr),
m_depthBufferTexture(nullptr),
m_defaultRasterState(nullptr),
m_alphaBlendState(nullptr),
m_shadowMap(nullptr),
m_shadowDepthBuffer(nullptr),
m_shadowRenderTarget(nullptr),
m_shadowDepthStencil(nullptr),
m_shadowShaderResource(nullptr),
m_clampSamplerState(nullptr),
m_shadowRasterState(nullptr),
m_constantBufferLight(nullptr),
m_constantBufferLight2(nullptr),
m_constantBufferProjection(nullptr),
m_constantBufferView(nullptr),
m_constantBufferModel(nullptr),
m_constantBufferCamera(nullptr),
m_constantBufferAlpha(nullptr),
m_linearSampler(nullptr),
m_fullScreen(false),
m_gameWidth(0.0f),
m_gameHeight(0.0f),
m_lightDirection(0.0f, 0.0f, 0.0f)
{
}

Graphics::~Graphics()
{
}

void
Graphics::Init(HWND hWnd, HINSTANCE hInstance)
{
	HRESULT result; // used for error checking
	m_hWnd = hWnd;
	m_hInstance = hInstance;

	// get screen dimensions from window handle
	RECT rc;
	GetClientRect(m_hWnd, &rc);
	UINT width = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;

	// Initialise DirectX
	DXGI_SWAP_CHAIN_DESC scd = { 0 };
	D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_9_3;
	scd.BufferCount = 1;
	scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	scd.BufferDesc.Width = width;
	scd.BufferDesc.Height = height;
	scd.BufferDesc.RefreshRate.Numerator = 60;
	scd.BufferDesc.RefreshRate.Denominator = 1;
	scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	scd.OutputWindow = m_hWnd;
	scd.SampleDesc.Count = 1;
	scd.SampleDesc.Quality = 0;
	scd.Windowed = !m_fullScreen;
	scd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	result = D3D11CreateDeviceAndSwapChain(NULL,
										   D3D_DRIVER_TYPE_HARDWARE,
										   NULL,
										   NULL, //D3D11_CREATE_DEVICE_FLAG::D3D11_CREATE_DEVICE_DEBUG,
										   NULL,
										   NULL,
										   D3D11_SDK_VERSION,
										   &scd,
										   &m_swapchain,
										   &m_D3DDevice,
										   NULL,
										   &m_D3DDeviceContext);

	// D3D_FEATURE_LEVEL level = m_D3DDevice->GetFeatureLevel();

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating D3D11 Device in Graphics.cpp; \n");
		
		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message
	
		PostQuitMessage(0); // quit game
	}

	// Get backbuffer pointer from swapchain
	result = m_swapchain->GetBuffer(0, __uuidof(m_backbuffer), (void**)&m_backbuffer);
	
	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error getting Back Buffer in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	// create render target view
	result = m_D3DDevice->CreateRenderTargetView(m_backbuffer, nullptr, &m_renderTargetView);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Render Target in Graphics; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	m_backbuffer->Release();

	// depth buffer descripton
	D3D11_TEXTURE2D_DESC t2d = { 0 };
	t2d.Width = width;
	t2d.Height = height;
	t2d.MipLevels = 1;
	t2d.ArraySize = 1;
	t2d.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	t2d.SampleDesc.Count = 1;
	t2d.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	t2d.Usage = D3D11_USAGE_DEFAULT;

	// create depth buffer
	result = m_D3DDevice->CreateTexture2D(&t2d, NULL, &m_depthBufferTexture);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Texture2D in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	// create depth stencil state
	D3D11_DEPTH_STENCIL_VIEW_DESC dsvd;
	ZeroMemory(&dsvd, sizeof(dsvd));
	dsvd.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;

	// create depth stencil state
	D3D11_DEPTH_STENCIL_DESC svd = { 0 };

	// Set up the description of the stencil state.
	svd.DepthEnable = true;
	svd.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	svd.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	svd.StencilEnable = true;
	svd.StencilReadMask = 0xFF;
	svd.StencilWriteMask = 0xFF;
	svd.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	svd.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	svd.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	svd.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	svd.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	svd.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	svd.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	svd.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Create the depth stencil state.
	result = m_D3DDevice->CreateDepthStencilState(&svd, &m_depthStencilState);
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating DepthStencil in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	// create depth stencil view
	result = m_D3DDevice->CreateDepthStencilView(m_depthBufferTexture, &dsvd, &m_depthBuffer);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating DepthStencil in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	// setup default raster state
	D3D11_RASTERIZER_DESC rd = {};
	rd.AntialiasedLineEnable = false;
	rd.CullMode = D3D11_CULL_BACK;
	rd.DepthBias = 0;
	rd.DepthBiasClamp = 0.0f;
	rd.DepthClipEnable = true;
	rd.FillMode = D3D11_FILL_SOLID;
	rd.FrontCounterClockwise = false;
	rd.MultisampleEnable = false;
	rd.ScissorEnable = false;
	rd.SlopeScaledDepthBias = 0.0f;

	// Create the rasterizer state
	result = m_D3DDevice->CreateRasterizerState(&rd, &m_defaultRasterState);
	
	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Rasterization State in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	// set blending state description for alpha transparency
	D3D11_BLEND_DESC bdesc = { 0 };
	bdesc.RenderTarget[0].BlendEnable = true;
	bdesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	bdesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	bdesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	bdesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	bdesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
	bdesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	bdesc.RenderTarget[0].RenderTargetWriteMask = 0x0f;
	bdesc.AlphaToCoverageEnable = true;
	bdesc.IndependentBlendEnable = true;
	result = m_D3DDevice->CreateBlendState(&bdesc, &m_alphaBlendState);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Blend State in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	SetStandardViewPort();

	// set linear sampler
	D3D11_SAMPLER_DESC samplerbd = {};
	samplerbd.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerbd.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerbd.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerbd.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerbd.BorderColor[0] = 0.0f;
	samplerbd.BorderColor[1] = 0.0f;
	samplerbd.BorderColor[2] = 0.0f;
	samplerbd.BorderColor[3] = 0.0f;
	samplerbd.MinLOD = 0.0f;
	samplerbd.MaxLOD = D3D11_FLOAT32_MAX;
	samplerbd.MipLODBias = 0.0f;
	samplerbd.MaxAnisotropy = 1;
	samplerbd.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	result = m_D3DDevice->CreateSamplerState(&samplerbd, &m_linearSampler);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Sampler State in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	m_D3DDeviceContext->PSSetSamplers(0, 1, &m_linearSampler);

	// shadow map description
	D3D11_TEXTURE2D_DESC smd;
	smd.Width = GlobalConstants::SHADOWMAP_WIDTH;
	smd.Height = GlobalConstants::SHADOWMAP_HEIGHT;
	smd.MipLevels = 1;
	smd.ArraySize = 1;
	smd.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	smd.SampleDesc.Count = 1;
	smd.SampleDesc.Quality = 0;
	smd.Usage = D3D11_USAGE_DEFAULT;
	smd.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	smd.CPUAccessFlags = 0;
	smd.MiscFlags = 0;
	
	// create shadow map
	result = m_D3DDevice->CreateTexture2D(&smd, NULL, &m_shadowMap);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Shadow Map in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	// Setup the description of the render target view.
	// setup render target view
	D3D11_RENDER_TARGET_VIEW_DESC rtvd;
	rtvd.Format = smd.Format;
	rtvd.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	rtvd.Texture2D.MipSlice = 0;

	// Create the render target view.
	result = m_D3DDevice->CreateRenderTargetView(m_shadowMap, &rtvd, &m_shadowRenderTarget);
	
	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Shadow render target in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	// Initialize the description of the depth buffer.
	D3D11_TEXTURE2D_DESC sdbd;
	ZeroMemory(&sdbd, sizeof(sdbd));

	// Set up the description of the depth buffer.
	sdbd.Width = GlobalConstants::SHADOWMAP_WIDTH;
	sdbd.Height = GlobalConstants::SHADOWMAP_HEIGHT;
	sdbd.MipLevels = 1;
	sdbd.ArraySize = 1;
	sdbd.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	sdbd.SampleDesc.Count = 1;
	sdbd.SampleDesc.Quality = 0;
	sdbd.Usage = D3D11_USAGE_DEFAULT;
	sdbd.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	sdbd.CPUAccessFlags = 0;
	sdbd.MiscFlags = 0;

	// Create the texture for the depth buffer using the filled out description.
	result = m_D3DDevice->CreateTexture2D(&sdbd, NULL, &m_shadowDepthBuffer);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Shadow depth buffer in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}
	
	// shadow stencil view description
	dsvd = { };
	dsvd.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	dsvd.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	dsvd.Texture2D.MipSlice = 0;

	// create shadow depth stencil
	result = m_D3DDevice->CreateDepthStencilView(m_shadowDepthBuffer, &dsvd, &m_shadowDepthStencil);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Shadow Depth Stencil in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	// shadow shader resource view description
	D3D11_SHADER_RESOURCE_VIEW_DESC srvd;
	srvd = { };
	srvd.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvd.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	srvd.Texture2D.MipLevels = 1;
	srvd.Texture2D.MostDetailedMip = 0;

	// create shadow shader resource
	result = m_D3DDevice->CreateShaderResourceView(m_shadowMap, &srvd, &m_shadowShaderResource);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Shadow shader resource in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	// comparison sampler state description
	samplerbd = { };
	samplerbd.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerbd.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerbd.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	
	// create sampler state
	result = m_D3DDevice->CreateSamplerState(&samplerbd, &m_clampSamplerState);

	m_D3DDeviceContext->PSSetSamplers(1, 1, &m_clampSamplerState);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating sampler state in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	// setup shadow raster state
	rd.CullMode = D3D11_CULL_FRONT;
	result = m_D3DDevice->CreateRasterizerState(&rd, &m_shadowRasterState);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Raster State in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	// create shadow viewport
	m_shadowViewport = { 0 };
	m_shadowViewport.Height = GlobalConstants::SHADOWMAP_HEIGHT;
	m_shadowViewport.Width = GlobalConstants::SHADOWMAP_WIDTH;
	m_shadowViewport.MinDepth = 0.f;
	m_shadowViewport.MaxDepth = 1.f;

	// setup constant buffers
	D3D11_BUFFER_DESC cbd = { 0 };
	cbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cbd.Usage = D3D11_USAGE_DEFAULT;
	cbd.CPUAccessFlags = 0;
	cbd.ByteWidth = sizeof(ConstantBufferLight2); // padding needed for buffers
	result = m_D3DDevice->CreateBuffer(&cbd, nullptr, &m_constantBufferLight2);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Constant Buffer in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	cbd = { 0 };
	cbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cbd.Usage = D3D11_USAGE_DEFAULT;
	cbd.CPUAccessFlags = 0;
	cbd.ByteWidth = sizeof(ConstantBufferProjection);
	result = m_D3DDevice->CreateBuffer(&cbd, nullptr, &m_constantBufferProjection);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Constant Buffer in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	cbd = { 0 };
	cbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cbd.Usage = D3D11_USAGE_DEFAULT;
	cbd.CPUAccessFlags = 0;
	cbd.ByteWidth = sizeof(ConstantBufferView);
	result = m_D3DDevice->CreateBuffer(&cbd, nullptr, &m_constantBufferView);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Constant Buffer in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	cbd = { 0 };
	cbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cbd.Usage = D3D11_USAGE_DEFAULT;
	cbd.CPUAccessFlags = 0;
	cbd.ByteWidth = sizeof(ConstantBufferModel);
	result = m_D3DDevice->CreateBuffer(&cbd, nullptr, &m_constantBufferModel);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Constant Buffer in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message

		PostQuitMessage(0); // quit game
	}

	cbd = { 0 };
	cbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cbd.Usage = D3D11_USAGE_DEFAULT;
	cbd.CPUAccessFlags = 0;
	cbd.ByteWidth = sizeof(ConstantBufferCamera);
	result = m_D3DDevice->CreateBuffer(&cbd, nullptr, &m_constantBufferCamera);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Constant Buffer in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message

		PostQuitMessage(0); // quit game
	}

	cbd = { 0 };
	cbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cbd.Usage = D3D11_USAGE_DEFAULT;
	cbd.CPUAccessFlags = 0;
	cbd.ByteWidth = sizeof(ConstantBufferParticle);
	result = m_D3DDevice->CreateBuffer(&cbd, nullptr, &m_constantBufferAlpha);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Constant Buffer in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message

		PostQuitMessage(0); // quit game
	}

	cbd = { 0 };
	cbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cbd.Usage = D3D11_USAGE_DEFAULT;
	cbd.CPUAccessFlags = 0;
	cbd.ByteWidth = sizeof(ConstantBufferLight);
	result = m_D3DDevice->CreateBuffer(&cbd, nullptr, &m_constantBufferLight);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Constant Buffer in Graphics.cpp; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message

		PostQuitMessage(0); // quit game
	}

	// set up generic scene lighting
	ConstantBufferLight2 lightingBuffer;

	m_lightDirection = Vector3(-0.707f, -1.0f, -0.707f);
	lightingBuffer.ambLightColour = Vector4(0.4f, 0.4f, 0.4f, 1.0f);
	lightingBuffer.diffuseColour = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	lightingBuffer.specularColour = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	lightingBuffer.specularPower = 32.0f;
	lightingBuffer.lightDirection = m_lightDirection;
	lightingBuffer.lightDirection.Normalize();

	m_D3DDeviceContext->UpdateSubresource(m_constantBufferLight2, 0, nullptr, &lightingBuffer, 0, 0);
	// set pixel shader constant
	m_D3DDeviceContext->VSSetConstantBuffers(0, 1, &m_constantBufferLight2);
	m_D3DDeviceContext->PSSetConstantBuffers(0, 1, &m_constantBufferLight2);

	// set game dimensions
	m_gameWidth = (float)width;
	m_gameHeight = (float)height;
}

void
Graphics::ReleaseAll()
{
	// release all com pointers
	if(m_linearSampler) { m_linearSampler->Release(); }
	if(m_constantBufferAlpha) { m_constantBufferAlpha->Release(); }
	if(m_constantBufferCamera) { m_constantBufferCamera->Release(); }
	if(m_constantBufferModel) { m_constantBufferModel->Release(); }
	if(m_constantBufferView) { m_constantBufferView->Release(); }
	if(m_constantBufferProjection) { m_constantBufferProjection->Release(); }
	if(m_constantBufferLight2) { m_constantBufferLight2->Release(); }
	if(m_constantBufferLight) { m_constantBufferLight->Release(); }
	if(m_shadowRasterState) { m_shadowRasterState->Release(); }
	if(m_clampSamplerState) { m_clampSamplerState->Release(); }
	if(m_shadowShaderResource) { m_shadowShaderResource->Release(); }
	if(m_shadowDepthStencil) { m_shadowDepthStencil->Release(); }
	if(m_shadowRenderTarget) { m_shadowRenderTarget->Release(); }
	if(m_shadowDepthBuffer) { m_shadowDepthBuffer->Release(); }
	if(m_shadowMap) { m_shadowMap->Release(); }
	if(m_alphaBlendState) { m_alphaBlendState->Release(); }
	if(m_defaultRasterState) { m_defaultRasterState->Release(); }
	if(m_depthBufferTexture) { m_depthBufferTexture->Release(); }
	if(m_depthBuffer) { m_depthBuffer->Release(); }
	if(m_renderTargetView) { m_renderTargetView->Release(); }
	if(m_D3DDeviceContext) { m_D3DDeviceContext->Release(); }
	if(m_D3DDevice) { m_D3DDevice->Release(); }
	if(m_swapchain) { m_swapchain->Release(); }
}

void 
Graphics::SetWorldMatrix(Matrix world)
{
	// create constant buffer for vertex shader
	ConstantBufferModel constantBufferModel;
	constantBufferModel.worldMatrix = XMMatrixTranspose(world);
	m_D3DDeviceContext->UpdateSubresource(m_constantBufferModel, 0, nullptr, &constantBufferModel, 0, 0);
	m_D3DDeviceContext->VSSetConstantBuffers(3, 1, &m_constantBufferModel);
}

void
Graphics::SetViewMatrix(Matrix view)
{
	// update view matrix in shader
	ConstantBufferView constantBufferview;
	constantBufferview.viewMatrix = XMMatrixTranspose(view);
	m_D3DDeviceContext->UpdateSubresource(m_constantBufferView, 0, nullptr, &constantBufferview, 0, 0);
	m_D3DDeviceContext->VSSetConstantBuffers(2, 1, &m_constantBufferView);
}

void 
Graphics::SetProjectionMatrix(Matrix projection)
{
	// update projection matrix in shader
	ConstantBufferProjection constantBufferProjection;
	constantBufferProjection.projectionMatrix = XMMatrixTranspose(projection);
	m_D3DDeviceContext->UpdateSubresource(m_constantBufferProjection, 0, nullptr, &constantBufferProjection, 0, 0);
	m_D3DDeviceContext->VSSetConstantBuffers(1, 1, &m_constantBufferProjection);
}

void 
Graphics::SetCBufferCamera(Vector3 position)
{
	// update camera position in constant buffer
	ConstantBufferCamera constantBufferCamera;
	constantBufferCamera.cameraPosition = position;
	constantBufferCamera.padding = 0.0f;
	m_D3DDeviceContext->UpdateSubresource(m_constantBufferCamera, 0, nullptr, &constantBufferCamera, 0, 0);
	m_D3DDeviceContext->VSSetConstantBuffers(4, 1, &m_constantBufferCamera);
}

void 
Graphics::SetCbufferAlpha(float alpha)
{
	// update alpha value in constant buffer
	ConstantBufferParticle constantBufferParticle;
	constantBufferParticle.alpha = alpha;
	constantBufferParticle.padding = Vector3(0.0f, 0.0f, 0.0f);
	m_D3DDeviceContext->UpdateSubresource(m_constantBufferAlpha, 0, nullptr, &constantBufferParticle, 0, 0);
	m_D3DDeviceContext->PSSetConstantBuffers(5, 1, &m_constantBufferAlpha);
}

void Graphics::SetCBufferLight(Vector3 position)
{
	// setup lighting for shadow mapping
	Matrix lightPerspective = XMMatrixOrthographicLH(40, 40, 1.0f, 1000);
	//Vector3 eye = Vector3(90.0, 20.0f, 10.0f);
	Vector3 eye = position + Vector3(10.0, 20.0f, 10.0f);
	Vector3 lookat = eye + m_lightDirection * 15;
	Matrix lightView = XMMatrixLookAtLH(eye, lookat, Vector3(0.0f, 1.0f, 0.0f));

	ConstantBufferLight constantBufferShadows;
	constantBufferShadows.lightProjectionMatrix = XMMatrixTranspose(lightPerspective);
	constantBufferShadows.lightViewMatrix = XMMatrixTranspose(lightView);
	m_D3DDeviceContext->UpdateSubresource(m_constantBufferLight, 0, nullptr, &constantBufferShadows, 0, 0); 
	m_D3DDeviceContext->VSSetConstantBuffers(6, 1, &m_constantBufferLight);
	m_D3DDeviceContext->PSSetConstantBuffers(6, 1, &m_constantBufferLight);
}

void Graphics::SetAlphaBlending()
{
	m_D3DDeviceContext->OMSetBlendState(m_alphaBlendState, nullptr, 0xffffffff);
}

void 
Graphics::SetStandardViewPort()
{
	// get screen dimensions from window handle
	RECT rc;
	GetClientRect(m_hWnd, &rc);
	UINT width = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;

	// initialise viewport
	D3D11_VIEWPORT vp;
	vp.Width = (float)width;
	vp.Height = (float)height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	m_D3DDeviceContext->RSSetViewports(1, &vp);
}

void
Graphics::SetShadowRenderState()
{
	m_D3DDeviceContext->RSSetState(0);
	m_D3DDeviceContext->RSSetViewports(1, &m_shadowViewport);
}

void Graphics::SetShadowRenderTarget()
{
	// clear shadow map from render target
	ID3D11RenderTargetView* nullRenderTarget = NULL;
	m_D3DDeviceContext->OMSetRenderTargets(1, &nullRenderTarget, NULL);

	ID3D11ShaderResourceView *const pSRV[1] = { NULL };
	m_D3DDeviceContext->PSSetShaderResources(2, 1, pSRV);

	m_D3DDeviceContext->OMSetRenderTargets(1, &m_shadowRenderTarget, m_shadowDepthStencil);
	m_D3DDeviceContext->ClearRenderTargetView(m_shadowRenderTarget, Colors::DarkSlateGray);
	m_D3DDeviceContext->ClearDepthStencilView(m_shadowDepthStencil, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
}

void
Graphics::BeginScene()
{
	// reset Rasterisation state to default state
	m_D3DDeviceContext->RSSetState(0);

	// set depth stencil
	m_D3DDeviceContext->OMSetDepthStencilState(m_depthStencilState, 1);
}

void 
Graphics::ResetRenderTarget()
{
	// clear shadow map from render target
	ID3D11RenderTargetView* nullRenderTarget = NULL;
	m_D3DDeviceContext->OMSetRenderTargets(1, &nullRenderTarget, NULL);

	m_D3DDeviceContext->OMSetRenderTargets(1, &m_renderTargetView, m_depthBuffer);

	ID3D11ShaderResourceView *const pSRV[1] = { NULL };
	m_D3DDeviceContext->PSSetShaderResources(2, 1, pSRV);

	// set shadow map as shader resource
	m_D3DDeviceContext->PSSetShaderResources(2, 1, &m_shadowShaderResource);
}

void 
Graphics::ClearBackbuffer()
{
	// clear backbuffer
	m_D3DDeviceContext->ClearRenderTargetView(m_renderTargetView, Colors::DarkSlateGray);
}

void 
Graphics::ClearDepthBuffer()
{
	// clear depth buffer
	m_D3DDeviceContext->ClearDepthStencilView(m_depthBuffer, D3D11_CLEAR_DEPTH | D3D10_CLEAR_STENCIL, 1.0f, 0);
}

void
Graphics::PresentBackBuffer()
{
	// present backbuffer
	m_swapchain->Present(0, 0);
}
