// Graphics.h
// Christopher Ball 2017
// This class manages DirectX and all the functions for 
// drawing to the screen

#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

#include "pch.h"

class Graphics
{
public:
	Graphics();					
	~Graphics();			
	void						Init(HWND hWindow, HINSTANCE hInstance);	// initialises graphics device
	void						ReleaseAll();								// release all ID3D11 pointers
	void						SetWorldMatrix(Matrix world);				// use world, view and projection matrices to send final to constant buffer
	void						SetViewMatrix(Matrix view);					// pass view matrix from camera
	void						SetProjectionMatrix(Matrix projection);		// pass projection matrix from camera
	void						SetCBufferCamera(Vector3 position);			// pass camera position to vertex shader
	void						SetCbufferAlpha(float alpha);				// pass alpha value to pixel shader
	void						SetCBufferLight(Vector3 position);							// set light for shadows
	void						SetAlphaBlending();
	void						SetStandardViewPort();						// setup standard viewport
	void						SetShadowRenderState();						// set shadow rendering state
	void						SetShadowRenderTarget();					// shadow render target
	void						BeginScene();								// prepare render target for rendering scene
	
	void						ResetRenderTarget();						// reset render target to back buffer
	void						ClearBackbuffer();							// clear backbuffer
	void						ClearDepthBuffer();							// clear depth buffer
	void						PresentBackBuffer();						// present backbuffer to screen

	ID3D11Device*				GetDevice() { return m_D3DDevice; }			// return graphics device pointer
	ID3D11DeviceContext*		GetDeviceContext() { return m_D3DDeviceContext; }	// return graphics context pointer
	ID3D11DepthStencilView*		GetDepthStencilView() { return m_depthBuffer; } // returns depth buffer
	ID3D11DepthStencilView*		GetShadowDepthStencil() { return m_shadowDepthStencil
	; }	// returns shadow depth stencil
	ID3D11RenderTargetView*		GetShadowRenderTarget() { return m_shadowRenderTarget; }
	ID3D11DepthStencilState*	GetDepthStencilState() { return m_depthStencilState; }
	float GetWidth()	const { return m_gameWidth; }					// return game window width
	float GetHeight()	const { return m_gameHeight; }					// return game window height
	
	HWND GetHwnd()		const { return m_hWnd; }						// return window handle

private:
	HWND						m_hWnd;						// handle to the window
	HINSTANCE					m_hInstance;				// instance of our window
	
	IDXGISwapChain*				m_swapchain;				// pointer to swapchain of frame buffers
	ID3D11Device*				m_D3DDevice;				// pointer to D3D Device
	ID3D11DeviceContext*		m_D3DDeviceContext;			// pointer to D3D Context

	ID3D11RenderTargetView*		m_renderTargetView;			// pointer to render target view
	ID3D11Texture2D*			m_backbuffer;				// pointer to 2d backbuffer
	
	ID3D11DepthStencilView*		m_depthBuffer;				// pointer to depth view resource	
	ID3D11Texture2D*			m_depthBufferTexture;		// pointer to depth buffer
	ID3D11DepthStencilState*	m_depthStencilState;		// depth stencil state 
	ID3D11RasterizerState*		m_defaultRasterState;		// rasterizer state
	ID3D11BlendState*			m_alphaBlendState;			// blend state for alpha blending
	
	ID3D11SamplerState*			m_linearSampler;			// pointer to samplerstate for texture sampling

	ID3D11Texture2D*			m_shadowMap;				// shadow map
	ID3D11Texture2D*			m_shadowDepthBuffer;		// shadow map
	ID3D11DepthStencilView*		m_shadowDepthStencil;		// depth stencil for shadows
	ID3D11ShaderResourceView*	m_shadowShaderResource;		// resource view for shadows
	ID3D11RenderTargetView*		m_shadowRenderTarget;		// render target to render shadow map
	ID3D11SamplerState*			m_clampSamplerState;		// sampler state used to calculate shadow map
	ID3D11RasterizerState*		m_shadowRasterState;		// rasterizer state for shadow mapping
	D3D11_VIEWPORT				m_shadowViewport;			// viewport for shadow mapping

	ID3D11Buffer*				m_constantBufferLight;		// constant buffer for shadows
	ID3D11Buffer*				m_constantBufferLight2;	// constant buffer used for lighting
	ID3D11Buffer*				m_constantBufferProjection;	// constant buffer used for the view projection
	ID3D11Buffer*				m_constantBufferView;		// constant buffer for camera view
	ID3D11Buffer*				m_constantBufferModel;		// constant buffer for world matrix
	ID3D11Buffer*				m_constantBufferCamera;		// camera position for Blinn-Phong shader
	ID3D11Buffer*				m_constantBufferAlpha;		// particle buffer to set alpha channel value

	bool						m_fullScreen;				// fullscreen setting
	
	float						m_gameWidth;				// game resolution width
	float						m_gameHeight;				// game resolution height

	Vector3						m_lightDirection;				// direction of light
};

#endif _GRAPHICS_H_
