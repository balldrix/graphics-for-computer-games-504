#include "Hud.h"
#include "Graphics.h"
#include "Sprite.h"

#include "Constants.h"

Hud::Hud() :
	m_health(nullptr),
	m_life(nullptr),
	m_screenWidth(0.0f),
	m_screenHeight(0.0f),
	m_numLives(0)
{
	m_lives.clear();
}

Hud::~Hud()
{
	ReleaseAll();
	DeleteAll();
}

void
Hud::Init(Graphics* graphics, SpriteBatch* spriteBatch)
{	
	// dimensions of screen in pixels
	m_screenWidth = graphics->GetWidth();
	m_screenHeight = graphics->GetHeight();

	// create memory for sprites
	m_health = new Sprite();
	m_life = new Sprite();

	// load sprite textures
	m_health->Init(graphics, "Assets\\Sprites\\health.png");
	m_life->Init(graphics, "Assets\\Sprites\\heart.png");
		
	// set sprite positions
	m_health->SetOrigin(Vector2(0.0f, 0.0f));
	m_health->SetPosition(Vector2(m_screenWidth * 0.01f, m_screenHeight * 0.01f));
	
	// heart positions are set in the render method
	// due to the order changes
	// set max lives
	m_numLives = PlayerConstants::MAX_LIVES;

	m_life->SetOrigin(Vector2(0.0f, 0.0f));

	for(int i = 0; i < m_numLives; i++)
	{
		m_lives.push_back(m_life);
	}

	// set default active sprites
	m_health->SetActive(true);	
}

void 
Hud::Update(float deltaTime)
{
}

void 
Hud::Render(SpriteBatch* spriteBatch)
{
	// render health
	if(m_health->IsActive())
	{
		// render right arrow
		m_health->Render(spriteBatch);
	}

	// render lives
	for(unsigned int i = 0; i < m_numLives; i++)
	{
		// set position and render
		m_lives[i]->SetPosition(Vector2((m_screenWidth * 0.01f) + (m_lives[i]->GetWidth() * i) + m_lives[i]->GetWidth() * 0.2f, m_screenHeight * 0.1));
		m_lives[i]->Render(spriteBatch);
	}
}

void 
Hud::ReleaseAll()
{
	// release all objects
	if(m_life) { m_life->Release(); }
	if(m_health) { m_health->Release(); }
}

void
Hud::DeleteAll()
{
	// clear notifications container
	m_lives.clear();

	// delete life sprite
	if(m_life)
	{
		delete m_life;
		m_life = nullptr;
	}

	// delete health sprite
	if(m_health)
	{
		delete m_health;
		m_health = nullptr;
	}
}

void
Hud::SetHealth(int health)
{
	if(m_health->IsActive())
	{
		// get current health bar rect
		RECT r;
		r = m_health->GetRect();

		// set right to fraction of 
		r.right = m_health->GetWidth() * ((float)health / PlayerConstants::MAX_HEALTH);

		// set rect
		m_health->SetRect(r);
	}
}

void 
Hud::SetLives(int lives)
{
	m_numLives = lives;
}
