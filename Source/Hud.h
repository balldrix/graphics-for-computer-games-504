// Hud.h
// Christopher Ball 2017
// Container for all hud elements like 
// health and life sprites

#ifndef _HUD_H_
#define _HUD_H_

#include "pch.h"
#include "Sprite.h"

// forward declarations
class Graphics;

class Hud
{
public:
	Hud();
	~Hud();
	void	Init(Graphics* graphics,
				 SpriteBatch* spriteBatch);		// initialise hud
	void	Update(float deltaTime);
	void	Render(SpriteBatch* spriteBatch);
			
	void	ReleaseAll();						// release all sprites
	void	DeleteAll();						// delete pointers
		
	void	SetHealth(int health);				// set health bar
	void	SetLives(int lives);				// take life away

private:
	Sprite*	m_health;							// health bar
	Sprite* m_life;								// life heart

	std::vector<Sprite*> m_lives;				// lives container

	float m_screenWidth;							// screen width
	float m_screenHeight;							// screen height

	int m_numLives;								// num lives
};

#endif _HUD_H_
