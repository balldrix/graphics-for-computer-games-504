#include "Level.h"

Level::Level()
{
}

Level::~Level()
{
}

bool
Level::LoadGroundFloor(std::string filename)
{
	char comma = ' '; // char to read commas in csv file
	std::ifstream file; // ifstream file buffer
	file.open(filename); // opens file and reads to buffer
	if(file) // if file is open
	{
		for(unsigned int z = 0; z < MAZE_DEPTH; z++)
		{
			for(unsigned int x = 0; x < MAZE_WIDTH; x++)
			{
				// use temp char so code can skip commas
				int temp = 0;
			
				file >> temp; // int from file into temp var
				if(x != MAZE_WIDTH - 1)
				{
					file >> comma;
				}
			
				m_groundLevel.push_back(temp); // set the character to the grid space in map list
			}
		}
	}
	else
	{	
		return false;
	}
	file.close(); // close file
	
	return true;
}

bool 
Level::LoadSecondFloor(std::string filename)
{
	char comma = ' '; // char to read commas in csv file
	std::ifstream file; // ifstream file buffer
	file.open(filename); // opens file and reads to buffer
	if(file) // if file is open
	{
		for(unsigned int z = 0; z < MAZE_DEPTH; z++)
		{
			for(unsigned int x = 0; x < MAZE_WIDTH; x++)
			{
				// use temp char so code can skip commas
				int temp = 0;

				file >> temp; // int from file into temp var
				if(x != MAZE_WIDTH - 1)
				{
					file >> comma;
				}

				m_secondLevel.push_back(temp); // set the character to the grid space in map list
			}
		}
	}
	else
	{
		return false;
	}
	file.close(); // close file

	return true;
}

void
Level::Delete()
{
	m_secondLevel.clear();
	m_groundLevel.clear();
}
