// Level.h
// Christopher Ball 2017
// A class to manage the loading of the maze
// from a text file

#ifndef _LEVEL_H_
#define _LEVEL_H_

#include "pch.h"

// constants
const unsigned int MAZE_WIDTH = 42; // maze width
const unsigned int MAZE_DEPTH = 21; // maze depth

// cell type by value
namespace CellType
{
	const int EMPTY = -1;	// empty space
	const int WALL = 3;		// wall
	const int PATH = 2;		// path
	const int RAMP = 4;		// stairs ramp
};

class Level
{
public:
	Level();
	~Level(); 
	
	bool				LoadGroundFloor(std::string filename); // load map grid from file into map array

	bool				LoadSecondFloor(std::string filename); // load map grid from file into map array

	int					GetGroundFloorMap(unsigned int x, unsigned int z) const { return m_groundLevel[(MAZE_WIDTH*z) + x]; } // return value of specific cell in 2d array
	
	int					GetSecondFloorMap(unsigned int x, unsigned int z) const { return m_secondLevel[(MAZE_WIDTH*z) + x]; } // return value of specific cell in 2d array


	void				Delete(); // clears level containers

private:
	std::vector<int>	m_groundLevel;		 // array of ground floor map
	std::vector<int>	m_secondLevel;		 // array of second floor map
};

#endif _LEVEL_H_

