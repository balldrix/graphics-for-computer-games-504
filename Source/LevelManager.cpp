#include "pch.h"
#include "LevelManager.h"
#include "WaypointManager.h"
#include "NPCManager.h"
#include "Level.h"

LevelManager::LevelManager() :
	m_currentLevel(0),
	m_waypointManager(nullptr),
	m_NPCManager(nullptr),
	m_level(nullptr)
{
}

LevelManager::~LevelManager()
{
	if(m_level) // if level is not null
	{
		// delete level and null pointer
		delete m_level; 
		m_level = nullptr;
	}
}

bool 
LevelManager::Init(WaypointManager* waypointManager,
				   NPCManager* NPCManager)
{
	// set pointers
	m_waypointManager = waypointManager;
	m_NPCManager = NPCManager;

	// init level pointer
	m_level = new Level();

	// set level to first level in list
	m_currentLevel = LEVEL_ONE;

	// load level
	if(!LoadLevel(m_currentLevel))
	{
		return false; // if level fails to load return false
	}
	return true; // level loaded ok and wasn't the last level
}

bool
LevelManager::SwitchLevel()
{
	m_currentLevel++; // increase level 

	if(m_currentLevel == MAX_LEVELS)
	{
		return false; // if all levels are complete return false
	}

	if(!LoadLevel(m_currentLevel))
	{
		return false; // if level fails to load return false
	}
	return true; // level loaded ok and wasn't the last level
}

bool 
LevelManager::LoadLevel(unsigned int num)
{
	// set num to string value num
	std::string digit = std::to_string(num); 

	// set file paths with contatenation
	std::string waypointFilepath = "Assets\\Levels\\Level" + digit + "\\Level" + digit + "_Waypoints.txt";

	std::string NPCFilePath = "Assets\\Levels\\Level" + digit + "\\Level" + digit + "_NPCs.txt";

	std::string groundFloor = "Assets\\Levels\\Level" + digit + "\\Level" + digit + "_Maze Objects Ground Floor.csv";

	std::string secondFloor = "Assets\\Levels\\Level" + digit + "\\Level" + digit + "_Maze Objects Second Floor.csv";

	// initialise level
	if(!m_waypointManager->LoadWaypoints(waypointFilepath))
	{
		return false;
	}

	if(!m_NPCManager->LoadNPCs(NPCFilePath))
	{
		return false;
	}

	if(!m_level->LoadGroundFloor(groundFloor))
	{
		return false; // if loading fails return false
	}

	if(!m_level->LoadSecondFloor(secondFloor))
	{
		return false; // if loading fails return false
	}

	return true; // level loading worked
}

void LevelManager::SetLevel(int num)
{
	m_currentLevel = num;
}

void LevelManager::DeleteLevel()
{
	m_level->Delete(); // delete maps stored in containers
}
