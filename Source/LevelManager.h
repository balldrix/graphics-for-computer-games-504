// LevelManager.h
// Christopher Ball 2017
// This class manages the level by storing 
// container of levels to load

#ifndef _LEVEL_MANAGER_H_
#define _LEVEL_MANAGER_H_

class NPCManager;
class WaypointManager;
class Level;
class NPC;

// enum of list of levels
enum LevelList
{
	LEVEL_ONE = 1, // start enum at 1
	MAX_LEVELS // used to check if reached last level
};

class LevelManager
{
public:
	LevelManager();
	~LevelManager();

	bool		 Init(WaypointManager* waypointManager,
					  NPCManager* NPCManager); // initialise level manager
	bool		 SwitchLevel(); // switch level to next one
	bool		 LoadLevel(unsigned int num); // load specific level
	void		 SetLevel(int num); // set current level
	Level*		 GetLevel() const { return m_level; } // get current level number
	unsigned int GetCurrentLevel() const { return m_currentLevel; } // get pointer to current loaded level
	void		 DeleteLevel(); // delete level to clear maps

private:
	unsigned int m_currentLevel; // current level number 
	WaypointManager*	m_waypointManager; // pointer to waypoint manager
	NPCManager*			m_NPCManager; // pointer to npc manager
	Level*		 m_level; // pointer to level class
	LevelList	 m_levelList; // list of level enumerator


};

#endif _LEVEL_MANAGER_H_