// Material.h
// Christopher Ball 2017
// Material class to hold data related 
// to textures, lighting and shaders

#ifndef _MATERIAL_H_
#define _MATERIAL_H_

#include "pch.h"

// forward declarations
class Graphics;
class Texture;
class Shader;

const int MAX_TEXTURES = 2;

class Material
{
public:
	Material();
	~Material();
	// initialise methods
	void Init(Shader* shader);	
	void Init(Texture* texture, Shader* shader);
	void Init(Texture* texture, Texture* normalMap, Shader* shader);

	void RenderSetup(Graphics* graphics);		// prepare pipeline for rendering materials
	void ShadowRenderSetup(Graphics* graphics);	// prepare pipeline for shadow mapping

private:
	ID3D11ShaderResourceView* m_texture[MAX_TEXTURES];		// Shader Resource Views
	ID3D11VertexShader* m_vertexShader;			// vertex shader
	ID3D11PixelShader* m_pixelShader;			// pixel shader
	ID3D11InputLayout* m_inputLayout;			// vertex input layout
};

#endif _MATERIAL_H_
