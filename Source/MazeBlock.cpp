#include "MazeBlock.h"

MazeBlock::MazeBlock()
{
}

MazeBlock::~MazeBlock()
{
}

void MazeBlock::Update()
{
	// set up bounding box
	m_hitbox.SetAABB(Vector3(-1.0f, -1.0f, -1.0f) * m_scale, Vector3(1.0f, 1.0f, 1.0) * m_scale);
	m_hitbox.OffSetAABB(m_position);

	// setup scale matrix
	m_scaleMatrix = XMMatrixScaling(m_scale.x, m_scale.y, m_scale.z);

	// translate model position
	m_modelMatrix = XMMatrixTranslationFromVector(m_position);

	// create final model world matrix
	m_modelMatrix = m_rotationalMatrix * m_scaleMatrix * m_modelMatrix;
}