// MazeBlock.h
// Christopher Ball 2017
// The wall object is a child of GameObject and 
// is used to make up the walls of the maze

#ifndef _MAZEBLOCK_H_
#define _MAZEBLOCK_H_

#include "GameObject.h"

class MazeBlock : public GameObject
{
public:
	MazeBlock();
	~MazeBlock();
	void Update(); // overide update method
};

#endif _MAZEBLOCK_H_
