#include "MeshObject.h"
#include "Graphics.h"
#include "ConstantBuffers.h"

MeshObject::MeshObject() :
	m_vertexBuffer(nullptr),
	m_indexBuffer(nullptr),
	m_vertexCount(0),
	m_indexCount(0),
	m_stride(0)
{}

MeshObject::~MeshObject()
{
}

void 
MeshObject::Render(Graphics* graphics)
{
	UINT offset = 0;

	// set vertex buffer
	graphics->GetDeviceContext()->IASetVertexBuffers(0, 1, &m_vertexBuffer, &m_stride, &offset);

	// set index buffer
	graphics->GetDeviceContext()->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R16_UINT, 0);

	// set primitive topology
	graphics->GetDeviceContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// render indexed
	graphics->GetDeviceContext()->DrawIndexed(m_indexCount, 0, 0);
}

void
MeshObject::Release()
{
	// release all buffers

	if(m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = nullptr;
	}

	if(m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = nullptr;
	}
}