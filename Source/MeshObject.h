// MeshObject.h
// Christopher Ball 2017
// The mesh class is a base class to hold the vertex and index buffers
// a child class will specify the vertices and indices specific to 
// that mesh. Modified code from SumoDX by Mickey Macdonald @scruffyfurn

#ifndef _MESHOBJECT_H_
#define _MESHOBJECT_H_

#include "pch.h"

// forward declarations
class Graphics;

class MeshObject
{
public:
	MeshObject();
	~MeshObject();
	virtual void Init(Graphics* graphics) = 0; // pure virtual method to initialise mesh object
	virtual void Render(Graphics* graphics); // render mesh object
	void Release();

protected:
	ID3D11Buffer*	m_vertexBuffer; // vertex buffer
	ID3D11Buffer*	m_indexBuffer; // index buffer
	int				m_vertexCount; // number of vertices
	int				m_indexCount; // number of indices
	UINT			m_stride;		// size of vertex struct
};

#endif _MESHOBJECT_H_