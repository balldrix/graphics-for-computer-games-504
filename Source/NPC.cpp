#include "pch.h"
#include "NPC.h"
#include "AStarNode.h"
#include "AStarPathfinding.h"
#include "Bullet.h"
#include "ParticleSystem.h"
#include "ParticleEmitter.h"

#include "Constants.h"

NPC::NPC() :
	m_particleSystem(nullptr),
	m_pathfinding(nullptr),
	m_previousWaypoint(nullptr),
	m_nextWaypoint(nullptr),
	m_direction(Vector3(0.0f, 0.0f, 0.0f)),
	m_destination(Vector3(0.0f, 0.0f, 0.0f)),
	m_waypointIndex(0),
	m_target(nullptr),
	m_NPCState(NPCAIState::NPC_ROAMING),
	m_lineOfSight(false),
	m_bullet(nullptr),
	m_shootingTimer(0.0f)
{
	m_sphereCollider.position = Vector3(0.0f, 0.0f, 0.0f);
	m_sphereCollider.radius = 0.5f;
}

NPC::~NPC()
{
	// delete bullet
	if(m_bullet != nullptr)
	{
		delete m_bullet;
		m_bullet = nullptr;
	}

	// reset path
	if(!m_path.empty())
	{
		if(m_start != nullptr)
		{
			delete m_start;
			m_start = nullptr;
		}

		if(m_goal != nullptr)
		{
			delete m_goal;
			m_goal = nullptr;
		}

		for(int i = 0; i < m_path.size(); i++)
		{
			delete m_path[i];
			m_path[i] = nullptr;
		}
	}

	if(m_pathfinding != nullptr)
	{
		delete m_pathfinding;
		m_pathfinding = nullptr;
	}
}

void 
NPC::Update(float deltaTime)
{
	// update bullet if active
	if(m_bullet->IsActive())
	{
		m_bullet->Update(deltaTime);
	}

	// ai logic
	switch(m_NPCState)
	{
	case NPC_ROAMING:
		m_NPCState = Roaming();
		break;
	case NPC_SHOOTING:
		m_NPCState = Shooting();
		break;
	default:
		break;
	}

	// update position with new velocity
	m_position = m_position - m_velocity * deltaTime;

	// translate model position
	m_modelMatrix = XMMatrixTranslationFromVector(m_position);

	// update collider
	m_sphereCollider.position = m_position;

	// update shooting timer
	m_shootingTimer += deltaTime;
}

void 
NPC::Release()
{

}

void 
NPC::AddWaypoint(const Waypoint* waypoint)
{
	// if waypoint is emppty
	if(m_waypointList.size() == 0)
	{
		// set start position at first waypoint
		m_position = waypoint->position;
	}

	// if next waypoint has not been set
	if(m_nextWaypoint == nullptr)
	{
		// set next waypoint
		m_nextWaypoint = waypoint;
	}

	// push waypoint to list
	m_waypointList.push_back(waypoint);
}

void 
NPC::AddPathfinding(bool map[])
{
	m_pathfinding = new AStarPathfinding();
	m_pathfinding->Init(map);
}

void 
NPC::ArrivedAtWaypoint()
{
	// increase index
	m_waypointIndex++;

	// if index is high than size of list
	// reset the index to 0
	if(m_waypointIndex == m_waypointList.size())
	{
		m_waypointIndex = 0;
	}

	// set previous and new next waypoints
	m_previousWaypoint = m_nextWaypoint;
	m_nextWaypoint = m_waypointList[m_waypointIndex];

	// clear path
	m_path.clear();
}

void 
NPC::FindNewPath()
{
	// reset path and start, goal nodes
	if(!m_path.empty())
	{
		if(m_start != nullptr)
		{
			delete m_start;
			m_start = nullptr;
		}

		if(m_goal != nullptr)
		{
			delete m_goal;
			m_goal = nullptr;
		}
		
		for(int i = 0; i < m_path.size(); i++)
		{
			delete m_path[i];
			m_path[i] = nullptr;
		}

		m_path.clear();
	}

	m_start = new AStarNode();
	m_goal = new AStarNode();

	// create start and goal nodes
	m_goal->Init(m_nextWaypoint->position.x / GlobalConstants::CELL_WIDTH,
			   -(m_nextWaypoint->position.z / GlobalConstants::CELL_WIDTH),
			   nullptr,
			   nullptr);
	m_start->Init(m_previousWaypoint->position.x / GlobalConstants::CELL_WIDTH,
				-(m_previousWaypoint->position.z / GlobalConstants::CELL_WIDTH),
				nullptr,
				  m_goal);

	// find path to waypoint
	m_pathfinding->FindPath(m_start, m_goal, m_path);
}

void
NPC::FollowPath()
{
	// if path is empty
	if(m_path.empty())
	{
		// find new path
		FindNewPath();

		// set destination of next node in path list
		m_destination = GetNodeWorldPosition(m_path.back());
	}

	// npc is at node
	if(Vector3(m_position - m_destination).Length() < 0.05f)
	{
		// remove node from path list
		m_path.pop_back();

		// set new node to follow
		m_destination = GetNodeWorldPosition(m_path.back());
	}

	// get direction from npc to node
	m_direction = m_position - m_destination;
	m_direction.Normalize();

	m_velocity = m_direction * NPCConstants::NPC_SPEED;
}

void
NPC::AddBullet(Bullet* bullet)
{
	m_bullet = bullet;
}

void 
NPC::RenderBullet(Graphics* graphics)
{
	// only render if the bullet is active
	if(m_bullet->IsActive())
	{
		m_bullet->Render(graphics);	
	}
}

void
NPC::AddParticleSystem(ParticleSystem* system)
{
	m_particleSystem = system;
}

void
NPC::SetTarget(GameObject* target)
{
	m_target = target;
}

void 
NPC::SetAIState(const NPCAIState& state)
{
	m_NPCState = state;
}

void 
NPC::SetLineOfSight(bool los)
{
	m_lineOfSight = los;
}

Vector3
NPC::GetNodeWorldPosition(AStarNode* node)
{
	// convert node grid position to world position
	Vector3 nodePosition;
	nodePosition.x = node->m_x * GlobalConstants::CELL_WIDTH;
	nodePosition.y = m_position.y;
	nodePosition.z = node->m_z * GlobalConstants::CELL_WIDTH;
	nodePosition.z = -nodePosition.z;
	return nodePosition;
}

NPCAIState 
NPC::Roaming()
{
	// check distance from waypoint
	Vector3 distance = m_position - m_nextWaypoint->position;

	// if waypoint reached, update waypoints
	if(distance.Length() < 0.05f)
	{
		ArrivedAtWaypoint();
	}

	// allow npc to follow path to next waypoint
	FollowPath();

	// check line of sight
	if(m_lineOfSight)
	{
		// if target is in sight
		// set state to shooting
		return NPCAIState::NPC_SHOOTING;
	}
	else
	{
		return NPCAIState::NPC_ROAMING;
	}
}

NPCAIState 
NPC::Shooting()
{
	Vector3 distance = m_position - m_destination;

	// if npc is at destination node
	if(distance.Length() < 0.05f)
	{
		// stop moving
		m_velocity = Vector3(0.0f, 0.0f, 0.0f);

		// shoot bullet at target
		if(m_shootingTimer > NPCConstants::NPC_FIRINGRATE)
		{
			m_shootingTimer = 0.0f;

			m_bullet->SetActive(true);
			m_bullet->SetPosition(m_position);
			m_particleSystem->GetEmitter("FIRE_SMOKE")->Begin(m_position + Vector3(0.0f, 0.8, 0.0f));
			m_particleSystem->GetEmitter("FIRE")->Begin(m_position + Vector3(0.0f, 0.5, 0.0f));

			// calculate new velocity
 			Vector3 direction = m_target->GetPosition() - m_position;
   			direction.Normalize();

			m_bullet->SetVelocity(direction * NPCConstants::BULLET_SPEED);
		}
	}
	else
	{
		return NPCAIState::NPC_ROAMING;
	}

	// if npc still has line of sight
	if(m_lineOfSight)
	{
		return NPCAIState::NPC_SHOOTING;
	}
	else
	{
		return NPCAIState::NPC_ROAMING;
	}
}
