// NPC.h
// Christopher Ball 2017
// Enemy ai which patrols the maze,

#ifndef _NPC_H_
#define _NPC_H_

#include "pch.h"
#include "GameObject.h"

#include "WaypointManager.h"

class AStarNode;
class AStarPathfinding;
class ParticleSystem;
class Bullet;

// npc states for ai
enum NPCAIState
{
	NPC_ROAMING,
	NPC_SHOOTING
};

// inherit from game object
class NPC : public GameObject
{
public:
	NPC();
	~NPC();
	void							Update(float deltaTime);						// update model matrix
	void							Release();										// release unique pointers

	void							AddWaypoint(const Waypoint* waypoint);			// add waypoint to list
	void							AddPathfinding(bool map[]);						// add pathfinding component
	void							ArrivedAtWaypoint();							// update waypoints
	void							FindNewPath();									// calculate new path to next wp
	void							FollowPath();									// set velocity to follow path

	void							AddBullet(Bullet* bullet);						// add bullets to container
	void							RenderBullet(Graphics* graphics);				// render bullet
	void							AddParticleSystem(ParticleSystem* system);		// copy pointer to particle system
	
	void							SetTarget(GameObject* target);					// set target object for shooting / los
	void							SetAIState(const NPCAIState& state);			// set ai state
	void							SetLineOfSight(bool los);						// set line of sight state
	GameObject*						GetTarget() const { return m_target; }			// return pointer to npc's target
	NPCAIState						GetState() const { return m_NPCState; }		// return current AI state

	Vector3							GetNodeWorldPosition(AStarNode* node);			// gets distance from node to npc position

	AStarPathfinding*				GetPathfinding() const { return m_pathfinding;} // get pointer to pathfinding 
	Bullet*							GetBullet() const { return m_bullet; }			// return pointer to bullet

	// AI logic
	NPCAIState Roaming();
	NPCAIState Shooting();

private:
	ParticleSystem*					m_particleSystem;								// pointer to particle system
	AStarPathfinding*				m_pathfinding;									// pointer to pathfinding component
	AStarNode*						m_start;										// start node
	AStarNode*						m_goal;											// goal node
	std::vector<const Waypoint*>	m_waypointList;									// list of waypoints to patrol
	std::vector<AStarNode*>			m_path;											// list of nodes to travel with pathfinding
	const Waypoint*					m_previousWaypoint;								// previous waypoint 
	const Waypoint*					m_nextWaypoint;									// current waypoint
	Vector3							m_direction;									// movement direction
	Vector3							m_destination;									// next node
	int								m_waypointIndex;								// waypoint tracker
	GameObject*						m_target;										// player target
	NPCAIState						m_NPCState;										// ai state
	bool							m_lineOfSight;									// NPC has line of sight of target
	Bullet*							m_bullet;										// projectile
	float							m_shootingTimer;									// bullet timer
};

#endif _NPC_H_
