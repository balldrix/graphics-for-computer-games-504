#include "pch.h"
#include "NPCManager.h"
#include "Graphics.h"
#include "NPC.h"

NPCManager::NPCManager()
{
}

NPCManager::NPCManager(WaypointManager* waypointManager)
{
	m_waypointManager = waypointManager;
}

NPCManager::~NPCManager()
{
}

bool
NPCManager::LoadNPCs(std::string filename)
{
	std::ifstream file; // ifstream file buffer
	file.open(filename); // opens file and reads to buffer
	if(file) // if file is open
	{
		std::string ID;
		int waypointCounter;
		std::string waypoint;

		// loop until end of file
		while(!file.eof())
		{
			// ready data
			file >> ID;
			file >> waypointCounter;

			NPC* npc = new NPC();
			npc->SetID(ID);
		
			// add waypoints
			for(int i = 0; i < waypointCounter; i++)
			{
				file >> waypoint;
				npc->AddWaypoint(m_waypointManager->GetWaypoint(waypoint));
			}

			// add npc to list
			m_NPCList.push_back(npc);
		}
	}
	else
	{
		return false;
	}
	file.close(); // close file
	return true;
}

void
NPCManager::Render(Graphics* graphics)
{
	for(int i = 0; i < m_NPCList.size(); i++)
	{
		m_NPCList[i]->Render(graphics);
		m_NPCList[i]->RenderBullet(graphics);
	}
}

void NPCManager::RenderToShadowMap(Graphics* graphics)
{
	for(int i = 0; i < m_NPCList.size(); i++)
	{
		m_NPCList[i]->RenderToShadowMap(graphics);
	}
}

void 
NPCManager::Update(float deltaTime)
{
	for(int i = 0; i < m_NPCList.size(); i++)
	{
		m_NPCList[i]->Update(deltaTime);
	}
}

NPC* 
NPCManager::GetNPC(int index)
{
	return m_NPCList[index];
}

void 
NPCManager::DeleteAll()
{
	for(int i = 0; i < m_NPCList.size(); i++)
	{
		delete m_NPCList[i];
		m_NPCList[i] = nullptr;
	}

	m_NPCList.clear();
}

int 
NPCManager::GetNumNPCs()
{
	return m_NPCList.size();
}
