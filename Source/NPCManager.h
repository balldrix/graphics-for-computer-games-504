// NPCManager.h
// Christopher Ball 2017
// manages list of npcs

#ifndef _NPC_MANAGER_H_
#define _NPC_MANAGER_H_

#include "pch.h"

class Graphics;
class NPC;
class WaypointManager;

class NPCManager
{
public:
	NPCManager();
	NPCManager(WaypointManager* waypointManager);
	~NPCManager();

	bool		LoadNPCs(std::string filename);	// load npcs from txt file

	void		Render(Graphics* graphics);	// render npcs
	void		RenderToShadowMap(Graphics* graphics); // render pass for shadows
	void		Update(float deltaTime);	// update npcs

	NPC*		GetNPC(int index);			// get npc from list
	void		DeleteAll();				// delete all npcs

	int			GetNumNPCs();				// return npc amount in list

private:
	WaypointManager*	m_waypointManager;	// pointer to waypoint manager
	std::vector<NPC*>	m_NPCList;			// list of npcs

};

#endif	_NPC_MANAGER_H_