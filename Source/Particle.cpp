#include "pch.h"
#include "Particle.h"
#include "Camera.h"

Particle::Particle() :
	m_target(nullptr),
	m_age(0.0f),
	m_gravity(0.0f),
	m_speed(0.0f),
	m_maxAge(0.0f)
{
}

Particle::~Particle()
{
}

void 
Particle::Update(float deltaTime)
{
	Matrix billboardY = XMMatrixIdentity();
	Matrix billboardX = XMMatrixIdentity();

	if(m_target) // check flag has a target turn to face target
	{
		Vector3 eye = m_target->GetEyePosition();
		eye.y = 0.0;
		// get vector point of target
		Vector3 lookAt = eye - m_position;
		lookAt.Normalize();

		// angle is inverse cos of look at z axis
		float angle = acos(lookAt.z);

		// if camera is left of mesh
		if(lookAt.x < 0)
		{
			// inverse angle
			angle *= -1.0f;
		}

		// set rotation matrix by rotating around y
		billboardY = XMMatrixRotationY(angle);

		eye = m_target->GetEyePosition();
		eye.z = 0.0;
		lookAt = eye - m_position;
		lookAt.Normalize();

		angle = asin(lookAt.z);

		// if camera is left of mesh
		if(lookAt.y > 0)
		{
			// inverse angle
			angle *= -1.0f;
		}

		billboardX = XMMatrixRotationX(angle);
	}

	// update particle position
	m_velocity = (m_velocity - Vector3(0.0f, m_gravity, 0.0f));
	m_position += (m_velocity * m_speed) * deltaTime;

	// set up particle rotation
	m_rotationalMatrix = XMMatrixRotationZ(XMConvertToRadians(m_rotation));

	// setup scale matrix
	m_scaleMatrix = XMMatrixScaling(m_scale.x, m_scale.y, m_scale.z);

	// translate model position
	m_modelMatrix = XMMatrixTranslationFromVector(m_position);

	// create final model world matrix
	m_modelMatrix = m_rotationalMatrix * billboardX * billboardY * m_scaleMatrix * m_modelMatrix;

	// set alpha
	m_alpha = 1 - (m_age / m_maxAge);

	// update timer
	m_age += deltaTime;
}

void 
Particle::SetTarget(Camera* target)
{
	m_target = target;
}

void 
Particle::SetGravity(float gravity)
{
	m_gravity = gravity;
}

void 
Particle::SetAge(float age)
{
	m_age = age;
}

void 
Particle::SetSpeed(float speed)
{
	m_speed = speed;
}

void 
Particle::SetMaxAge(float maxAge)
{
	m_maxAge = maxAge;
}
