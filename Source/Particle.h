// Particle.h
// Christopher Ball 2017
// particle object class

#ifndef _PARTICLE_H_
#define _PARTICLE_H_

#include "GameObject.h"

// forward declarations
class Camera;

class Particle : public GameObject
{
public:
	Particle();
	~Particle();
	void Update(float deltaTime);	// update particle

	void SetTarget(Camera* target); // set target for billboarding 
	void SetGravity(float gravity); // set gravity amount for this particle
	void SetAge(float age); // set new age
	void SetSpeed(float speed); // set speed
	void SetMaxAge(float maxAge); // set max age
	
	float GetAge() const { return m_age; } // returns current age


private:
	Camera* m_target;			// target look at position
	float m_age;				// particle current age
	float m_gravity;			// gravity amount
	float m_speed;				// speed of particle
	float m_maxAge;				// max age
};

#endif _PARTICLE_H_