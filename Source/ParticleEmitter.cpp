#include "pch.h"
#include "ParticleEmitter.h"
#include "Particle.h"
#include "Randomiser.h"

ParticleEmitter::ParticleEmitter() :
	m_ID(""),
	m_startPosition(0.0f, 0.0f, 0.0f),
	m_numParticles(0.0f),
	m_maxParticles(0.0f),
	m_rate(0.0f),
	m_particleMaxAge(0.0f),
	m_loop(false),
	m_active(false),
	m_timer(0.0f),
	m_distance(0.0f),
	m_shape(NONE)
{
}

ParticleEmitter::~ParticleEmitter()
{
	DeleteAll();
}

void
ParticleEmitter::Init(Camera* target, std::string ID, Particle* particle, ParticleEmisionShape shape, float size, float speed, bool rotation, float gravity, int numParticles, int maxParticles, float rate, float particleLifetime, float distance, bool loop)
{
	// store parameters
	m_ID = ID;
	m_numParticles = numParticles;
	m_maxParticles = maxParticles;
	m_rate = rate;
	m_speed = speed;
	m_particleMaxAge = particleLifetime;
	m_loop = loop;
	m_shape = shape;
	m_distance = distance;
	m_count = 0;

	m_velocity = Vector3(0.0f, 0.0f, 0.0f);

	// load container with particles
	for(unsigned int i = 0; i < m_maxParticles; i++)
	{
		// set up particles
		Particle* m_particle = new Particle();
		m_particle->Init(particle->GetMesh(), particle->GetMaterial());
		m_particle->SetScale(Vector3(size, size, size));
		m_particle->SetGravity(gravity);
		m_particle->SetTarget(target);
		m_particle->SetActive(false);
		m_particle->SetSpeed(speed);
		m_particle->SetMaxAge(m_particleMaxAge);
		m_particle->SetAge(0.0f);

		if(rotation)
		{
			m_particle->SetAngle(Randomiser::GetRandNum(0,360));
		}

		if(shape == SPHERE)
		{
			// set random velocity for sphere shape emitter
			float x = Randomiser::GetRandNum(0, 10);
			float y = Randomiser::GetRandNum(0, 10);
			float z = Randomiser::GetRandNum(0, 10);

			// convert rand num to number between -1 and +1
			m_velocity.x = ((x / 10) * 2) - 1;
			m_velocity.y = ((y / 10) * 2) - 1;
			m_velocity.z = ((z / 10) * 2) - 1;

			m_velocity.Normalize();
		}
		else
		{
			m_velocity = Vector3(0.0f, speed, 0.0f);
		}

		m_particle->SetVelocity(m_velocity);

		// add to container
		m_particleList.push_back(m_particle);
	}
}

void 
ParticleEmitter::Begin(Vector3 position)
{
	// activate emitter
	m_active = true;
	m_currentPosition = position;
	m_count = 0;
	m_timer = 0.0f;

	// loop through particle list and activate 
	for(unsigned int i = 0; i < m_numParticles; i++)
	{
		EmitParticle(i);
		m_count++;
	}
}

void 
ParticleEmitter::End()
{
	// make emitter end loop
	m_loop = false;
	m_count = m_maxParticles;
}

void 
ParticleEmitter::Update(float deltaTime)
{	
	// loop through particles to update and
	// to check if any have reached max age
	for(unsigned int i = 0; i < m_maxParticles; i++)
	{
		Particle* particle = m_particleList[i];
		if(particle->IsActive())
		{
			// update active particle
			particle->Update(deltaTime);

			// if particle has reached max age
			// check if emitter is a looper to restart particle
			// or kill particle
			if(particle->GetAge() > m_particleMaxAge)
			{
				if(m_loop)
				{
					// reset particle
					EmitParticle(i);
				}
				else
				{
					// kill particle
					particle->SetActive(false);
					//m_count--;
					particle->SetAge(0.0f);
					// check if all particles are dead
					CheckEmitterLife();
				}
			}
		}
	}

	// check rate of birth and emit another particle
	// if max hasn't been reached
	if(m_count < m_maxParticles)
	{
		if(m_timer > m_rate)
		{
			// loop through list to activate another
			for(unsigned int i = 0; i < m_particleList.size(); i++)
			{
				Particle* particle = m_particleList[i];

				if(!particle->IsActive())
				{
					EmitParticle(i);
					break;
				}
			}
			m_timer = 0.0f;
		}
	}

	// update timer
	m_timer += deltaTime;
}

void 
ParticleEmitter::Render(Graphics* graphics)
{
	// loop through particles and render
	for(unsigned int i = 0; i < m_maxParticles; i++)
	{
		Particle* particle = m_particleList[i];
		if(particle->IsActive())
		{
			particle->Render(graphics);
		}
	}
}

void 
ParticleEmitter::DeleteAll()
{
	// delete emitter
	for(unsigned int i = 0; i < m_particleList.size(); i++)
	{
		delete m_particleList[i];
		m_particleList[i] = nullptr;
	}

	m_particleList.clear();
}

void 
ParticleEmitter::EmitParticle(int i)
{
	Particle* particle = m_particleList[i];
	particle->SetActive(true);
	particle->SetAge(0.0f);

	Vector3 offset = Vector3(0.0f, 0.0f, 0.0f);

	if(m_shape == RING)
	{
		// set random position for sphere shape emitter
		float x = Randomiser::GetRandNum(0, 10);
		float y = Randomiser::GetRandNum(0, 10);
		float z = Randomiser::GetRandNum(0, 10);

		// convert rand num to number between -1 and +1
		offset.x = ((x / 10) * 2) - 1;
		offset.y = 0.0f;
		offset.z = ((z / 10) * 2) - 1;
		offset.Normalize();
	}
	
	if(m_shape == SPHERE)
	{
		// set random velocity for sphere shape emitter
		float x = Randomiser::GetRandNum(0, 10);
		float y = Randomiser::GetRandNum(0, 10);
		float z = Randomiser::GetRandNum(0, 10);

		// convert rand num to number between -1 and +1
		m_velocity.x = ((x / 10) * 2) - 1;
		m_velocity.y = ((y / 10) * 2) - 1;
		m_velocity.z = ((z / 10) * 2) - 1;

		m_velocity.Normalize();
	}
	else
	{
		m_velocity = Vector3(0.0f, m_speed, 0.0f);
	}

	particle->SetVelocity(m_velocity);


	particle->SetPosition(m_currentPosition + (offset * m_distance));
}

void
ParticleEmitter::SetPosition(Vector3 position)
{
	m_currentPosition = position;
}

void 
ParticleEmitter::CheckEmitterLife()
{
	// loop through list to check if all particles are inactive
	for(unsigned int i = 0; i < m_maxParticles; i++)
	{
		Particle* particle = m_particleList[i];
		if(particle->IsActive())
		{
			// if any particles are still active then
			// return without doing anything else
			return;
		}
	}

	// if above loop manages to end without returning 
	// then all particles are inactive
	m_active = false;
	m_timer = 0.0f;
	m_count = 0.0f;
}
