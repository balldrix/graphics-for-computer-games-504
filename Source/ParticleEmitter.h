// ParticleEmitter.h
// Christopher Ball 2017
// manages the effect using particles

#ifndef _PARTICLE_EMITTER_H_
#define _PARTICLE_EMITTER_H_

#include "pch.h"

// forward declarations
class Graphics;
class Particle;
class Camera;

// particle emission shape
enum ParticleEmisionShape
{
	SPHERE,
	LINE,
	RING,
	CONE,
	NONE
};

class ParticleEmitter
{
public:
	ParticleEmitter();
	~ParticleEmitter();
	void Init(Camera* target,
			  std::string ID, 
			  Particle* particle, 
			  ParticleEmisionShape shape,
			  float size,
			  float speed,
			  bool rotation,
			  float gravity,
			  int numParticles,
			  int maxParticles,
			  float rate,
			  float particleLifetime,
			  float distance,
			  bool loop);				// initialise particle effect
	void Begin(Vector3 position);						// begin effect
	void End();							// stop effect
	void Update(float deltaTime);		// update effect
	void Render(Graphics* graphics);	// render particles
	void DeleteAll();					// delete all particles
	void EmitParticle(int i);				// emit a particle
	void SetPosition(Vector3 position); // set new emitter position

	bool IsActive() const { return m_active;  } // return active status

	void CheckEmitterLife(); // check if all particles 

	std::string GetID() const { return m_ID; }

private:
	std::string m_ID;			// ID of emmitter effect
	std::vector<Particle*> m_particleList;	// list of particles
	Vector3 m_startPosition;	// effect start position
	Vector3 m_currentPosition; // effect position
	int m_numParticles;	// start number of particles
	int m_count;
	int m_maxParticles;	// max particles allowed
	float m_rate;			// rate of particle birth
	float m_speed;			// set speed
	float m_particleMaxAge; // particle max age
	bool m_loop;			// is effect loopable
	float m_timer;			// emission timer
	bool m_active;			// on when emitter is active
	float m_distance;		// distance of emission from centre
	Vector3 m_velocity;		// velocity of particles
	ParticleEmisionShape m_shape; // shape of emitter
};

#endif _PARTICLE_EMITTER_H_
