// ParticleMesh.h
// Christopher Ball 2017
// child of MeshObject, holds data for a particle

#ifndef _PARTICLE_MESH_
#define _PARTICLE_MESH_

// forward declarations
class Graphics;

#include "MeshObject.h"

// inherit from Mesh Object
class ParticleMesh : public MeshObject
{
public:
	ParticleMesh();
	~ParticleMesh();
	void Init(Graphics* graphics); // initialise quad mesh
};

#endif _PARTICLE_MESH_