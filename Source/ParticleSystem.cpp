#include "ParticleSystem.h"
#include "Graphics.h"
#include "ParticleEmitter.h"

ParticleSystem::ParticleSystem()
{
}

ParticleSystem::~ParticleSystem()
{
	DeleteAll();
}

void 
ParticleSystem::Update(float deltaTime)
{
	// loop through particle emitter list 
	// and update active emitters
	for(unsigned int i = 0; i < m_emitterList.size(); i++)
	{
		ParticleEmitter* emitter = m_emitterList[i];
		if(emitter->IsActive())
		{
			emitter->Update(deltaTime);
		}
	}
}

void 
ParticleSystem::Render(Graphics* graphics)
{
	// loop through particle emitter list 
	// and render active emitters
	for(unsigned int i = 0; i < m_emitterList.size(); i++)
	{
		ParticleEmitter* emitter = m_emitterList[i];
		if(emitter->IsActive())
		{
			emitter->Render(graphics);
		}
	}
}

void 
ParticleSystem::DeleteAll()
{
	// loop through particle emitter list 
	// and delete all emitters
	for(unsigned int i = 0; i < m_emitterList.size(); i++)
	{
		if(m_emitterList[i] != nullptr)
		{
			delete m_emitterList[i];
			m_emitterList[i] = nullptr;
		}
	}

	m_emitterList.clear();
}

void 
ParticleSystem::AddParticleEffect(ParticleEmitter* emitter)
{
	m_emitterList.push_back(emitter);
}

ParticleEmitter* 
ParticleSystem::GetEmitter(std::string ID) const
{
	// return emitter with ID parameter
	for(unsigned int i = 0; i < m_emitterList.size(); i++)
	{
		if(m_emitterList[i]->GetID() == ID)
		{
			return m_emitterList[i];
		}
	}
	
	// return null pointer if ID doesn't exist
	return nullptr;
}