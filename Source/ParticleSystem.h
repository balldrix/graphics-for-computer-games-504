// ParticleSystem.h
// Christopher Ball 2017
// particle system class

#ifndef _PARTICLE_SYSTEM_H_
#define _PARTICLE_SYSTEM_H_

#include "pch.h"

// forward declarations
class Graphics;
class ParticleEmitter;

class ParticleSystem
{
public:
	ParticleSystem();
	~ParticleSystem();

	void Update(float deltaTime);			// update particles
	void Render(Graphics* graphics);							// render particles
	void DeleteAll();						// delete all particles
	void AddParticleEffect(ParticleEmitter* emitter);	// add particle to list
	ParticleEmitter* GetEmitter(std::string ID) const;
private:
	std::vector<ParticleEmitter*> m_emitterList;
};

#endif _PARTICLE_SYSTEM_H_
