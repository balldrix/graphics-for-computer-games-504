#include "Player.h"
#include "pch.h"
#include "Constants.h"

Player::Player() :
m_facing(0.0f, 0.0f, -1.0f),
m_angle(0.0f),
m_health(0),
m_lives(0)
{
	m_sphereCollider.position = Vector3(0.0f, 0.0f, 0.0f);
	m_sphereCollider.radius = 0.5f;
}

Player::~Player()
{
}

void 
Player::Update(float deltaTime)
{
	// set rotational matrix
	m_rotationalMatrix = Matrix::CreateRotationY(m_angle);

	// rotate facing direction
	m_facing = Vector3::Transform(m_defaultZAxis, m_rotationalMatrix);
	m_facing.Normalize();

	// update player position
	m_position += m_velocity * deltaTime;

	// transpose model matrix
	m_modelMatrix = XMMatrixTranslationFromVector(m_position);

	// update collider
	m_sphereCollider.position = m_position;

	// create final model world matrix
	m_modelMatrix = m_rotationalMatrix * m_scaleMatrix * m_modelMatrix;
}

void 
Player::SetFacing(Vector3 facing)
{
	m_facing = facing;
}

void 
Player::SetAngle(float angle)
{
	m_angle = angle;
}

void 
Player::SetHealth(int health)
{
	m_health = health;
}

void 
Player::SetLives(int lives)
{
	m_lives = lives;
}

void 
Player::Reset()
{
	m_facing = Vector3(0.0f, 0.0f, 1.0f);
	m_angle = 0.0f;
	m_health = PlayerConstants::MAX_HEALTH;
}
