// Player.h
// Christopher Ball 2017
// class for all player related methods and data

#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "pch.h"
#include "GameObject.h"

// player constants

// inherit from game object
class Player : public GameObject
{
public:
	Player();
	~Player();
	void			Update(float deltaTime);	// update player
	
	void			SetFacing(Vector3 facing);	// set facing direction
	void			SetAngle(float angle);		// set player angle
	void			SetHealth(int health);		// set health
	void			SetLives(int lives);		// set num lives

	Vector3			GetFacing() const { return m_facing; } // get facing direction
	float			GetAngle() const { return m_angle; } // rotational angle
	int				GetHealth() const { return m_health; } // return player health
	int				GetLives() const { return m_lives; } // return lives left

	void			Reset();	// reset player
	
private:
	Vector3			m_facing;	// player facing direction
	float			m_angle;	// rotational angle
	int				m_health;	// health bar
	int				m_lives;	// num lives left
};

#endif _PLAYER_H_