#include "PostProcessing.h"

#include "Graphics.h"
#include "TextureEffect.h"
#include "Shader.h"

#include "Error.h"
#include "Constants.h"

PostProcessing::PostProcessing() :
	m_graphics(nullptr),
	m_horizontalBlurShader(nullptr),
	m_verticalBlurShader(nullptr),
	m_sceneTexture(nullptr),
	m_downSample(nullptr),
	m_horizontalBlurTexture(nullptr),
	m_verticalBlurTexture(nullptr),
	m_upSample(nullptr),
	m_screenWidth(0),
	m_screenHeight(0),
	m_downSampleWidth(0),
	m_downSampleHeight(0)
{
}

PostProcessing::~PostProcessing()
{
}

void 
PostProcessing::Init(Graphics* graphics, SpriteBatch* spriteBatch)
{
	// copy pointers
	m_graphics = graphics;
	m_spriteBatch = spriteBatch;

	// set dimensions
	m_screenWidth = m_graphics->GetWidth();
	m_screenHeight = m_graphics->GetHeight();

	m_downSampleWidth = m_screenWidth / 2;
	m_downSampleHeight = m_screenHeight / 2;

	// load shaders
	m_horizontalBlurShader = new Shader();
	m_verticalBlurShader = new Shader();

	m_horizontalBlurShader->LoadPixelShader(m_graphics, L"Shaders\\HBlurPixelShader.cso");
	m_verticalBlurShader->LoadPixelShader(m_graphics, L"Shaders\\VBlurPixelShader.cso");

	// initialise texture effects
	m_sceneTexture = new TextureEffect();
	m_downSample = new TextureEffect();
	m_horizontalBlurTexture = new TextureEffect();
	m_verticalBlurTexture = new TextureEffect();
	m_upSample = new TextureEffect();

	m_sceneTexture->Init(m_graphics, nullptr, m_screenWidth, m_screenHeight);
	m_downSample->Init(m_graphics, nullptr, m_downSampleWidth, m_downSampleHeight);
	m_horizontalBlurTexture->Init(m_graphics, m_horizontalBlurShader, m_downSampleWidth, m_downSampleHeight);
	m_verticalBlurTexture->Init(m_graphics, nullptr, m_downSampleWidth, m_downSampleHeight);
	m_upSample->Init(m_graphics, nullptr, m_screenWidth, m_screenHeight);
}

void 
PostProcessing::Begin()
{
	// first part of process is to capture the scene
	m_sceneTexture->Begin();
}

void 
PostProcessing::Process()
{
	// down sample scene
	m_downSample->Begin();
	m_sceneTexture->Render(m_spriteBatch, m_downSampleWidth, m_downSampleHeight);

	// horizontal blur
	m_horizontalBlurTexture->Begin();
	m_downSample->Render(m_spriteBatch, m_downSampleWidth, m_downSampleHeight);

	// vertical blur
	m_verticalBlurTexture->Begin();
	m_horizontalBlurTexture->Render(m_spriteBatch, m_downSampleWidth, m_downSampleHeight);

	// upsample
	m_upSample->Begin();
	m_verticalBlurTexture->Render(m_spriteBatch, m_screenWidth, m_screenHeight);
}

void 
PostProcessing::Render()
{
	m_upSample->Render(m_spriteBatch, m_screenWidth, m_screenHeight);
}

void 
PostProcessing::Release()
{
	if(m_upSample) { m_upSample->Release(); }
	if(m_verticalBlurTexture) { m_verticalBlurTexture->Release(); }
	if(m_horizontalBlurTexture) { m_horizontalBlurTexture->Release(); }
	if(m_downSample) { m_downSample->Release(); }
	if(m_sceneTexture) { m_sceneTexture->Release(); }
	if(m_verticalBlurShader) { m_verticalBlurShader->Release(); }
	if(m_horizontalBlurShader) { m_horizontalBlurShader->Release(); }
}

