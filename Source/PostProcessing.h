// PostProcessing.h
// Christopher Ball 2017
// manages post processing effects such as blur

#ifndef _POSTPROCESSING_H_
#define _POSTPROCESSING_H_

#include "pch.h"

// forward declarations
class Graphics;
class Shader;
class TextureEffect;

class PostProcessing
{
public:
	PostProcessing();
	~PostProcessing();
	void Init(Graphics* graphics, SpriteBatch* spriteBatch);							// initialise post processing
	void Begin();											// begin post processing
	void Process();											// process effects
	void Render();					// render effects
	void Release();											// release pointers

private:
	Graphics*			m_graphics;							// pointer to graphics class
	SpriteBatch*		m_spriteBatch;						// pointer to sprite batch
	// shaders using post processing	
	Shader*				m_horizontalBlurShader;							
	Shader*				m_verticalBlurShader;

	// textures effects required
	TextureEffect*		m_sceneTexture;						// texture of scene
	TextureEffect*		m_downSample;						// downsample of game scene
	TextureEffect*		m_horizontalBlurTexture;			// horizontal blur pass
	TextureEffect*		m_verticalBlurTexture;				// vertical blur pass
	TextureEffect*		m_upSample;							// up sample of complete effects

	// screen dimensions
	int					m_screenWidth;						// screen width
	int					m_screenHeight;						// screen height

	// downsample dimensions
	int					m_downSampleWidth;					// width of downsample
	int					m_downSampleHeight;					// height of downsample
};

#endif _POSTPROCESSING_H_
