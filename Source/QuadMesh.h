// QuadMesh.h
// Christopher Ball 2017
// child of MeshObject, holds data for a quad

#ifndef _QUAD_MESH_H_
#define _QUAD_MESH_H_

// forward declarations
class Graphics;

#include "MeshObject.h"

// inherit from Mesh Object
class QuadMesh : public MeshObject
{
public:
	QuadMesh();
	~QuadMesh();
	void Init(Graphics* graphics); // initialise quad mesh
};

#endif _QUAD_MESH_H_