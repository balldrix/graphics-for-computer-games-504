#include "Ramp.h"

Ramp::Ramp() :
	m_angle(0.0f)
{
}

Ramp::~Ramp()
{
}

void Ramp::Update()
{
	// set rotational matrix
	m_rotationalMatrix = Matrix::CreateRotationY(m_angle);

	// transpose model matrix
	m_modelMatrix = XMMatrixTranslationFromVector(m_position);

	// create final model world matrix
	m_modelMatrix = m_rotationalMatrix * m_scaleMatrix * m_modelMatrix;
}

void 
Ramp::SetAngle(float angle)
{
	m_angle = angle;
}
