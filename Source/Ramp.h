// Ramp.h
// Christopher Ball 2017
// Object class for the ramp

#ifndef _RAMP_H_
#define _RAMP_H_

#include "GameObject.h"

// inherit from game object
class Ramp : public GameObject
{
public:
	Ramp();
	~Ramp();
	void Update(); // update world position matrix
	void SetAngle(float angle);
private:
	float m_angle; // ramp angle
};

#endif _RAMP_H_
