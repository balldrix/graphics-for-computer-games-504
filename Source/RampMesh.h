// RampMesh.h
// Christopher Ball 2017
// Mesh information for the ramp

#ifndef _RAMPMESH_H_
#define _RAMPMESH_H_

#include "MeshObject.h"

// forward declarations
class Graphics;

// inherit from game object
class RampMesh : public MeshObject
{
public:
	RampMesh();
	~RampMesh();
	void Init(Graphics* graphics); // initilalise ramp mesh
};

#endif _RAMPMESH_H_