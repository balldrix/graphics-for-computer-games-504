#include "Shader.h"
#include "pch.h"
#include "Graphics.h"
#include "Error.h"
#include "ConstantBuffers.h"

Shader::Shader() :
	m_vertexShader(nullptr),
	m_pixelShader(nullptr),
	m_vertexLayout(nullptr)
{
}

Shader::~Shader()
{
}

void 
Shader::LoadVertexShader(Graphics* graphics, std::wstring filename, D3D11_INPUT_ELEMENT_DESC* inputElement, int numElements)
{
	HRESULT result; // used for error checking
	
	// shader data buffer
	ID3DBlob* compiledShader;
	// read from file to data buffer
	D3DReadFileToBlob(filename.c_str(), &compiledShader);

	// create vertex shader
	result = graphics->GetDevice()->CreateVertexShader(compiledShader->GetBufferPointer(), compiledShader->GetBufferSize(), nullptr, &m_vertexShader);

	// error checking
	if(result != S_OK)
	{
		// create error message for file log
		std::string error = " Error creating vertex shader in Shader.cpp;\n";

		// call Error namespace to log file
		Error::FileLog(error);

		// display windows message box
		MessageBox(graphics->GetHwnd(), L"Shader Error. See Logs/Error.txt", L"Error!", MB_OK);

		PostQuitMessage(0); // quit game
	}

	// create input layout
	result = graphics->GetDevice()->CreateInputLayout(inputElement, numElements, compiledShader->GetBufferPointer(), compiledShader->GetBufferSize(), &m_vertexLayout);

	// error checking
	if(result != S_OK)
	{
		// create error message for file log
		std::string error = " Error creating input layout in Shader.cpp;\n";

		// call Error namespace to log file
		Error::FileLog(error);

		// display windows message box
		MessageBox(graphics->GetHwnd(), L"Shader Error. See Logs/Error.txt", L"Error!", MB_OK);

		PostQuitMessage(0); // quit game
	}
}

void 
Shader::LoadPixelShader(Graphics* graphics, std::wstring filename)
{
	HRESULT result; // used for error checking

	// shader data buffer
	ID3DBlob* compiledShader;
	// read from file to data buffer
	D3DReadFileToBlob(filename.c_str(), &compiledShader);

	// create pixel shader
	result = graphics->GetDevice()->CreatePixelShader(compiledShader->GetBufferPointer(), compiledShader->GetBufferSize(), nullptr, &m_pixelShader);

	// error checking
	if(result != S_OK)
	{
		std::string error = " Error creating pixel shader in Shader.cpp;\n";

		// call Error namespace to log file
		Error::FileLog(error);

		// display windows message box
		MessageBox(graphics->GetHwnd(), L"Shader Error. See Logs/Error.txt", L"Error!", MB_OK);

		PostQuitMessage(0); // quit game
	}
}

void
Shader::Release()
{
	// release all shader related pointers
	if(m_vertexLayout) { m_vertexLayout->Release(); }
	if(m_pixelShader) { m_pixelShader->Release(); }
	if(m_vertexShader) { m_vertexShader->Release(); }
}
