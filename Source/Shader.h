// Shader.h
// Christopher Ball 2017
// Shader class to store shader resources

#ifndef _SHADER_H_
#define _SHADER_H_

#include "pch.h"

// foward declarations
class Graphics;

class Shader
{
public:
	Shader();
	~Shader();
	void LoadVertexShader(Graphics* graphics, std::wstring filename, D3D11_INPUT_ELEMENT_DESC* inputElement, int numElements); // load shader from file
	void LoadPixelShader(Graphics* graphics, std::wstring filename); // load shader from file
	void Release(); // release shaders and input layout
	ID3D11VertexShader* GetVertexShader() const { return m_vertexShader; }
	ID3D11PixelShader* GetPixelShader() const { return m_pixelShader; }
	ID3D11InputLayout* GetInputLayout() const { return m_vertexLayout; }

private:
	ID3D11VertexShader* m_vertexShader; // vertex shader
	ID3D11PixelShader* m_pixelShader; // pixel shader
	ID3D11InputLayout* m_vertexLayout; // input layout
};

#endif _SHADER_H_