#include "Shadow.h"

Shadow::Shadow()
{
}

Shadow::~Shadow()
{
}

void 
Shadow::Update()
{
	// translate model position
	m_modelMatrix = XMMatrixTranslationFromVector(m_position);

	// tranform block matrix
	m_modelMatrix = m_rotationalMatrix * m_modelMatrix;
}

void 
Shadow::RotationalMatrix(const Matrix& matrix)
{
	m_rotationalMatrix = matrix;
}
