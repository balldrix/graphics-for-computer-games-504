// Shadow.h
// Christopher Ball 2016
// Shadow object to render under neath AI block

#ifndef _SHADOW_H_
#define _SHADOW_H_

#include "GameObject.h"

// inherit from game object
class Shadow : public GameObject
{
public:
	Shadow();
	~Shadow();
	void Update(); // update model matrix
	void RotationalMatrix(const Matrix& matrix);
};

#endif _SHADOW_H_