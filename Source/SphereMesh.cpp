#include "pch.h"
#include "SphereMesh.h"

#include "Graphics.h"
#include "ConstantBuffers.h"

SphereMesh::SphereMesh() :
	m_radius(0.5f)
{
}

SphereMesh::SphereMesh(float radius)
{
	m_radius = radius;
}

SphereMesh::~SphereMesh()
{
}

void
SphereMesh::Init(Graphics* graphics)
{
	// setup sphere mesh vertices, indices
	// and buffers for the sphere mesh

	std::vector<VertexPositionNormalTexture> vertices;
	std::vector<uint16_t> indices;

	GeometricPrimitive::CreateSphere(vertices, indices, (m_radius * 2), 16Ui16, false);

	D3D11_BUFFER_DESC bd = { 0 };
	D3D11_SUBRESOURCE_DATA subData = { 0 };

	m_vertexCount = vertices.size();
	m_indexCount = indices.size();

	// create vertex buffer for a sphere
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(VertexPositionNormalTexture) * m_vertexCount;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	subData.pSysMem = vertices.data();
	graphics->GetDevice()->CreateBuffer(&bd, &subData, &m_vertexBuffer);

	// create index buffer
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(short) * m_indexCount;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	subData.pSysMem = indices.data();
	graphics->GetDevice()->CreateBuffer(&bd, &subData, &m_indexBuffer);

	// set stride
	m_stride = sizeof(VertexPositionNormalTexture);
}
