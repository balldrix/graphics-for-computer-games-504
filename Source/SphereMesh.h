// SphereMesh.h
// Christopher Ball 2017
// child of Meshobject, hold data for spheres

#ifndef _SPHEREMESH_H_
#define _SPHEREMESH_H_

#include "MeshObject.h"

// forward declarations
class Graphics;

// inherits MeshObject class
class SphereMesh : public MeshObject
{
public:
	SphereMesh();
	SphereMesh(float radius);
	~SphereMesh();
	void Init(Graphics* graphics); // initialise sphere mesh
	float GetRadius;

private:
	float m_radius;
};


#endif _SPHEREMESH_H_
