// Texture.h
// Christopher Ball 2017
// a class to load and store textures
// for use in materials

#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include "pch.h"

// forward declarations
class Graphics;

class Texture
{
public:
	Texture();
	~Texture();
	void LoadTexture(Graphics* graphics, std::string filename); // load texture from file
	void LoadDDS(Graphics* graphics, std::string filename); // loads DDS texture file
	
	ID3D11ShaderResourceView*	GetTexture() { return m_texture; } // return pointer to texture resource
	ID3D11Resource*				GetResource() { return m_resource; } // return pointer to resource
	void Release(); // release shader resource

private:
	ID3D11ShaderResourceView*	m_texture; // shader resource
	ID3D11Resource*			m_resource; // texture resource
};

#endif _TEXTURE_H_
