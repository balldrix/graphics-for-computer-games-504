#include "pch.h"
#include "TextureEffect.h"
#include "Graphics.h"
#include "Shader.h"
#include "Error.h"

TextureEffect::TextureEffect() :
	m_graphics(nullptr),
	m_shader(nullptr),
	m_renderTargetView(nullptr),
	m_targetTexture(nullptr),
	m_shaderResource(nullptr)
{
}

TextureEffect::~TextureEffect()
{
}

void 
TextureEffect::Init(Graphics* graphics, Shader* shader, int textureWidth, int textureHeight)
{
	// result id tracking
	HRESULT result;

	// copy pointers
	m_graphics = graphics;
	m_shader = shader;

	// setup target texture
	D3D11_TEXTURE2D_DESC t2d = { 0 };
	t2d.Width = textureWidth;
	t2d.Height = textureHeight;
	t2d.MipLevels = 1;
	t2d.ArraySize = 1;
	t2d.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	t2d.SampleDesc.Count = 1;
	t2d.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	t2d.Usage = D3D11_USAGE_DEFAULT;

	// create target texture
	result = m_graphics->GetDevice()->CreateTexture2D(&t2d,
													  NULL,
													  &m_targetTexture);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Texture2D in TextureEffect.cpp; \n");

		MessageBox(m_graphics->GetHwnd(), L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	// setup render target view
	D3D11_RENDER_TARGET_VIEW_DESC rtvd;
	rtvd.Format = t2d.Format;
	rtvd.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	rtvd.Texture2D.MipSlice = 0;

	// create render target view
	result = m_graphics->GetDevice()->CreateRenderTargetView(m_targetTexture,
															 &rtvd,
															 &m_renderTargetView);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Render Target View in TextureEffect.cpp; \n");

		MessageBox(m_graphics->GetHwnd(), L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	// setup shader resource
	D3D11_SHADER_RESOURCE_VIEW_DESC srvd;
	srvd.Format = t2d.Format;
	srvd.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvd.Texture2D.MostDetailedMip = 0;
	srvd.Texture2D.MipLevels = 1;

	// create the shader resource view.
	result = m_graphics->GetDevice()->CreateShaderResourceView(m_targetTexture,
															   &srvd,
															   &m_shaderResource);
	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog(" Error Creating Shader Resource View in TextureEffect.cpp; \n");

		MessageBox(m_graphics->GetHwnd(), L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}
}

void 
TextureEffect::Begin()
{
	// set correct render target to begin processing
	m_graphics->GetDeviceContext()->OMSetRenderTargets(1,
													   &m_renderTargetView,
													   m_graphics->GetDepthStencilView());

	// clear render target buffer
	m_graphics->GetDeviceContext()->ClearRenderTargetView(m_renderTargetView, Colors::DarkSlateGray);
}

void 
TextureEffect::Render(SpriteBatch* spriteBatch, int width, int height)
{
	// check if custom shader is used
	if(m_shader != nullptr)
	{
		// begin sprite batch rendering using custom shader
		spriteBatch->Begin(SpriteSortMode_Immediate, nullptr, nullptr, nullptr, nullptr, [=]
		{
			m_graphics->GetDeviceContext()->PSSetShader(m_shader->GetPixelShader(), nullptr, 0);
			m_graphics->GetDeviceContext()->PSSetShaderResources(0, 1, &m_shaderResource);
		});
	}
	else
	{
		spriteBatch->Begin();
	}
	
	// render texture
	RECT r = { 0, 0, width, height };
	spriteBatch->Draw(m_shaderResource, r);
	spriteBatch->End();
}

void 
TextureEffect::Release()
{
	// release all resources
	if(m_shaderResource) { m_shaderResource->Release(); }
	if(m_targetTexture) { m_targetTexture->Release(); }
	if(m_renderTargetView) { m_renderTargetView->Release(); }
}
