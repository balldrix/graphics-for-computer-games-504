// TextureEffect.h
// Christopher Ball 2017
// creates a texture effect for post processing

#ifndef _TEXTURE_EFFECT_H_
#define _TEXTURE_EFFECT_H_

#include "pch.h"

// forward declarations
class Graphics;
class Shader;

class TextureEffect
{
public:
	TextureEffect();
	~TextureEffect();
	void Init(Graphics* graphics, Shader* shader, int textureWidth, int textureHeight);
	void Begin();
	void Render(SpriteBatch* spriteBatch, int width, int height);
	void Release();

private:
	Graphics*					m_graphics;					// pointer to graphics class
	Shader*						m_shader;					// shader for effects
	ID3D11RenderTargetView*		m_renderTargetView;			// pointer to render target view
	ID3D11Texture2D*			m_targetTexture;			// pointer to 2d backbuffer
	ID3D11ShaderResourceView*	m_shaderResource;			// shader resource view
};

#endif _TEXTURE_EFFECT_H_
