// UnitVectors.h
// Christopher Ball 2016
// Namespace with unit vectors
// for each compass direction

#ifndef	_UNIT_VECTORS_H_
#define _UNIT_VECTORS_H_

#include "pch.h"

// map compass directions as unit vectors
namespace MapDirections
{
	const Vector3 North = Vector3(0.0f, 0.0f, 1.0f); // north direction
	const Vector3 NorthEast = Vector3(0.707f, 0.0f, 0.707f); // north east direction
	const Vector3 East = Vector3(1.0f, 0.0f, 0.0f); // east direction
	const Vector3 SouthEast = Vector3(0.707f, 0.0f, -0.707f); // south east direction
	const Vector3 South = Vector3(0.0f, 0.0f, -1.0f); // south direction
	const Vector3 SouthWest = Vector3(-0.707f, 0.0f, -0.707f); // south west direction
	const Vector3 West = Vector3(-1.0f, 0.0f, 0.0f); // west direction
	const Vector3 NorthWest = Vector3(-0.707f, 0.0f, 0.707f); // north west direction
}

#endif _UNIT_VECTORS_H_