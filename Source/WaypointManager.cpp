#include "WaypointManager.h"

WaypointManager::WaypointManager()
{
}

WaypointManager::~WaypointManager()
{
}

bool
WaypointManager::LoadWaypoints(std::string filename)
{
	std::ifstream file; // ifstream file buffer
	file.open(filename); // opens file and reads to buffer
	if(file) // if file is open
	{
		std::string ID;
		float x, y, z;
		
		// loop until end of file
		while(!file.eof())
		{
			// ready data
			file >> ID;
			file >> x;
			file >> y;
			file >> z;

			Waypoint waypoint;
			waypoint.ID = ID;
			waypoint.position.x = x;
			waypoint.position.y = y;
			waypoint.position.z = z;

			// push waypoint to list
			m_waypointList.push_back(waypoint);
		}
	}
	else
	{
		return false;
	}
	file.close(); // close file
	return true;
}

const Waypoint*
WaypointManager::GetWaypoint(std::string ID) const
{
	for(int i = 0; i < m_waypointList.size(); i++)
	{
		if(m_waypointList[i].ID == ID)
		{
			return &m_waypointList[i];
		}
	}
	
	return nullptr;
}

const Waypoint*
WaypointManager::GetWaypoint(int index) const
{
	return &m_waypointList[index];
}

void 
WaypointManager::Delete()
{
	m_waypointList.clear();
}
