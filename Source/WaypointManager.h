// WaypointManager.h
// Christopher Ball 2017
// manages all waypoints

#ifndef _WAYPOINTMANAGER_H_
#define _WAYPOINTMANAGER_H_

#include "pch.h"

struct Waypoint
{
	std::string ID;
	Vector3 position;
};

class WaypointManager
{
public:
	WaypointManager();
	~WaypointManager();	
	
	bool					LoadWaypoints(std::string filename);	// loads waypoints
	const Waypoint*			GetWaypoint(std::string ID) const;		// get waypoint using ID
	const Waypoint*			GetWaypoint(int index) const;			// get waypoint using index
	void					Delete();								// delete waypoints			

private:
	std::vector<Waypoint>	 m_waypointList;						// list of waypoints
};

#endif _WAYPOINTMANAGER_H_