Christopher Ball UD6127838H
3D Graphics for Computer Games 4241-504

This project was written using Windows 7, Visual Studio 2015 (v140), target version 10.0.14393.0, Directx TK 2015 with shader level 4_1.

Keys are:

W - move forward
A - move backward
S - turn left
D - turn right

UP - zoom in
DOWN - zoom out
LEFT - rotate camera left
RIGHT - rotate camera right

KEYPAD 0 - reset camera